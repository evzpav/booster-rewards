package main

import (
	"booster-rewards/ERC20Token"
	"booster-rewards/booster"
	"fmt"
	"log"
	"math/big"
	"os"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

var nodeURL string = "https://mainnet.infura.io/v3/3a41fe49e61540b991aa5c6de755344b"
var boosterContractAddress string = "0xf403c135812408bfbe8713b5a23a04b3d48aae31"
var crvTokenContract string = "0xd533a949740bb3306d119cc777fa900ba034cd52"
var voterProxyContract string = "0x989aeb4d175e16225e39e87d0d97a3360524ad80"

func main() {

	fileName := "output_" + formatTime(time.Now()) + ".txt"
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	client, err := ethclient.Dial(nodeURL)
	if err != nil {
		log.Fatalf("Failed to connect to the Ethereum network: %v", err)
	}

	booster, err := booster.NewBooster(common.HexToAddress(boosterContractAddress), client)
	if err != nil {
		log.Fatal(err)
	}

	curveToken, err := ERC20Token.NewERC20(common.HexToAddress(crvTokenContract), client)
	if err != nil {
		log.Fatal(err)
	}

	ticker := time.NewTicker(1000 * time.Millisecond)
	done := make(chan bool)

	line := fmt.Sprintf("%s; started checking...\n", formatTime(time.Now()))
	_, err = file.Write([]byte(line))
	if err != nil {
		fmt.Println(err)
	}

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:

				boostCRVBalance, err := curveToken.BalanceOf(&bind.CallOpts{}, common.HexToAddress(boosterContractAddress))
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("%s - booster CRV balance: %v\n", formatTime(time.Now()), boostCRVBalance.String())

				if len(boostCRVBalance.String()) > 1 {
					line := fmt.Sprintf("%s; %s; booster CRV\n", formatTime(time.Now()), boostCRVBalance.String())
					_, err = file.Write([]byte(line))
					if err != nil {
						fmt.Println(err)
					}
				}

				voterProxyCRVBalance, err := curveToken.BalanceOf(&bind.CallOpts{}, common.HexToAddress(voterProxyContract))
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("%s - voter proxy CRV balance: %v\n", formatTime(time.Now()), voterProxyCRVBalance.String())

				if len(voterProxyCRVBalance.String()) > 1 {
					line := fmt.Sprintf("%s; %s; voter proxy CRV\n", formatTime(time.Now()), voterProxyCRVBalance.String())
					_, err = file.Write([]byte(line))
					if err != nil {
						fmt.Println(err)
					}
				}

				if len(boostCRVBalance.String()) <= 1 {
					continue
				}

				incentive, err := booster.EarmarkIncentive(&bind.CallOpts{})
				if err != nil {
					log.Fatal(err)
				}

				fmt.Printf("incentive: %s\n", incentive.String())

				feeDenominator, err := booster.FEEDENOMINATOR(&bind.CallOpts{})
				if err != nil {
					log.Fatal(err)
				}

				fmt.Printf("feeDenominator: %s\n", feeDenominator.String())

				incentiveAmount := resolveCallIncentiveAmount(incentive, feeDenominator, boostCRVBalance)
				fmt.Printf("booster callIncentive amount: %v\n", incentiveAmount.String())

				line := fmt.Sprintf("%s; %s; %s\n", formatTime(time.Now()), boostCRVBalance.String(), incentiveAmount.String())
				_, err = file.Write([]byte(line))
				if err != nil {
					fmt.Println(err)
				}

			}

		}
	}()

	time.Sleep(10 * time.Hour)
	ticker.Stop()
	done <- true
	fmt.Println("Ticker stopped")
}

func resolveCallIncentiveAmount(earmarkIncentive, feeDenomnator, crvBalance *big.Int) *big.Int {
	n := earmarkIncentive.Mul(crvBalance, earmarkIncentive)
	return n.Div(n, feeDenomnator)
}

func formatTime(time time.Time) string {
	return time.Format("2006-01-02 15:04:05")
}
