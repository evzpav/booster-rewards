# Booster Rewards


## Run project
```bash
go run main.go
# CRV and CVX balances of the booster stakerRewards contract will be diplayed
```

## Contract bindings
Clone the repo https://github.com/cryptoriums/contraget and run the following to get the bindings:

```bash
# Booster contract
go run cmd/contraget/contraget.go --path=0xf403c135812408bfbe8713b5a23a04b3d48aae31 --download-dst=tmp --pkg-dst=pkg/contracts --network=mainnet --name=booster

# ERC20 token contract
go run cmd/contraget/contraget.go --path=0x4e3fbd56cd56c3e72c1403e103b45db9da5b9d2b --download-dst=tmp --pkg-dst=pkg/contracts --network=mainnet --name=ERC20Token

# failing CRV token contraget:
go run cmd/contraget/contraget.go --path=0xd533a949740bb3306d119cc777fa900ba034cd52 --download-dst=tmp --pkg-dst=pkg/contracts --network=mainnet --name=ERC20Token

# Then copy the folders created to a separate repository(this one)
```
