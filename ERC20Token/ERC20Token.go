// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ERC20Token

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// AddressMetaData contains all meta data concerning the Address contract.
var AddressMetaData = &bind.MetaData{
	ABI: "[]",
	Bin: "0x60566023600b82828239805160001a607314601657fe5b30600052607381538281f3fe73000000000000000000000000000000000000000030146080604052600080fdfea2646970667358221220d72744f68f6a9898624916ba2961efeaf2e3ba119f264e5bc6ade80fc2e3910264736f6c634300060c0033",
}

// AddressABI is the input ABI used to generate the binding from.
// Deprecated: Use AddressMetaData.ABI instead.
var AddressABI = AddressMetaData.ABI

// AddressBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use AddressMetaData.Bin instead.
var AddressBin = AddressMetaData.Bin

// DeployAddress deploys a new Ethereum contract, binding an instance of Address to it.
func DeployAddress(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Address, error) {
	parsed, err := AddressMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(AddressBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Address{AddressCaller: AddressCaller{contract: contract}, AddressTransactor: AddressTransactor{contract: contract}, AddressFilterer: AddressFilterer{contract: contract}}, nil
}

// Address is an auto generated Go binding around an Ethereum contract.
type Address struct {
	AddressCaller     // Read-only binding to the contract
	AddressTransactor // Write-only binding to the contract
	AddressFilterer   // Log filterer for contract events
}

// AddressCaller is an auto generated read-only Go binding around an Ethereum contract.
type AddressCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AddressTransactor is an auto generated write-only Go binding around an Ethereum contract.
type AddressTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AddressFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type AddressFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// AddressSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type AddressSession struct {
	Contract     *Address          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// AddressCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type AddressCallerSession struct {
	Contract *AddressCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// AddressTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type AddressTransactorSession struct {
	Contract     *AddressTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// AddressRaw is an auto generated low-level Go binding around an Ethereum contract.
type AddressRaw struct {
	Contract *Address // Generic contract binding to access the raw methods on
}

// AddressCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type AddressCallerRaw struct {
	Contract *AddressCaller // Generic read-only contract binding to access the raw methods on
}

// AddressTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type AddressTransactorRaw struct {
	Contract *AddressTransactor // Generic write-only contract binding to access the raw methods on
}

// NewAddress creates a new instance of Address, bound to a specific deployed contract.
func NewAddress(address common.Address, backend bind.ContractBackend) (*Address, error) {
	contract, err := bindAddress(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Address{AddressCaller: AddressCaller{contract: contract}, AddressTransactor: AddressTransactor{contract: contract}, AddressFilterer: AddressFilterer{contract: contract}}, nil
}

// NewAddressCaller creates a new read-only instance of Address, bound to a specific deployed contract.
func NewAddressCaller(address common.Address, caller bind.ContractCaller) (*AddressCaller, error) {
	contract, err := bindAddress(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &AddressCaller{contract: contract}, nil
}

// NewAddressTransactor creates a new write-only instance of Address, bound to a specific deployed contract.
func NewAddressTransactor(address common.Address, transactor bind.ContractTransactor) (*AddressTransactor, error) {
	contract, err := bindAddress(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &AddressTransactor{contract: contract}, nil
}

// NewAddressFilterer creates a new log filterer instance of Address, bound to a specific deployed contract.
func NewAddressFilterer(address common.Address, filterer bind.ContractFilterer) (*AddressFilterer, error) {
	contract, err := bindAddress(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &AddressFilterer{contract: contract}, nil
}

// bindAddress binds a generic wrapper to an already deployed contract.
func bindAddress(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(AddressABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Address *AddressRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Address.Contract.AddressCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Address *AddressRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Address.Contract.AddressTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Address *AddressRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Address.Contract.AddressTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Address *AddressCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Address.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Address *AddressTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Address.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Address *AddressTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Address.Contract.contract.Transact(opts, method, params...)
}

// ContextMetaData contains all meta data concerning the Context contract.
var ContextMetaData = &bind.MetaData{
	ABI: "[]",
}

// ContextABI is the input ABI used to generate the binding from.
// Deprecated: Use ContextMetaData.ABI instead.
var ContextABI = ContextMetaData.ABI

// Context is an auto generated Go binding around an Ethereum contract.
type Context struct {
	ContextCaller     // Read-only binding to the contract
	ContextTransactor // Write-only binding to the contract
	ContextFilterer   // Log filterer for contract events
}

// ContextCaller is an auto generated read-only Go binding around an Ethereum contract.
type ContextCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContextTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ContextTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContextFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ContextFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ContextSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ContextSession struct {
	Contract     *Context          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ContextCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ContextCallerSession struct {
	Contract *ContextCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// ContextTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ContextTransactorSession struct {
	Contract     *ContextTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// ContextRaw is an auto generated low-level Go binding around an Ethereum contract.
type ContextRaw struct {
	Contract *Context // Generic contract binding to access the raw methods on
}

// ContextCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ContextCallerRaw struct {
	Contract *ContextCaller // Generic read-only contract binding to access the raw methods on
}

// ContextTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ContextTransactorRaw struct {
	Contract *ContextTransactor // Generic write-only contract binding to access the raw methods on
}

// NewContext creates a new instance of Context, bound to a specific deployed contract.
func NewContext(address common.Address, backend bind.ContractBackend) (*Context, error) {
	contract, err := bindContext(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Context{ContextCaller: ContextCaller{contract: contract}, ContextTransactor: ContextTransactor{contract: contract}, ContextFilterer: ContextFilterer{contract: contract}}, nil
}

// NewContextCaller creates a new read-only instance of Context, bound to a specific deployed contract.
func NewContextCaller(address common.Address, caller bind.ContractCaller) (*ContextCaller, error) {
	contract, err := bindContext(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ContextCaller{contract: contract}, nil
}

// NewContextTransactor creates a new write-only instance of Context, bound to a specific deployed contract.
func NewContextTransactor(address common.Address, transactor bind.ContractTransactor) (*ContextTransactor, error) {
	contract, err := bindContext(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ContextTransactor{contract: contract}, nil
}

// NewContextFilterer creates a new log filterer instance of Context, bound to a specific deployed contract.
func NewContextFilterer(address common.Address, filterer bind.ContractFilterer) (*ContextFilterer, error) {
	contract, err := bindContext(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ContextFilterer{contract: contract}, nil
}

// bindContext binds a generic wrapper to an already deployed contract.
func bindContext(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ContextABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Context *ContextRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Context.Contract.ContextCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Context *ContextRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Context.Contract.ContextTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Context *ContextRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Context.Contract.ContextTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Context *ContextCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Context.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Context *ContextTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Context.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Context *ContextTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Context.Contract.contract.Transact(opts, method, params...)
}

// ConvexTokenMetaData contains all meta data concerning the ConvexToken contract.
var ConvexTokenMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_proxy\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"subtractedValue\",\"type\":\"uint256\"}],\"name\":\"decreaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"addedValue\",\"type\":\"uint256\"}],\"name\":\"increaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"maxSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"operator\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"reductionPerCliff\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalCliffs\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"updateOperator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"vecrvProxy\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"dd62ed3e": "allowance(address,address)",
		"095ea7b3": "approve(address,uint256)",
		"70a08231": "balanceOf(address)",
		"313ce567": "decimals()",
		"a457c2d7": "decreaseAllowance(address,uint256)",
		"39509351": "increaseAllowance(address,uint256)",
		"d5abeb01": "maxSupply()",
		"40c10f19": "mint(address,uint256)",
		"06fdde03": "name()",
		"570ca735": "operator()",
		"aa74e622": "reductionPerCliff()",
		"95d89b41": "symbol()",
		"1f96e76f": "totalCliffs()",
		"18160ddd": "totalSupply()",
		"a9059cbb": "transfer(address,uint256)",
		"23b872dd": "transferFrom(address,address,uint256)",
		"d5934b76": "updateOperator()",
		"fca975a1": "vecrvProxy()",
	},
	Bin: "0x60806040526a52b7d2dcc80cd2e40000006007556103e86008553480156200002657600080fd5b50604051620010db380380620010db833981810160405260208110156200004c57600080fd5b5051604080518082018252600c81526b21b7b73b32bc102a37b5b2b760a11b60208281019182528351808501909452600380855262086acb60eb1b9185019190915282519293926200009f929062000183565b508051620000b590600490602084019062000183565b505060058054601260ff1990911617610100600160a81b031916610100330217905550600680546001600160a01b0319166001600160a01b03831617905560085460075462000110916200011a602090811b620007ff17901c565b600955506200021f565b600080821162000171576040805162461bcd60e51b815260206004820152601a60248201527f536166654d6174683a206469766973696f6e206279207a65726f000000000000604482015290519081900360640190fd5b8183816200017b57fe5b049392505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10620001c657805160ff1916838001178555620001f6565b82800160010185558215620001f6579182015b82811115620001f6578251825591602001919060010190620001d9565b506200020492915062000208565b5090565b5b8082111562000204576000815560010162000209565b610eac806200022f6000396000f3fe608060405234801561001057600080fd5b50600436106101165760003560e01c806370a08231116100a2578063aa74e62211610071578063aa74e62214610352578063d5934b761461035a578063d5abeb0114610362578063dd62ed3e1461036a578063fca975a11461039857610116565b806370a08231146102cc57806395d89b41146102f2578063a457c2d7146102fa578063a9059cbb1461032657610116565b806323b872dd116100e957806323b872dd146101fa578063313ce56714610230578063395093511461024e57806340c10f191461027a578063570ca735146102a857610116565b806306fdde031461011b578063095ea7b31461019857806318160ddd146101d85780631f96e76f146101f2575b600080fd5b6101236103a0565b6040805160208082528351818301528351919283929083019185019080838360005b8381101561015d578181015183820152602001610145565b50505050905090810190601f16801561018a5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6101c4600480360360408110156101ae57600080fd5b506001600160a01b038135169060200135610436565b604080519115158252519081900360200190f35b6101e0610454565b60408051918252519081900360200190f35b6101e061045a565b6101c46004803603606081101561021057600080fd5b506001600160a01b03813581169160208101359091169060400135610460565b6102386104e7565b6040805160ff9092168252519081900360200190f35b6101c46004803603604081101561026457600080fd5b506001600160a01b0381351690602001356104f0565b6102a66004803603604081101561029057600080fd5b506001600160a01b03813516906020013561053e565b005b6102b061060b565b604080516001600160a01b039092168252519081900360200190f35b6101e0600480360360208110156102e257600080fd5b50356001600160a01b031661061f565b61012361063a565b6101c46004803603604081101561031057600080fd5b506001600160a01b03813516906020013561069b565b6101c46004803603604081101561033c57600080fd5b506001600160a01b038135169060200135610703565b6101e0610717565b6102a661071d565b6101e06107bf565b6101e06004803603604081101561038057600080fd5b506001600160a01b03813581169160200135166107c5565b6102b06107f0565b60038054604080516020601f600260001961010060018816150201909516949094049384018190048102820181019092528281526060939092909183018282801561042c5780601f106104015761010080835404028352916020019161042c565b820191906000526020600020905b81548152906001019060200180831161040f57829003601f168201915b5050505050905090565b600061044a610443610866565b848461086a565b5060015b92915050565b60025490565b60085481565b600061046d848484610956565b6104dd84610479610866565b6104d885604051806060016040528060288152602001610de1602891396001600160a01b038a166000908152600160205260408120906104b7610866565b6001600160a01b031681526020810191909152604001600020549190610ab1565b61086a565b5060019392505050565b60055460ff1690565b600061044a6104fd610866565b846104d8856001600061050e610866565b6001600160a01b03908116825260208083019390935260409182016000908120918c168152925290205490610b48565b60055461010090046001600160a01b0316331461055a57610607565b6000610564610454565b905080610583576105758383610ba9565b61057d61071d565b50610607565b600061059a600954836107ff90919063ffffffff16565b9050600854811015610604576008546000906105b69083610c99565b6008549091506105d0906105ca8684610cf6565b906107ff565b935060006105e984600754610c9990919063ffffffff16565b9050808511156105f7578094505b6106018686610ba9565b50505b50505b5050565b60055461010090046001600160a01b031681565b6001600160a01b031660009081526020819052604090205490565b60048054604080516020601f600260001961010060018816150201909516949094049384018190048102820181019092528281526060939092909183018282801561042c5780601f106104015761010080835404028352916020019161042c565b600061044a6106a8610866565b846104d885604051806060016040528060258152602001610e5260259139600160006106d2610866565b6001600160a01b03908116825260208083019390935260409182016000908120918d16815292529020549190610ab1565b600061044a610710610866565b8484610956565b60095481565b600660009054906101000a90046001600160a01b03166001600160a01b031663570ca7356040518163ffffffff1660e01b815260040160206040518083038186803b15801561076b57600080fd5b505afa15801561077f573d6000803e3d6000fd5b505050506040513d602081101561079557600080fd5b5051600580546001600160a01b0390921661010002610100600160a81b0319909216919091179055565b60075481565b6001600160a01b03918216600090815260016020908152604080832093909416825291909152205490565b6006546001600160a01b031681565b6000808211610855576040805162461bcd60e51b815260206004820152601a60248201527f536166654d6174683a206469766973696f6e206279207a65726f000000000000604482015290519081900360640190fd5b81838161085e57fe5b049392505050565b3390565b6001600160a01b0383166108af5760405162461bcd60e51b8152600401808060200182810382526024815260200180610e2e6024913960400191505060405180910390fd5b6001600160a01b0382166108f45760405162461bcd60e51b8152600401808060200182810382526022815260200180610d786022913960400191505060405180910390fd5b6001600160a01b03808416600081815260016020908152604080832094871680845294825291829020859055815185815291517f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259281900390910190a3505050565b6001600160a01b03831661099b5760405162461bcd60e51b8152600401808060200182810382526025815260200180610e096025913960400191505060405180910390fd5b6001600160a01b0382166109e05760405162461bcd60e51b8152600401808060200182810382526023815260200180610d556023913960400191505060405180910390fd5b6109eb838383610d4f565b610a2881604051806060016040528060268152602001610d9a602691396001600160a01b0386166000908152602081905260409020549190610ab1565b6001600160a01b038085166000908152602081905260408082209390935590841681522054610a579082610b48565b6001600160a01b038084166000818152602081815260409182902094909455805185815290519193928716927fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef92918290030190a3505050565b60008184841115610b405760405162461bcd60e51b81526004018080602001828103825283818151815260200191508051906020019080838360005b83811015610b05578181015183820152602001610aed565b50505050905090810190601f168015610b325780820380516001836020036101000a031916815260200191505b509250505060405180910390fd5b505050900390565b600082820183811015610ba2576040805162461bcd60e51b815260206004820152601b60248201527f536166654d6174683a206164646974696f6e206f766572666c6f770000000000604482015290519081900360640190fd5b9392505050565b6001600160a01b038216610c04576040805162461bcd60e51b815260206004820152601f60248201527f45524332303a206d696e7420746f20746865207a65726f206164647265737300604482015290519081900360640190fd5b610c1060008383610d4f565b600254610c1d9082610b48565b6002556001600160a01b038216600090815260208190526040902054610c439082610b48565b6001600160a01b0383166000818152602081815260408083209490945583518581529351929391927fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9281900390910190a35050565b600082821115610cf0576040805162461bcd60e51b815260206004820152601e60248201527f536166654d6174683a207375627472616374696f6e206f766572666c6f770000604482015290519081900360640190fd5b50900390565b600082610d055750600061044e565b82820282848281610d1257fe5b0414610ba25760405162461bcd60e51b8152600401808060200182810382526021815260200180610dc06021913960400191505060405180910390fd5b50505056fe45524332303a207472616e7366657220746f20746865207a65726f206164647265737345524332303a20617070726f766520746f20746865207a65726f206164647265737345524332303a207472616e7366657220616d6f756e7420657863656564732062616c616e6365536166654d6174683a206d756c7469706c69636174696f6e206f766572666c6f7745524332303a207472616e7366657220616d6f756e74206578636565647320616c6c6f77616e636545524332303a207472616e736665722066726f6d20746865207a65726f206164647265737345524332303a20617070726f76652066726f6d20746865207a65726f206164647265737345524332303a2064656372656173656420616c6c6f77616e63652062656c6f77207a65726fa2646970667358221220ec11d854d05c076bf670e7bebf536bfc6a7f6ee1c29097933f9dbf258e628d3e64736f6c634300060c0033",
}

// ConvexTokenABI is the input ABI used to generate the binding from.
// Deprecated: Use ConvexTokenMetaData.ABI instead.
var ConvexTokenABI = ConvexTokenMetaData.ABI

// Deprecated: Use ConvexTokenMetaData.Sigs instead.
// ConvexTokenFuncSigs maps the 4-byte function signature to its string representation.
var ConvexTokenFuncSigs = ConvexTokenMetaData.Sigs

// ConvexTokenBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use ConvexTokenMetaData.Bin instead.
var ConvexTokenBin = ConvexTokenMetaData.Bin

// DeployConvexToken deploys a new Ethereum contract, binding an instance of ConvexToken to it.
func DeployConvexToken(auth *bind.TransactOpts, backend bind.ContractBackend, _proxy common.Address) (common.Address, *types.Transaction, *ConvexToken, error) {
	parsed, err := ConvexTokenMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(ConvexTokenBin), backend, _proxy)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &ConvexToken{ConvexTokenCaller: ConvexTokenCaller{contract: contract}, ConvexTokenTransactor: ConvexTokenTransactor{contract: contract}, ConvexTokenFilterer: ConvexTokenFilterer{contract: contract}}, nil
}

// ConvexToken is an auto generated Go binding around an Ethereum contract.
type ConvexToken struct {
	ConvexTokenCaller     // Read-only binding to the contract
	ConvexTokenTransactor // Write-only binding to the contract
	ConvexTokenFilterer   // Log filterer for contract events
}

// ConvexTokenCaller is an auto generated read-only Go binding around an Ethereum contract.
type ConvexTokenCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConvexTokenTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ConvexTokenTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConvexTokenFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ConvexTokenFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ConvexTokenSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ConvexTokenSession struct {
	Contract     *ConvexToken      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ConvexTokenCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ConvexTokenCallerSession struct {
	Contract *ConvexTokenCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// ConvexTokenTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ConvexTokenTransactorSession struct {
	Contract     *ConvexTokenTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// ConvexTokenRaw is an auto generated low-level Go binding around an Ethereum contract.
type ConvexTokenRaw struct {
	Contract *ConvexToken // Generic contract binding to access the raw methods on
}

// ConvexTokenCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ConvexTokenCallerRaw struct {
	Contract *ConvexTokenCaller // Generic read-only contract binding to access the raw methods on
}

// ConvexTokenTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ConvexTokenTransactorRaw struct {
	Contract *ConvexTokenTransactor // Generic write-only contract binding to access the raw methods on
}

// NewConvexToken creates a new instance of ConvexToken, bound to a specific deployed contract.
func NewConvexToken(address common.Address, backend bind.ContractBackend) (*ConvexToken, error) {
	contract, err := bindConvexToken(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ConvexToken{ConvexTokenCaller: ConvexTokenCaller{contract: contract}, ConvexTokenTransactor: ConvexTokenTransactor{contract: contract}, ConvexTokenFilterer: ConvexTokenFilterer{contract: contract}}, nil
}

// NewConvexTokenCaller creates a new read-only instance of ConvexToken, bound to a specific deployed contract.
func NewConvexTokenCaller(address common.Address, caller bind.ContractCaller) (*ConvexTokenCaller, error) {
	contract, err := bindConvexToken(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ConvexTokenCaller{contract: contract}, nil
}

// NewConvexTokenTransactor creates a new write-only instance of ConvexToken, bound to a specific deployed contract.
func NewConvexTokenTransactor(address common.Address, transactor bind.ContractTransactor) (*ConvexTokenTransactor, error) {
	contract, err := bindConvexToken(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ConvexTokenTransactor{contract: contract}, nil
}

// NewConvexTokenFilterer creates a new log filterer instance of ConvexToken, bound to a specific deployed contract.
func NewConvexTokenFilterer(address common.Address, filterer bind.ContractFilterer) (*ConvexTokenFilterer, error) {
	contract, err := bindConvexToken(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ConvexTokenFilterer{contract: contract}, nil
}

// bindConvexToken binds a generic wrapper to an already deployed contract.
func bindConvexToken(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ConvexTokenABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ConvexToken *ConvexTokenRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ConvexToken.Contract.ConvexTokenCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ConvexToken *ConvexTokenRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ConvexToken.Contract.ConvexTokenTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ConvexToken *ConvexTokenRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ConvexToken.Contract.ConvexTokenTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ConvexToken *ConvexTokenCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ConvexToken.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ConvexToken *ConvexTokenTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ConvexToken.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ConvexToken *ConvexTokenTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ConvexToken.Contract.contract.Transact(opts, method, params...)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) Allowance(opts *bind.CallOpts, owner common.Address, spender common.Address) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "allowance", owner, spender)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ConvexToken *ConvexTokenSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _ConvexToken.Contract.Allowance(&_ConvexToken.CallOpts, owner, spender)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _ConvexToken.Contract.Allowance(&_ConvexToken.CallOpts, owner, spender)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) BalanceOf(opts *bind.CallOpts, account common.Address) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "balanceOf", account)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ConvexToken *ConvexTokenSession) BalanceOf(account common.Address) (*big.Int, error) {
	return _ConvexToken.Contract.BalanceOf(&_ConvexToken.CallOpts, account)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) BalanceOf(account common.Address) (*big.Int, error) {
	return _ConvexToken.Contract.BalanceOf(&_ConvexToken.CallOpts, account)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ConvexToken *ConvexTokenCaller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ConvexToken *ConvexTokenSession) Decimals() (uint8, error) {
	return _ConvexToken.Contract.Decimals(&_ConvexToken.CallOpts)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ConvexToken *ConvexTokenCallerSession) Decimals() (uint8, error) {
	return _ConvexToken.Contract.Decimals(&_ConvexToken.CallOpts)
}

// MaxSupply is a free data retrieval call binding the contract method 0xd5abeb01.
//
// Solidity: function maxSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) MaxSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "maxSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MaxSupply is a free data retrieval call binding the contract method 0xd5abeb01.
//
// Solidity: function maxSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenSession) MaxSupply() (*big.Int, error) {
	return _ConvexToken.Contract.MaxSupply(&_ConvexToken.CallOpts)
}

// MaxSupply is a free data retrieval call binding the contract method 0xd5abeb01.
//
// Solidity: function maxSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) MaxSupply() (*big.Int, error) {
	return _ConvexToken.Contract.MaxSupply(&_ConvexToken.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ConvexToken *ConvexTokenCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ConvexToken *ConvexTokenSession) Name() (string, error) {
	return _ConvexToken.Contract.Name(&_ConvexToken.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ConvexToken *ConvexTokenCallerSession) Name() (string, error) {
	return _ConvexToken.Contract.Name(&_ConvexToken.CallOpts)
}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_ConvexToken *ConvexTokenCaller) Operator(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "operator")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_ConvexToken *ConvexTokenSession) Operator() (common.Address, error) {
	return _ConvexToken.Contract.Operator(&_ConvexToken.CallOpts)
}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_ConvexToken *ConvexTokenCallerSession) Operator() (common.Address, error) {
	return _ConvexToken.Contract.Operator(&_ConvexToken.CallOpts)
}

// ReductionPerCliff is a free data retrieval call binding the contract method 0xaa74e622.
//
// Solidity: function reductionPerCliff() view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) ReductionPerCliff(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "reductionPerCliff")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ReductionPerCliff is a free data retrieval call binding the contract method 0xaa74e622.
//
// Solidity: function reductionPerCliff() view returns(uint256)
func (_ConvexToken *ConvexTokenSession) ReductionPerCliff() (*big.Int, error) {
	return _ConvexToken.Contract.ReductionPerCliff(&_ConvexToken.CallOpts)
}

// ReductionPerCliff is a free data retrieval call binding the contract method 0xaa74e622.
//
// Solidity: function reductionPerCliff() view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) ReductionPerCliff() (*big.Int, error) {
	return _ConvexToken.Contract.ReductionPerCliff(&_ConvexToken.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ConvexToken *ConvexTokenCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ConvexToken *ConvexTokenSession) Symbol() (string, error) {
	return _ConvexToken.Contract.Symbol(&_ConvexToken.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ConvexToken *ConvexTokenCallerSession) Symbol() (string, error) {
	return _ConvexToken.Contract.Symbol(&_ConvexToken.CallOpts)
}

// TotalCliffs is a free data retrieval call binding the contract method 0x1f96e76f.
//
// Solidity: function totalCliffs() view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) TotalCliffs(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "totalCliffs")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalCliffs is a free data retrieval call binding the contract method 0x1f96e76f.
//
// Solidity: function totalCliffs() view returns(uint256)
func (_ConvexToken *ConvexTokenSession) TotalCliffs() (*big.Int, error) {
	return _ConvexToken.Contract.TotalCliffs(&_ConvexToken.CallOpts)
}

// TotalCliffs is a free data retrieval call binding the contract method 0x1f96e76f.
//
// Solidity: function totalCliffs() view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) TotalCliffs() (*big.Int, error) {
	return _ConvexToken.Contract.TotalCliffs(&_ConvexToken.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenSession) TotalSupply() (*big.Int, error) {
	return _ConvexToken.Contract.TotalSupply(&_ConvexToken.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ConvexToken *ConvexTokenCallerSession) TotalSupply() (*big.Int, error) {
	return _ConvexToken.Contract.TotalSupply(&_ConvexToken.CallOpts)
}

// VecrvProxy is a free data retrieval call binding the contract method 0xfca975a1.
//
// Solidity: function vecrvProxy() view returns(address)
func (_ConvexToken *ConvexTokenCaller) VecrvProxy(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _ConvexToken.contract.Call(opts, &out, "vecrvProxy")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// VecrvProxy is a free data retrieval call binding the contract method 0xfca975a1.
//
// Solidity: function vecrvProxy() view returns(address)
func (_ConvexToken *ConvexTokenSession) VecrvProxy() (common.Address, error) {
	return _ConvexToken.Contract.VecrvProxy(&_ConvexToken.CallOpts)
}

// VecrvProxy is a free data retrieval call binding the contract method 0xfca975a1.
//
// Solidity: function vecrvProxy() view returns(address)
func (_ConvexToken *ConvexTokenCallerSession) VecrvProxy() (common.Address, error) {
	return _ConvexToken.Contract.VecrvProxy(&_ConvexToken.CallOpts)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactor) Approve(opts *bind.TransactOpts, spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "approve", spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenSession) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Approve(&_ConvexToken.TransactOpts, spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactorSession) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Approve(&_ConvexToken.TransactOpts, spender, amount)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ConvexToken *ConvexTokenTransactor) DecreaseAllowance(opts *bind.TransactOpts, spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "decreaseAllowance", spender, subtractedValue)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ConvexToken *ConvexTokenSession) DecreaseAllowance(spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.DecreaseAllowance(&_ConvexToken.TransactOpts, spender, subtractedValue)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ConvexToken *ConvexTokenTransactorSession) DecreaseAllowance(spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.DecreaseAllowance(&_ConvexToken.TransactOpts, spender, subtractedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ConvexToken *ConvexTokenTransactor) IncreaseAllowance(opts *bind.TransactOpts, spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "increaseAllowance", spender, addedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ConvexToken *ConvexTokenSession) IncreaseAllowance(spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.IncreaseAllowance(&_ConvexToken.TransactOpts, spender, addedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ConvexToken *ConvexTokenTransactorSession) IncreaseAllowance(spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.IncreaseAllowance(&_ConvexToken.TransactOpts, spender, addedValue)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address _to, uint256 _amount) returns()
func (_ConvexToken *ConvexTokenTransactor) Mint(opts *bind.TransactOpts, _to common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "mint", _to, _amount)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address _to, uint256 _amount) returns()
func (_ConvexToken *ConvexTokenSession) Mint(_to common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Mint(&_ConvexToken.TransactOpts, _to, _amount)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address _to, uint256 _amount) returns()
func (_ConvexToken *ConvexTokenTransactorSession) Mint(_to common.Address, _amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Mint(&_ConvexToken.TransactOpts, _to, _amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactor) Transfer(opts *bind.TransactOpts, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "transfer", recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenSession) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Transfer(&_ConvexToken.TransactOpts, recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactorSession) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.Transfer(&_ConvexToken.TransactOpts, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactor) TransferFrom(opts *bind.TransactOpts, sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "transferFrom", sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenSession) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.TransferFrom(&_ConvexToken.TransactOpts, sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ConvexToken *ConvexTokenTransactorSession) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ConvexToken.Contract.TransferFrom(&_ConvexToken.TransactOpts, sender, recipient, amount)
}

// UpdateOperator is a paid mutator transaction binding the contract method 0xd5934b76.
//
// Solidity: function updateOperator() returns()
func (_ConvexToken *ConvexTokenTransactor) UpdateOperator(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ConvexToken.contract.Transact(opts, "updateOperator")
}

// UpdateOperator is a paid mutator transaction binding the contract method 0xd5934b76.
//
// Solidity: function updateOperator() returns()
func (_ConvexToken *ConvexTokenSession) UpdateOperator() (*types.Transaction, error) {
	return _ConvexToken.Contract.UpdateOperator(&_ConvexToken.TransactOpts)
}

// UpdateOperator is a paid mutator transaction binding the contract method 0xd5934b76.
//
// Solidity: function updateOperator() returns()
func (_ConvexToken *ConvexTokenTransactorSession) UpdateOperator() (*types.Transaction, error) {
	return _ConvexToken.Contract.UpdateOperator(&_ConvexToken.TransactOpts)
}

// ConvexTokenApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the ConvexToken contract.
type ConvexTokenApprovalIterator struct {
	Event *ConvexTokenApproval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConvexTokenApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConvexTokenApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConvexTokenApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConvexTokenApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConvexTokenApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConvexTokenApproval represents a Approval event raised by the ConvexToken contract.
type ConvexTokenApproval struct {
	Owner   common.Address
	Spender common.Address
	Value   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, spender []common.Address) (*ConvexTokenApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _ConvexToken.contract.FilterLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return &ConvexTokenApprovalIterator{contract: _ConvexToken.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *ConvexTokenApproval, owner []common.Address, spender []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _ConvexToken.contract.WatchLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConvexTokenApproval)
				if err := _ConvexToken.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) ParseApproval(log types.Log) (*ConvexTokenApproval, error) {
	event := new(ConvexTokenApproval)
	if err := _ConvexToken.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ConvexTokenTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the ConvexToken contract.
type ConvexTokenTransferIterator struct {
	Event *ConvexTokenTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ConvexTokenTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ConvexTokenTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ConvexTokenTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ConvexTokenTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ConvexTokenTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ConvexTokenTransfer represents a Transfer event raised by the ConvexToken contract.
type ConvexTokenTransfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*ConvexTokenTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _ConvexToken.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &ConvexTokenTransferIterator{contract: _ConvexToken.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *ConvexTokenTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _ConvexToken.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ConvexTokenTransfer)
				if err := _ConvexToken.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ConvexToken *ConvexTokenFilterer) ParseTransfer(log types.Log) (*ConvexTokenTransfer, error) {
	event := new(ConvexTokenTransfer)
	if err := _ConvexToken.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ERC20MetaData contains all meta data concerning the ERC20 contract.
var ERC20MetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"name_\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"symbol_\",\"type\":\"string\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"subtractedValue\",\"type\":\"uint256\"}],\"name\":\"decreaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"addedValue\",\"type\":\"uint256\"}],\"name\":\"increaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"dd62ed3e": "allowance(address,address)",
		"095ea7b3": "approve(address,uint256)",
		"70a08231": "balanceOf(address)",
		"313ce567": "decimals()",
		"a457c2d7": "decreaseAllowance(address,uint256)",
		"39509351": "increaseAllowance(address,uint256)",
		"06fdde03": "name()",
		"95d89b41": "symbol()",
		"18160ddd": "totalSupply()",
		"a9059cbb": "transfer(address,uint256)",
		"23b872dd": "transferFrom(address,address,uint256)",
	},
	Bin: "0x608060405234801561001057600080fd5b5060405162000c6238038062000c628339818101604052604081101561003557600080fd5b810190808051604051939291908464010000000082111561005557600080fd5b90830190602082018581111561006a57600080fd5b825164010000000081118282018810171561008457600080fd5b82525081516020918201929091019080838360005b838110156100b1578181015183820152602001610099565b50505050905090810190601f1680156100de5780820380516001836020036101000a031916815260200191505b506040526020018051604051939291908464010000000082111561010157600080fd5b90830190602082018581111561011657600080fd5b825164010000000081118282018810171561013057600080fd5b82525081516020918201929091019080838360005b8381101561015d578181015183820152602001610145565b50505050905090810190601f16801561018a5780820380516001836020036101000a031916815260200191505b50604052505082516101a4915060039060208501906101cd565b5080516101b89060049060208401906101cd565b50506005805460ff1916601217905550610260565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061020e57805160ff191683800117855561023b565b8280016001018555821561023b579182015b8281111561023b578251825591602001919060010190610220565b5061024792915061024b565b5090565b5b80821115610247576000815560010161024c565b6109f280620002706000396000f3fe608060405234801561001057600080fd5b50600436106100a95760003560e01c8063395093511161007157806339509351146101d957806370a082311461020557806395d89b411461022b578063a457c2d714610233578063a9059cbb1461025f578063dd62ed3e1461028b576100a9565b806306fdde03146100ae578063095ea7b31461012b57806318160ddd1461016b57806323b872dd14610185578063313ce567146101bb575b600080fd5b6100b66102b9565b6040805160208082528351818301528351919283929083019185019080838360005b838110156100f05781810151838201526020016100d8565b50505050905090810190601f16801561011d5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b6101576004803603604081101561014157600080fd5b506001600160a01b03813516906020013561034f565b604080519115158252519081900360200190f35b61017361036c565b60408051918252519081900360200190f35b6101576004803603606081101561019b57600080fd5b506001600160a01b03813581169160208101359091169060400135610372565b6101c36103f9565b6040805160ff9092168252519081900360200190f35b610157600480360360408110156101ef57600080fd5b506001600160a01b038135169060200135610402565b6101736004803603602081101561021b57600080fd5b50356001600160a01b0316610450565b6100b661046b565b6101576004803603604081101561024957600080fd5b506001600160a01b0381351690602001356104cc565b6101576004803603604081101561027557600080fd5b506001600160a01b038135169060200135610534565b610173600480360360408110156102a157600080fd5b506001600160a01b0381358116916020013516610548565b60038054604080516020601f60026000196101006001881615020190951694909404938401819004810282018101909252828152606093909290918301828280156103455780601f1061031a57610100808354040283529160200191610345565b820191906000526020600020905b81548152906001019060200180831161032857829003601f168201915b5050505050905090565b600061036361035c610573565b8484610577565b50600192915050565b60025490565b600061037f848484610663565b6103ef8461038b610573565b6103ea85604051806060016040528060288152602001610927602891396001600160a01b038a166000908152600160205260408120906103c9610573565b6001600160a01b0316815260208101919091526040016000205491906107be565b610577565b5060019392505050565b60055460ff1690565b600061036361040f610573565b846103ea8560016000610420610573565b6001600160a01b03908116825260208083019390935260409182016000908120918c168152925290205490610855565b6001600160a01b031660009081526020819052604090205490565b60048054604080516020601f60026000196101006001881615020190951694909404938401819004810282018101909252828152606093909290918301828280156103455780601f1061031a57610100808354040283529160200191610345565b60006103636104d9610573565b846103ea856040518060600160405280602581526020016109986025913960016000610503610573565b6001600160a01b03908116825260208083019390935260409182016000908120918d168152925290205491906107be565b6000610363610541610573565b8484610663565b6001600160a01b03918216600090815260016020908152604080832093909416825291909152205490565b3390565b6001600160a01b0383166105bc5760405162461bcd60e51b81526004018080602001828103825260248152602001806109746024913960400191505060405180910390fd5b6001600160a01b0382166106015760405162461bcd60e51b81526004018080602001828103825260228152602001806108df6022913960400191505060405180910390fd5b6001600160a01b03808416600081815260016020908152604080832094871680845294825291829020859055815185815291517f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259281900390910190a3505050565b6001600160a01b0383166106a85760405162461bcd60e51b815260040180806020018281038252602581526020018061094f6025913960400191505060405180910390fd5b6001600160a01b0382166106ed5760405162461bcd60e51b81526004018080602001828103825260238152602001806108bc6023913960400191505060405180910390fd5b6106f88383836108b6565b61073581604051806060016040528060268152602001610901602691396001600160a01b03861660009081526020819052604090205491906107be565b6001600160a01b0380851660009081526020819052604080822093909355908416815220546107649082610855565b6001600160a01b038084166000818152602081815260409182902094909455805185815290519193928716927fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef92918290030190a3505050565b6000818484111561084d5760405162461bcd60e51b81526004018080602001828103825283818151815260200191508051906020019080838360005b838110156108125781810151838201526020016107fa565b50505050905090810190601f16801561083f5780820380516001836020036101000a031916815260200191505b509250505060405180910390fd5b505050900390565b6000828201838110156108af576040805162461bcd60e51b815260206004820152601b60248201527f536166654d6174683a206164646974696f6e206f766572666c6f770000000000604482015290519081900360640190fd5b9392505050565b50505056fe45524332303a207472616e7366657220746f20746865207a65726f206164647265737345524332303a20617070726f766520746f20746865207a65726f206164647265737345524332303a207472616e7366657220616d6f756e7420657863656564732062616c616e636545524332303a207472616e7366657220616d6f756e74206578636565647320616c6c6f77616e636545524332303a207472616e736665722066726f6d20746865207a65726f206164647265737345524332303a20617070726f76652066726f6d20746865207a65726f206164647265737345524332303a2064656372656173656420616c6c6f77616e63652062656c6f77207a65726fa26469706673582212203fae355e1d0e9d7a329370d778ea56481bc923b0551b28dbf16dea08e81812ab64736f6c634300060c0033",
}

// ERC20ABI is the input ABI used to generate the binding from.
// Deprecated: Use ERC20MetaData.ABI instead.
var ERC20ABI = ERC20MetaData.ABI

// Deprecated: Use ERC20MetaData.Sigs instead.
// ERC20FuncSigs maps the 4-byte function signature to its string representation.
var ERC20FuncSigs = ERC20MetaData.Sigs

// ERC20Bin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use ERC20MetaData.Bin instead.
var ERC20Bin = ERC20MetaData.Bin

// DeployERC20 deploys a new Ethereum contract, binding an instance of ERC20 to it.
func DeployERC20(auth *bind.TransactOpts, backend bind.ContractBackend, name_ string, symbol_ string) (common.Address, *types.Transaction, *ERC20, error) {
	parsed, err := ERC20MetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(ERC20Bin), backend, name_, symbol_)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &ERC20{ERC20Caller: ERC20Caller{contract: contract}, ERC20Transactor: ERC20Transactor{contract: contract}, ERC20Filterer: ERC20Filterer{contract: contract}}, nil
}

// ERC20 is an auto generated Go binding around an Ethereum contract.
type ERC20 struct {
	ERC20Caller     // Read-only binding to the contract
	ERC20Transactor // Write-only binding to the contract
	ERC20Filterer   // Log filterer for contract events
}

// ERC20Caller is an auto generated read-only Go binding around an Ethereum contract.
type ERC20Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ERC20Transactor is an auto generated write-only Go binding around an Ethereum contract.
type ERC20Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ERC20Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ERC20Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ERC20Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ERC20Session struct {
	Contract     *ERC20            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ERC20CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ERC20CallerSession struct {
	Contract *ERC20Caller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// ERC20TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ERC20TransactorSession struct {
	Contract     *ERC20Transactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ERC20Raw is an auto generated low-level Go binding around an Ethereum contract.
type ERC20Raw struct {
	Contract *ERC20 // Generic contract binding to access the raw methods on
}

// ERC20CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ERC20CallerRaw struct {
	Contract *ERC20Caller // Generic read-only contract binding to access the raw methods on
}

// ERC20TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ERC20TransactorRaw struct {
	Contract *ERC20Transactor // Generic write-only contract binding to access the raw methods on
}

// NewERC20 creates a new instance of ERC20, bound to a specific deployed contract.
func NewERC20(address common.Address, backend bind.ContractBackend) (*ERC20, error) {
	contract, err := bindERC20(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ERC20{ERC20Caller: ERC20Caller{contract: contract}, ERC20Transactor: ERC20Transactor{contract: contract}, ERC20Filterer: ERC20Filterer{contract: contract}}, nil
}

// NewERC20Caller creates a new read-only instance of ERC20, bound to a specific deployed contract.
func NewERC20Caller(address common.Address, caller bind.ContractCaller) (*ERC20Caller, error) {
	contract, err := bindERC20(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ERC20Caller{contract: contract}, nil
}

// NewERC20Transactor creates a new write-only instance of ERC20, bound to a specific deployed contract.
func NewERC20Transactor(address common.Address, transactor bind.ContractTransactor) (*ERC20Transactor, error) {
	contract, err := bindERC20(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ERC20Transactor{contract: contract}, nil
}

// NewERC20Filterer creates a new log filterer instance of ERC20, bound to a specific deployed contract.
func NewERC20Filterer(address common.Address, filterer bind.ContractFilterer) (*ERC20Filterer, error) {
	contract, err := bindERC20(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ERC20Filterer{contract: contract}, nil
}

// bindERC20 binds a generic wrapper to an already deployed contract.
func bindERC20(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ERC20ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ERC20 *ERC20Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ERC20.Contract.ERC20Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ERC20 *ERC20Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ERC20.Contract.ERC20Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ERC20 *ERC20Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ERC20.Contract.ERC20Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ERC20 *ERC20CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ERC20.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ERC20 *ERC20TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ERC20.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ERC20 *ERC20TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ERC20.Contract.contract.Transact(opts, method, params...)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ERC20 *ERC20Caller) Allowance(opts *bind.CallOpts, owner common.Address, spender common.Address) (*big.Int, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "allowance", owner, spender)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ERC20 *ERC20Session) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _ERC20.Contract.Allowance(&_ERC20.CallOpts, owner, spender)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_ERC20 *ERC20CallerSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _ERC20.Contract.Allowance(&_ERC20.CallOpts, owner, spender)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ERC20 *ERC20Caller) BalanceOf(opts *bind.CallOpts, account common.Address) (*big.Int, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "balanceOf", account)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ERC20 *ERC20Session) BalanceOf(account common.Address) (*big.Int, error) {
	return _ERC20.Contract.BalanceOf(&_ERC20.CallOpts, account)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_ERC20 *ERC20CallerSession) BalanceOf(account common.Address) (*big.Int, error) {
	return _ERC20.Contract.BalanceOf(&_ERC20.CallOpts, account)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ERC20 *ERC20Caller) Decimals(opts *bind.CallOpts) (uint8, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "decimals")

	if err != nil {
		return *new(uint8), err
	}

	out0 := *abi.ConvertType(out[0], new(uint8)).(*uint8)

	return out0, err

}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ERC20 *ERC20Session) Decimals() (uint8, error) {
	return _ERC20.Contract.Decimals(&_ERC20.CallOpts)
}

// Decimals is a free data retrieval call binding the contract method 0x313ce567.
//
// Solidity: function decimals() view returns(uint8)
func (_ERC20 *ERC20CallerSession) Decimals() (uint8, error) {
	return _ERC20.Contract.Decimals(&_ERC20.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ERC20 *ERC20Caller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ERC20 *ERC20Session) Name() (string, error) {
	return _ERC20.Contract.Name(&_ERC20.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_ERC20 *ERC20CallerSession) Name() (string, error) {
	return _ERC20.Contract.Name(&_ERC20.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ERC20 *ERC20Caller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ERC20 *ERC20Session) Symbol() (string, error) {
	return _ERC20.Contract.Symbol(&_ERC20.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_ERC20 *ERC20CallerSession) Symbol() (string, error) {
	return _ERC20.Contract.Symbol(&_ERC20.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ERC20 *ERC20Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ERC20.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ERC20 *ERC20Session) TotalSupply() (*big.Int, error) {
	return _ERC20.Contract.TotalSupply(&_ERC20.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_ERC20 *ERC20CallerSession) TotalSupply() (*big.Int, error) {
	return _ERC20.Contract.TotalSupply(&_ERC20.CallOpts)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ERC20 *ERC20Transactor) Approve(opts *bind.TransactOpts, spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.contract.Transact(opts, "approve", spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ERC20 *ERC20Session) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.Approve(&_ERC20.TransactOpts, spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_ERC20 *ERC20TransactorSession) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.Approve(&_ERC20.TransactOpts, spender, amount)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ERC20 *ERC20Transactor) DecreaseAllowance(opts *bind.TransactOpts, spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.contract.Transact(opts, "decreaseAllowance", spender, subtractedValue)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ERC20 *ERC20Session) DecreaseAllowance(spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.DecreaseAllowance(&_ERC20.TransactOpts, spender, subtractedValue)
}

// DecreaseAllowance is a paid mutator transaction binding the contract method 0xa457c2d7.
//
// Solidity: function decreaseAllowance(address spender, uint256 subtractedValue) returns(bool)
func (_ERC20 *ERC20TransactorSession) DecreaseAllowance(spender common.Address, subtractedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.DecreaseAllowance(&_ERC20.TransactOpts, spender, subtractedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ERC20 *ERC20Transactor) IncreaseAllowance(opts *bind.TransactOpts, spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.contract.Transact(opts, "increaseAllowance", spender, addedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ERC20 *ERC20Session) IncreaseAllowance(spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.IncreaseAllowance(&_ERC20.TransactOpts, spender, addedValue)
}

// IncreaseAllowance is a paid mutator transaction binding the contract method 0x39509351.
//
// Solidity: function increaseAllowance(address spender, uint256 addedValue) returns(bool)
func (_ERC20 *ERC20TransactorSession) IncreaseAllowance(spender common.Address, addedValue *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.IncreaseAllowance(&_ERC20.TransactOpts, spender, addedValue)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20Transactor) Transfer(opts *bind.TransactOpts, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.contract.Transact(opts, "transfer", recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20Session) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.Transfer(&_ERC20.TransactOpts, recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20TransactorSession) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.Transfer(&_ERC20.TransactOpts, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20Transactor) TransferFrom(opts *bind.TransactOpts, sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.contract.Transact(opts, "transferFrom", sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20Session) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.TransferFrom(&_ERC20.TransactOpts, sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_ERC20 *ERC20TransactorSession) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _ERC20.Contract.TransferFrom(&_ERC20.TransactOpts, sender, recipient, amount)
}

// ERC20ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the ERC20 contract.
type ERC20ApprovalIterator struct {
	Event *ERC20Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ERC20ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ERC20Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ERC20Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ERC20ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ERC20ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ERC20Approval represents a Approval event raised by the ERC20 contract.
type ERC20Approval struct {
	Owner   common.Address
	Spender common.Address
	Value   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ERC20 *ERC20Filterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, spender []common.Address) (*ERC20ApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _ERC20.contract.FilterLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return &ERC20ApprovalIterator{contract: _ERC20.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ERC20 *ERC20Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *ERC20Approval, owner []common.Address, spender []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _ERC20.contract.WatchLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ERC20Approval)
				if err := _ERC20.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_ERC20 *ERC20Filterer) ParseApproval(log types.Log) (*ERC20Approval, error) {
	event := new(ERC20Approval)
	if err := _ERC20.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ERC20TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the ERC20 contract.
type ERC20TransferIterator struct {
	Event *ERC20Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *ERC20TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(ERC20Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(ERC20Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *ERC20TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *ERC20TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// ERC20Transfer represents a Transfer event raised by the ERC20 contract.
type ERC20Transfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ERC20 *ERC20Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*ERC20TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _ERC20.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &ERC20TransferIterator{contract: _ERC20.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ERC20 *ERC20Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *ERC20Transfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _ERC20.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(ERC20Transfer)
				if err := _ERC20.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_ERC20 *ERC20Filterer) ParseTransfer(log types.Log) (*ERC20Transfer, error) {
	event := new(ERC20Transfer)
	if err := _ERC20.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// ICrvDepositMetaData contains all meta data concerning the ICrvDeposit contract.
var ICrvDepositMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"lockIncentive\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"9a408321": "deposit(uint256,bool)",
		"50940618": "lockIncentive()",
	},
}

// ICrvDepositABI is the input ABI used to generate the binding from.
// Deprecated: Use ICrvDepositMetaData.ABI instead.
var ICrvDepositABI = ICrvDepositMetaData.ABI

// Deprecated: Use ICrvDepositMetaData.Sigs instead.
// ICrvDepositFuncSigs maps the 4-byte function signature to its string representation.
var ICrvDepositFuncSigs = ICrvDepositMetaData.Sigs

// ICrvDeposit is an auto generated Go binding around an Ethereum contract.
type ICrvDeposit struct {
	ICrvDepositCaller     // Read-only binding to the contract
	ICrvDepositTransactor // Write-only binding to the contract
	ICrvDepositFilterer   // Log filterer for contract events
}

// ICrvDepositCaller is an auto generated read-only Go binding around an Ethereum contract.
type ICrvDepositCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICrvDepositTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ICrvDepositTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICrvDepositFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ICrvDepositFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICrvDepositSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ICrvDepositSession struct {
	Contract     *ICrvDeposit      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ICrvDepositCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ICrvDepositCallerSession struct {
	Contract *ICrvDepositCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// ICrvDepositTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ICrvDepositTransactorSession struct {
	Contract     *ICrvDepositTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// ICrvDepositRaw is an auto generated low-level Go binding around an Ethereum contract.
type ICrvDepositRaw struct {
	Contract *ICrvDeposit // Generic contract binding to access the raw methods on
}

// ICrvDepositCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ICrvDepositCallerRaw struct {
	Contract *ICrvDepositCaller // Generic read-only contract binding to access the raw methods on
}

// ICrvDepositTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ICrvDepositTransactorRaw struct {
	Contract *ICrvDepositTransactor // Generic write-only contract binding to access the raw methods on
}

// NewICrvDeposit creates a new instance of ICrvDeposit, bound to a specific deployed contract.
func NewICrvDeposit(address common.Address, backend bind.ContractBackend) (*ICrvDeposit, error) {
	contract, err := bindICrvDeposit(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ICrvDeposit{ICrvDepositCaller: ICrvDepositCaller{contract: contract}, ICrvDepositTransactor: ICrvDepositTransactor{contract: contract}, ICrvDepositFilterer: ICrvDepositFilterer{contract: contract}}, nil
}

// NewICrvDepositCaller creates a new read-only instance of ICrvDeposit, bound to a specific deployed contract.
func NewICrvDepositCaller(address common.Address, caller bind.ContractCaller) (*ICrvDepositCaller, error) {
	contract, err := bindICrvDeposit(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ICrvDepositCaller{contract: contract}, nil
}

// NewICrvDepositTransactor creates a new write-only instance of ICrvDeposit, bound to a specific deployed contract.
func NewICrvDepositTransactor(address common.Address, transactor bind.ContractTransactor) (*ICrvDepositTransactor, error) {
	contract, err := bindICrvDeposit(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ICrvDepositTransactor{contract: contract}, nil
}

// NewICrvDepositFilterer creates a new log filterer instance of ICrvDeposit, bound to a specific deployed contract.
func NewICrvDepositFilterer(address common.Address, filterer bind.ContractFilterer) (*ICrvDepositFilterer, error) {
	contract, err := bindICrvDeposit(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ICrvDepositFilterer{contract: contract}, nil
}

// bindICrvDeposit binds a generic wrapper to an already deployed contract.
func bindICrvDeposit(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ICrvDepositABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICrvDeposit *ICrvDepositRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICrvDeposit.Contract.ICrvDepositCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICrvDeposit *ICrvDepositRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.ICrvDepositTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICrvDeposit *ICrvDepositRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.ICrvDepositTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICrvDeposit *ICrvDepositCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICrvDeposit.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICrvDeposit *ICrvDepositTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICrvDeposit *ICrvDepositTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.contract.Transact(opts, method, params...)
}

// LockIncentive is a free data retrieval call binding the contract method 0x50940618.
//
// Solidity: function lockIncentive() view returns(uint256)
func (_ICrvDeposit *ICrvDepositCaller) LockIncentive(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _ICrvDeposit.contract.Call(opts, &out, "lockIncentive")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// LockIncentive is a free data retrieval call binding the contract method 0x50940618.
//
// Solidity: function lockIncentive() view returns(uint256)
func (_ICrvDeposit *ICrvDepositSession) LockIncentive() (*big.Int, error) {
	return _ICrvDeposit.Contract.LockIncentive(&_ICrvDeposit.CallOpts)
}

// LockIncentive is a free data retrieval call binding the contract method 0x50940618.
//
// Solidity: function lockIncentive() view returns(uint256)
func (_ICrvDeposit *ICrvDepositCallerSession) LockIncentive() (*big.Int, error) {
	return _ICrvDeposit.Contract.LockIncentive(&_ICrvDeposit.CallOpts)
}

// Deposit is a paid mutator transaction binding the contract method 0x9a408321.
//
// Solidity: function deposit(uint256 , bool ) returns()
func (_ICrvDeposit *ICrvDepositTransactor) Deposit(opts *bind.TransactOpts, arg0 *big.Int, arg1 bool) (*types.Transaction, error) {
	return _ICrvDeposit.contract.Transact(opts, "deposit", arg0, arg1)
}

// Deposit is a paid mutator transaction binding the contract method 0x9a408321.
//
// Solidity: function deposit(uint256 , bool ) returns()
func (_ICrvDeposit *ICrvDepositSession) Deposit(arg0 *big.Int, arg1 bool) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.Deposit(&_ICrvDeposit.TransactOpts, arg0, arg1)
}

// Deposit is a paid mutator transaction binding the contract method 0x9a408321.
//
// Solidity: function deposit(uint256 , bool ) returns()
func (_ICrvDeposit *ICrvDepositTransactorSession) Deposit(arg0 *big.Int, arg1 bool) (*types.Transaction, error) {
	return _ICrvDeposit.Contract.Deposit(&_ICrvDeposit.TransactOpts, arg0, arg1)
}

// ICurveGaugeMetaData contains all meta data concerning the ICurveGauge contract.
var ICurveGaugeMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"claim_rewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"reward_tokens\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"rewarded_token\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"70a08231": "balanceOf(address)",
		"e6f1daf2": "claim_rewards()",
		"b6b55f25": "deposit(uint256)",
		"54c49fe9": "reward_tokens(uint256)",
		"16fa50b1": "rewarded_token()",
		"2e1a7d4d": "withdraw(uint256)",
	},
}

// ICurveGaugeABI is the input ABI used to generate the binding from.
// Deprecated: Use ICurveGaugeMetaData.ABI instead.
var ICurveGaugeABI = ICurveGaugeMetaData.ABI

// Deprecated: Use ICurveGaugeMetaData.Sigs instead.
// ICurveGaugeFuncSigs maps the 4-byte function signature to its string representation.
var ICurveGaugeFuncSigs = ICurveGaugeMetaData.Sigs

// ICurveGauge is an auto generated Go binding around an Ethereum contract.
type ICurveGauge struct {
	ICurveGaugeCaller     // Read-only binding to the contract
	ICurveGaugeTransactor // Write-only binding to the contract
	ICurveGaugeFilterer   // Log filterer for contract events
}

// ICurveGaugeCaller is an auto generated read-only Go binding around an Ethereum contract.
type ICurveGaugeCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveGaugeTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ICurveGaugeTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveGaugeFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ICurveGaugeFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveGaugeSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ICurveGaugeSession struct {
	Contract     *ICurveGauge      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ICurveGaugeCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ICurveGaugeCallerSession struct {
	Contract *ICurveGaugeCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// ICurveGaugeTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ICurveGaugeTransactorSession struct {
	Contract     *ICurveGaugeTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// ICurveGaugeRaw is an auto generated low-level Go binding around an Ethereum contract.
type ICurveGaugeRaw struct {
	Contract *ICurveGauge // Generic contract binding to access the raw methods on
}

// ICurveGaugeCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ICurveGaugeCallerRaw struct {
	Contract *ICurveGaugeCaller // Generic read-only contract binding to access the raw methods on
}

// ICurveGaugeTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ICurveGaugeTransactorRaw struct {
	Contract *ICurveGaugeTransactor // Generic write-only contract binding to access the raw methods on
}

// NewICurveGauge creates a new instance of ICurveGauge, bound to a specific deployed contract.
func NewICurveGauge(address common.Address, backend bind.ContractBackend) (*ICurveGauge, error) {
	contract, err := bindICurveGauge(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ICurveGauge{ICurveGaugeCaller: ICurveGaugeCaller{contract: contract}, ICurveGaugeTransactor: ICurveGaugeTransactor{contract: contract}, ICurveGaugeFilterer: ICurveGaugeFilterer{contract: contract}}, nil
}

// NewICurveGaugeCaller creates a new read-only instance of ICurveGauge, bound to a specific deployed contract.
func NewICurveGaugeCaller(address common.Address, caller bind.ContractCaller) (*ICurveGaugeCaller, error) {
	contract, err := bindICurveGauge(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ICurveGaugeCaller{contract: contract}, nil
}

// NewICurveGaugeTransactor creates a new write-only instance of ICurveGauge, bound to a specific deployed contract.
func NewICurveGaugeTransactor(address common.Address, transactor bind.ContractTransactor) (*ICurveGaugeTransactor, error) {
	contract, err := bindICurveGauge(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ICurveGaugeTransactor{contract: contract}, nil
}

// NewICurveGaugeFilterer creates a new log filterer instance of ICurveGauge, bound to a specific deployed contract.
func NewICurveGaugeFilterer(address common.Address, filterer bind.ContractFilterer) (*ICurveGaugeFilterer, error) {
	contract, err := bindICurveGauge(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ICurveGaugeFilterer{contract: contract}, nil
}

// bindICurveGauge binds a generic wrapper to an already deployed contract.
func bindICurveGauge(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ICurveGaugeABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICurveGauge *ICurveGaugeRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICurveGauge.Contract.ICurveGaugeCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICurveGauge *ICurveGaugeRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveGauge.Contract.ICurveGaugeTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICurveGauge *ICurveGaugeRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICurveGauge.Contract.ICurveGaugeTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICurveGauge *ICurveGaugeCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICurveGauge.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICurveGauge *ICurveGaugeTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveGauge.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICurveGauge *ICurveGaugeTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICurveGauge.Contract.contract.Transact(opts, method, params...)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_ICurveGauge *ICurveGaugeCaller) BalanceOf(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _ICurveGauge.contract.Call(opts, &out, "balanceOf", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_ICurveGauge *ICurveGaugeSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _ICurveGauge.Contract.BalanceOf(&_ICurveGauge.CallOpts, arg0)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address ) view returns(uint256)
func (_ICurveGauge *ICurveGaugeCallerSession) BalanceOf(arg0 common.Address) (*big.Int, error) {
	return _ICurveGauge.Contract.BalanceOf(&_ICurveGauge.CallOpts, arg0)
}

// RewardTokens is a free data retrieval call binding the contract method 0x54c49fe9.
//
// Solidity: function reward_tokens(uint256 ) view returns(address)
func (_ICurveGauge *ICurveGaugeCaller) RewardTokens(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _ICurveGauge.contract.Call(opts, &out, "reward_tokens", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// RewardTokens is a free data retrieval call binding the contract method 0x54c49fe9.
//
// Solidity: function reward_tokens(uint256 ) view returns(address)
func (_ICurveGauge *ICurveGaugeSession) RewardTokens(arg0 *big.Int) (common.Address, error) {
	return _ICurveGauge.Contract.RewardTokens(&_ICurveGauge.CallOpts, arg0)
}

// RewardTokens is a free data retrieval call binding the contract method 0x54c49fe9.
//
// Solidity: function reward_tokens(uint256 ) view returns(address)
func (_ICurveGauge *ICurveGaugeCallerSession) RewardTokens(arg0 *big.Int) (common.Address, error) {
	return _ICurveGauge.Contract.RewardTokens(&_ICurveGauge.CallOpts, arg0)
}

// RewardedToken is a free data retrieval call binding the contract method 0x16fa50b1.
//
// Solidity: function rewarded_token() view returns(address)
func (_ICurveGauge *ICurveGaugeCaller) RewardedToken(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _ICurveGauge.contract.Call(opts, &out, "rewarded_token")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// RewardedToken is a free data retrieval call binding the contract method 0x16fa50b1.
//
// Solidity: function rewarded_token() view returns(address)
func (_ICurveGauge *ICurveGaugeSession) RewardedToken() (common.Address, error) {
	return _ICurveGauge.Contract.RewardedToken(&_ICurveGauge.CallOpts)
}

// RewardedToken is a free data retrieval call binding the contract method 0x16fa50b1.
//
// Solidity: function rewarded_token() view returns(address)
func (_ICurveGauge *ICurveGaugeCallerSession) RewardedToken() (common.Address, error) {
	return _ICurveGauge.Contract.RewardedToken(&_ICurveGauge.CallOpts)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xe6f1daf2.
//
// Solidity: function claim_rewards() returns()
func (_ICurveGauge *ICurveGaugeTransactor) ClaimRewards(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveGauge.contract.Transact(opts, "claim_rewards")
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xe6f1daf2.
//
// Solidity: function claim_rewards() returns()
func (_ICurveGauge *ICurveGaugeSession) ClaimRewards() (*types.Transaction, error) {
	return _ICurveGauge.Contract.ClaimRewards(&_ICurveGauge.TransactOpts)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xe6f1daf2.
//
// Solidity: function claim_rewards() returns()
func (_ICurveGauge *ICurveGaugeTransactorSession) ClaimRewards() (*types.Transaction, error) {
	return _ICurveGauge.Contract.ClaimRewards(&_ICurveGauge.TransactOpts)
}

// Deposit is a paid mutator transaction binding the contract method 0xb6b55f25.
//
// Solidity: function deposit(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeTransactor) Deposit(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.contract.Transact(opts, "deposit", arg0)
}

// Deposit is a paid mutator transaction binding the contract method 0xb6b55f25.
//
// Solidity: function deposit(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeSession) Deposit(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.Contract.Deposit(&_ICurveGauge.TransactOpts, arg0)
}

// Deposit is a paid mutator transaction binding the contract method 0xb6b55f25.
//
// Solidity: function deposit(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeTransactorSession) Deposit(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.Contract.Deposit(&_ICurveGauge.TransactOpts, arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeTransactor) Withdraw(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.contract.Transact(opts, "withdraw", arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeSession) Withdraw(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.Contract.Withdraw(&_ICurveGauge.TransactOpts, arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 ) returns()
func (_ICurveGauge *ICurveGaugeTransactorSession) Withdraw(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveGauge.Contract.Withdraw(&_ICurveGauge.TransactOpts, arg0)
}

// ICurveVoteEscrowMetaData contains all meta data concerning the ICurveVoteEscrow contract.
var ICurveVoteEscrowMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"create_lock\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"increase_amount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"increase_unlock_time\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"smart_wallet_checker\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"65fc3873": "create_lock(uint256,uint256)",
		"4957677c": "increase_amount(uint256)",
		"eff7a612": "increase_unlock_time(uint256)",
		"7175d4f7": "smart_wallet_checker()",
		"3ccfd60b": "withdraw()",
	},
}

// ICurveVoteEscrowABI is the input ABI used to generate the binding from.
// Deprecated: Use ICurveVoteEscrowMetaData.ABI instead.
var ICurveVoteEscrowABI = ICurveVoteEscrowMetaData.ABI

// Deprecated: Use ICurveVoteEscrowMetaData.Sigs instead.
// ICurveVoteEscrowFuncSigs maps the 4-byte function signature to its string representation.
var ICurveVoteEscrowFuncSigs = ICurveVoteEscrowMetaData.Sigs

// ICurveVoteEscrow is an auto generated Go binding around an Ethereum contract.
type ICurveVoteEscrow struct {
	ICurveVoteEscrowCaller     // Read-only binding to the contract
	ICurveVoteEscrowTransactor // Write-only binding to the contract
	ICurveVoteEscrowFilterer   // Log filterer for contract events
}

// ICurveVoteEscrowCaller is an auto generated read-only Go binding around an Ethereum contract.
type ICurveVoteEscrowCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveVoteEscrowTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ICurveVoteEscrowTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveVoteEscrowFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ICurveVoteEscrowFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ICurveVoteEscrowSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ICurveVoteEscrowSession struct {
	Contract     *ICurveVoteEscrow // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ICurveVoteEscrowCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ICurveVoteEscrowCallerSession struct {
	Contract *ICurveVoteEscrowCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts           // Call options to use throughout this session
}

// ICurveVoteEscrowTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ICurveVoteEscrowTransactorSession struct {
	Contract     *ICurveVoteEscrowTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts           // Transaction auth options to use throughout this session
}

// ICurveVoteEscrowRaw is an auto generated low-level Go binding around an Ethereum contract.
type ICurveVoteEscrowRaw struct {
	Contract *ICurveVoteEscrow // Generic contract binding to access the raw methods on
}

// ICurveVoteEscrowCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ICurveVoteEscrowCallerRaw struct {
	Contract *ICurveVoteEscrowCaller // Generic read-only contract binding to access the raw methods on
}

// ICurveVoteEscrowTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ICurveVoteEscrowTransactorRaw struct {
	Contract *ICurveVoteEscrowTransactor // Generic write-only contract binding to access the raw methods on
}

// NewICurveVoteEscrow creates a new instance of ICurveVoteEscrow, bound to a specific deployed contract.
func NewICurveVoteEscrow(address common.Address, backend bind.ContractBackend) (*ICurveVoteEscrow, error) {
	contract, err := bindICurveVoteEscrow(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ICurveVoteEscrow{ICurveVoteEscrowCaller: ICurveVoteEscrowCaller{contract: contract}, ICurveVoteEscrowTransactor: ICurveVoteEscrowTransactor{contract: contract}, ICurveVoteEscrowFilterer: ICurveVoteEscrowFilterer{contract: contract}}, nil
}

// NewICurveVoteEscrowCaller creates a new read-only instance of ICurveVoteEscrow, bound to a specific deployed contract.
func NewICurveVoteEscrowCaller(address common.Address, caller bind.ContractCaller) (*ICurveVoteEscrowCaller, error) {
	contract, err := bindICurveVoteEscrow(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ICurveVoteEscrowCaller{contract: contract}, nil
}

// NewICurveVoteEscrowTransactor creates a new write-only instance of ICurveVoteEscrow, bound to a specific deployed contract.
func NewICurveVoteEscrowTransactor(address common.Address, transactor bind.ContractTransactor) (*ICurveVoteEscrowTransactor, error) {
	contract, err := bindICurveVoteEscrow(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ICurveVoteEscrowTransactor{contract: contract}, nil
}

// NewICurveVoteEscrowFilterer creates a new log filterer instance of ICurveVoteEscrow, bound to a specific deployed contract.
func NewICurveVoteEscrowFilterer(address common.Address, filterer bind.ContractFilterer) (*ICurveVoteEscrowFilterer, error) {
	contract, err := bindICurveVoteEscrow(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ICurveVoteEscrowFilterer{contract: contract}, nil
}

// bindICurveVoteEscrow binds a generic wrapper to an already deployed contract.
func bindICurveVoteEscrow(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ICurveVoteEscrowABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICurveVoteEscrow *ICurveVoteEscrowRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICurveVoteEscrow.Contract.ICurveVoteEscrowCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICurveVoteEscrow *ICurveVoteEscrowRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.ICurveVoteEscrowTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICurveVoteEscrow *ICurveVoteEscrowRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.ICurveVoteEscrowTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ICurveVoteEscrow *ICurveVoteEscrowCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ICurveVoteEscrow.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.contract.Transact(opts, method, params...)
}

// SmartWalletChecker is a free data retrieval call binding the contract method 0x7175d4f7.
//
// Solidity: function smart_wallet_checker() view returns(address)
func (_ICurveVoteEscrow *ICurveVoteEscrowCaller) SmartWalletChecker(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _ICurveVoteEscrow.contract.Call(opts, &out, "smart_wallet_checker")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// SmartWalletChecker is a free data retrieval call binding the contract method 0x7175d4f7.
//
// Solidity: function smart_wallet_checker() view returns(address)
func (_ICurveVoteEscrow *ICurveVoteEscrowSession) SmartWalletChecker() (common.Address, error) {
	return _ICurveVoteEscrow.Contract.SmartWalletChecker(&_ICurveVoteEscrow.CallOpts)
}

// SmartWalletChecker is a free data retrieval call binding the contract method 0x7175d4f7.
//
// Solidity: function smart_wallet_checker() view returns(address)
func (_ICurveVoteEscrow *ICurveVoteEscrowCallerSession) SmartWalletChecker() (common.Address, error) {
	return _ICurveVoteEscrow.Contract.SmartWalletChecker(&_ICurveVoteEscrow.CallOpts)
}

// CreateLock is a paid mutator transaction binding the contract method 0x65fc3873.
//
// Solidity: function create_lock(uint256 , uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactor) CreateLock(opts *bind.TransactOpts, arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.contract.Transact(opts, "create_lock", arg0, arg1)
}

// CreateLock is a paid mutator transaction binding the contract method 0x65fc3873.
//
// Solidity: function create_lock(uint256 , uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowSession) CreateLock(arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.CreateLock(&_ICurveVoteEscrow.TransactOpts, arg0, arg1)
}

// CreateLock is a paid mutator transaction binding the contract method 0x65fc3873.
//
// Solidity: function create_lock(uint256 , uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorSession) CreateLock(arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.CreateLock(&_ICurveVoteEscrow.TransactOpts, arg0, arg1)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x4957677c.
//
// Solidity: function increase_amount(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactor) IncreaseAmount(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.contract.Transact(opts, "increase_amount", arg0)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x4957677c.
//
// Solidity: function increase_amount(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowSession) IncreaseAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.IncreaseAmount(&_ICurveVoteEscrow.TransactOpts, arg0)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x4957677c.
//
// Solidity: function increase_amount(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorSession) IncreaseAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.IncreaseAmount(&_ICurveVoteEscrow.TransactOpts, arg0)
}

// IncreaseUnlockTime is a paid mutator transaction binding the contract method 0xeff7a612.
//
// Solidity: function increase_unlock_time(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactor) IncreaseUnlockTime(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.contract.Transact(opts, "increase_unlock_time", arg0)
}

// IncreaseUnlockTime is a paid mutator transaction binding the contract method 0xeff7a612.
//
// Solidity: function increase_unlock_time(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowSession) IncreaseUnlockTime(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.IncreaseUnlockTime(&_ICurveVoteEscrow.TransactOpts, arg0)
}

// IncreaseUnlockTime is a paid mutator transaction binding the contract method 0xeff7a612.
//
// Solidity: function increase_unlock_time(uint256 ) returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorSession) IncreaseUnlockTime(arg0 *big.Int) (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.IncreaseUnlockTime(&_ICurveVoteEscrow.TransactOpts, arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ICurveVoteEscrow.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowSession) Withdraw() (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.Withdraw(&_ICurveVoteEscrow.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_ICurveVoteEscrow *ICurveVoteEscrowTransactorSession) Withdraw() (*types.Transaction, error) {
	return _ICurveVoteEscrow.Contract.Withdraw(&_ICurveVoteEscrow.TransactOpts)
}

// IDepositMetaData contains all meta data concerning the IDeposit contract.
var IDepositMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_account\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"claimRewards\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isShutdown\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"rewardArbitrator\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"rewardClaimed\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"withdrawTo\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"70a08231": "balanceOf(address)",
		"6c7b69cb": "claimRewards(uint256,address)",
		"bf86d690": "isShutdown()",
		"1526fe27": "poolInfo(uint256)",
		"043b684a": "rewardArbitrator()",
		"71192b17": "rewardClaimed(uint256,address,uint256)",
		"18160ddd": "totalSupply()",
		"14cd70e4": "withdrawTo(uint256,uint256,address)",
	},
}

// IDepositABI is the input ABI used to generate the binding from.
// Deprecated: Use IDepositMetaData.ABI instead.
var IDepositABI = IDepositMetaData.ABI

// Deprecated: Use IDepositMetaData.Sigs instead.
// IDepositFuncSigs maps the 4-byte function signature to its string representation.
var IDepositFuncSigs = IDepositMetaData.Sigs

// IDeposit is an auto generated Go binding around an Ethereum contract.
type IDeposit struct {
	IDepositCaller     // Read-only binding to the contract
	IDepositTransactor // Write-only binding to the contract
	IDepositFilterer   // Log filterer for contract events
}

// IDepositCaller is an auto generated read-only Go binding around an Ethereum contract.
type IDepositCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IDepositTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IDepositTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IDepositFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IDepositFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IDepositSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IDepositSession struct {
	Contract     *IDeposit         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IDepositCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IDepositCallerSession struct {
	Contract *IDepositCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// IDepositTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IDepositTransactorSession struct {
	Contract     *IDepositTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// IDepositRaw is an auto generated low-level Go binding around an Ethereum contract.
type IDepositRaw struct {
	Contract *IDeposit // Generic contract binding to access the raw methods on
}

// IDepositCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IDepositCallerRaw struct {
	Contract *IDepositCaller // Generic read-only contract binding to access the raw methods on
}

// IDepositTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IDepositTransactorRaw struct {
	Contract *IDepositTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIDeposit creates a new instance of IDeposit, bound to a specific deployed contract.
func NewIDeposit(address common.Address, backend bind.ContractBackend) (*IDeposit, error) {
	contract, err := bindIDeposit(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IDeposit{IDepositCaller: IDepositCaller{contract: contract}, IDepositTransactor: IDepositTransactor{contract: contract}, IDepositFilterer: IDepositFilterer{contract: contract}}, nil
}

// NewIDepositCaller creates a new read-only instance of IDeposit, bound to a specific deployed contract.
func NewIDepositCaller(address common.Address, caller bind.ContractCaller) (*IDepositCaller, error) {
	contract, err := bindIDeposit(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IDepositCaller{contract: contract}, nil
}

// NewIDepositTransactor creates a new write-only instance of IDeposit, bound to a specific deployed contract.
func NewIDepositTransactor(address common.Address, transactor bind.ContractTransactor) (*IDepositTransactor, error) {
	contract, err := bindIDeposit(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IDepositTransactor{contract: contract}, nil
}

// NewIDepositFilterer creates a new log filterer instance of IDeposit, bound to a specific deployed contract.
func NewIDepositFilterer(address common.Address, filterer bind.ContractFilterer) (*IDepositFilterer, error) {
	contract, err := bindIDeposit(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IDepositFilterer{contract: contract}, nil
}

// bindIDeposit binds a generic wrapper to an already deployed contract.
func bindIDeposit(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IDepositABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IDeposit *IDepositRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IDeposit.Contract.IDepositCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IDeposit *IDepositRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IDeposit.Contract.IDepositTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IDeposit *IDepositRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IDeposit.Contract.IDepositTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IDeposit *IDepositCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IDeposit.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IDeposit *IDepositTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IDeposit.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IDeposit *IDepositTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IDeposit.Contract.contract.Transact(opts, method, params...)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _account) view returns(uint256)
func (_IDeposit *IDepositCaller) BalanceOf(opts *bind.CallOpts, _account common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IDeposit.contract.Call(opts, &out, "balanceOf", _account)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _account) view returns(uint256)
func (_IDeposit *IDepositSession) BalanceOf(_account common.Address) (*big.Int, error) {
	return _IDeposit.Contract.BalanceOf(&_IDeposit.CallOpts, _account)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address _account) view returns(uint256)
func (_IDeposit *IDepositCallerSession) BalanceOf(_account common.Address) (*big.Int, error) {
	return _IDeposit.Contract.BalanceOf(&_IDeposit.CallOpts, _account)
}

// IsShutdown is a free data retrieval call binding the contract method 0xbf86d690.
//
// Solidity: function isShutdown() view returns(bool)
func (_IDeposit *IDepositCaller) IsShutdown(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _IDeposit.contract.Call(opts, &out, "isShutdown")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsShutdown is a free data retrieval call binding the contract method 0xbf86d690.
//
// Solidity: function isShutdown() view returns(bool)
func (_IDeposit *IDepositSession) IsShutdown() (bool, error) {
	return _IDeposit.Contract.IsShutdown(&_IDeposit.CallOpts)
}

// IsShutdown is a free data retrieval call binding the contract method 0xbf86d690.
//
// Solidity: function isShutdown() view returns(bool)
func (_IDeposit *IDepositCallerSession) IsShutdown() (bool, error) {
	return _IDeposit.Contract.IsShutdown(&_IDeposit.CallOpts)
}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IDeposit *IDepositCaller) PoolInfo(opts *bind.CallOpts, arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	var out []interface{}
	err := _IDeposit.contract.Call(opts, &out, "poolInfo", arg0)

	if err != nil {
		return *new(common.Address), *new(common.Address), *new(common.Address), *new(common.Address), *new(common.Address), *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	out1 := *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	out2 := *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	out3 := *abi.ConvertType(out[3], new(common.Address)).(*common.Address)
	out4 := *abi.ConvertType(out[4], new(common.Address)).(*common.Address)
	out5 := *abi.ConvertType(out[5], new(bool)).(*bool)

	return out0, out1, out2, out3, out4, out5, err

}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IDeposit *IDepositSession) PoolInfo(arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	return _IDeposit.Contract.PoolInfo(&_IDeposit.CallOpts, arg0)
}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IDeposit *IDepositCallerSession) PoolInfo(arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	return _IDeposit.Contract.PoolInfo(&_IDeposit.CallOpts, arg0)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IDeposit *IDepositCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IDeposit.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IDeposit *IDepositSession) TotalSupply() (*big.Int, error) {
	return _IDeposit.Contract.TotalSupply(&_IDeposit.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IDeposit *IDepositCallerSession) TotalSupply() (*big.Int, error) {
	return _IDeposit.Contract.TotalSupply(&_IDeposit.CallOpts)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x6c7b69cb.
//
// Solidity: function claimRewards(uint256 , address ) returns(bool)
func (_IDeposit *IDepositTransactor) ClaimRewards(opts *bind.TransactOpts, arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IDeposit.contract.Transact(opts, "claimRewards", arg0, arg1)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x6c7b69cb.
//
// Solidity: function claimRewards(uint256 , address ) returns(bool)
func (_IDeposit *IDepositSession) ClaimRewards(arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IDeposit.Contract.ClaimRewards(&_IDeposit.TransactOpts, arg0, arg1)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x6c7b69cb.
//
// Solidity: function claimRewards(uint256 , address ) returns(bool)
func (_IDeposit *IDepositTransactorSession) ClaimRewards(arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IDeposit.Contract.ClaimRewards(&_IDeposit.TransactOpts, arg0, arg1)
}

// RewardArbitrator is a paid mutator transaction binding the contract method 0x043b684a.
//
// Solidity: function rewardArbitrator() returns(address)
func (_IDeposit *IDepositTransactor) RewardArbitrator(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IDeposit.contract.Transact(opts, "rewardArbitrator")
}

// RewardArbitrator is a paid mutator transaction binding the contract method 0x043b684a.
//
// Solidity: function rewardArbitrator() returns(address)
func (_IDeposit *IDepositSession) RewardArbitrator() (*types.Transaction, error) {
	return _IDeposit.Contract.RewardArbitrator(&_IDeposit.TransactOpts)
}

// RewardArbitrator is a paid mutator transaction binding the contract method 0x043b684a.
//
// Solidity: function rewardArbitrator() returns(address)
func (_IDeposit *IDepositTransactorSession) RewardArbitrator() (*types.Transaction, error) {
	return _IDeposit.Contract.RewardArbitrator(&_IDeposit.TransactOpts)
}

// RewardClaimed is a paid mutator transaction binding the contract method 0x71192b17.
//
// Solidity: function rewardClaimed(uint256 , address , uint256 ) returns()
func (_IDeposit *IDepositTransactor) RewardClaimed(opts *bind.TransactOpts, arg0 *big.Int, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IDeposit.contract.Transact(opts, "rewardClaimed", arg0, arg1, arg2)
}

// RewardClaimed is a paid mutator transaction binding the contract method 0x71192b17.
//
// Solidity: function rewardClaimed(uint256 , address , uint256 ) returns()
func (_IDeposit *IDepositSession) RewardClaimed(arg0 *big.Int, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IDeposit.Contract.RewardClaimed(&_IDeposit.TransactOpts, arg0, arg1, arg2)
}

// RewardClaimed is a paid mutator transaction binding the contract method 0x71192b17.
//
// Solidity: function rewardClaimed(uint256 , address , uint256 ) returns()
func (_IDeposit *IDepositTransactorSession) RewardClaimed(arg0 *big.Int, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IDeposit.Contract.RewardClaimed(&_IDeposit.TransactOpts, arg0, arg1, arg2)
}

// WithdrawTo is a paid mutator transaction binding the contract method 0x14cd70e4.
//
// Solidity: function withdrawTo(uint256 , uint256 , address ) returns()
func (_IDeposit *IDepositTransactor) WithdrawTo(opts *bind.TransactOpts, arg0 *big.Int, arg1 *big.Int, arg2 common.Address) (*types.Transaction, error) {
	return _IDeposit.contract.Transact(opts, "withdrawTo", arg0, arg1, arg2)
}

// WithdrawTo is a paid mutator transaction binding the contract method 0x14cd70e4.
//
// Solidity: function withdrawTo(uint256 , uint256 , address ) returns()
func (_IDeposit *IDepositSession) WithdrawTo(arg0 *big.Int, arg1 *big.Int, arg2 common.Address) (*types.Transaction, error) {
	return _IDeposit.Contract.WithdrawTo(&_IDeposit.TransactOpts, arg0, arg1, arg2)
}

// WithdrawTo is a paid mutator transaction binding the contract method 0x14cd70e4.
//
// Solidity: function withdrawTo(uint256 , uint256 , address ) returns()
func (_IDeposit *IDepositTransactorSession) WithdrawTo(arg0 *big.Int, arg1 *big.Int, arg2 common.Address) (*types.Transaction, error) {
	return _IDeposit.Contract.WithdrawTo(&_IDeposit.TransactOpts, arg0, arg1, arg2)
}

// IERC20MetaData contains all meta data concerning the IERC20 contract.
var IERC20MetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"sender\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"recipient\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"dd62ed3e": "allowance(address,address)",
		"095ea7b3": "approve(address,uint256)",
		"70a08231": "balanceOf(address)",
		"18160ddd": "totalSupply()",
		"a9059cbb": "transfer(address,uint256)",
		"23b872dd": "transferFrom(address,address,uint256)",
	},
}

// IERC20ABI is the input ABI used to generate the binding from.
// Deprecated: Use IERC20MetaData.ABI instead.
var IERC20ABI = IERC20MetaData.ABI

// Deprecated: Use IERC20MetaData.Sigs instead.
// IERC20FuncSigs maps the 4-byte function signature to its string representation.
var IERC20FuncSigs = IERC20MetaData.Sigs

// IERC20 is an auto generated Go binding around an Ethereum contract.
type IERC20 struct {
	IERC20Caller     // Read-only binding to the contract
	IERC20Transactor // Write-only binding to the contract
	IERC20Filterer   // Log filterer for contract events
}

// IERC20Caller is an auto generated read-only Go binding around an Ethereum contract.
type IERC20Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Transactor is an auto generated write-only Go binding around an Ethereum contract.
type IERC20Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IERC20Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IERC20Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IERC20Session struct {
	Contract     *IERC20           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IERC20CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IERC20CallerSession struct {
	Contract *IERC20Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IERC20TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IERC20TransactorSession struct {
	Contract     *IERC20Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IERC20Raw is an auto generated low-level Go binding around an Ethereum contract.
type IERC20Raw struct {
	Contract *IERC20 // Generic contract binding to access the raw methods on
}

// IERC20CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IERC20CallerRaw struct {
	Contract *IERC20Caller // Generic read-only contract binding to access the raw methods on
}

// IERC20TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IERC20TransactorRaw struct {
	Contract *IERC20Transactor // Generic write-only contract binding to access the raw methods on
}

// NewIERC20 creates a new instance of IERC20, bound to a specific deployed contract.
func NewIERC20(address common.Address, backend bind.ContractBackend) (*IERC20, error) {
	contract, err := bindIERC20(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IERC20{IERC20Caller: IERC20Caller{contract: contract}, IERC20Transactor: IERC20Transactor{contract: contract}, IERC20Filterer: IERC20Filterer{contract: contract}}, nil
}

// NewIERC20Caller creates a new read-only instance of IERC20, bound to a specific deployed contract.
func NewIERC20Caller(address common.Address, caller bind.ContractCaller) (*IERC20Caller, error) {
	contract, err := bindIERC20(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IERC20Caller{contract: contract}, nil
}

// NewIERC20Transactor creates a new write-only instance of IERC20, bound to a specific deployed contract.
func NewIERC20Transactor(address common.Address, transactor bind.ContractTransactor) (*IERC20Transactor, error) {
	contract, err := bindIERC20(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IERC20Transactor{contract: contract}, nil
}

// NewIERC20Filterer creates a new log filterer instance of IERC20, bound to a specific deployed contract.
func NewIERC20Filterer(address common.Address, filterer bind.ContractFilterer) (*IERC20Filterer, error) {
	contract, err := bindIERC20(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IERC20Filterer{contract: contract}, nil
}

// bindIERC20 binds a generic wrapper to an already deployed contract.
func bindIERC20(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IERC20ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IERC20 *IERC20Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IERC20.Contract.IERC20Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IERC20 *IERC20Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IERC20.Contract.IERC20Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IERC20 *IERC20Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IERC20.Contract.IERC20Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IERC20 *IERC20CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IERC20.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IERC20 *IERC20TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IERC20.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IERC20 *IERC20TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IERC20.Contract.contract.Transact(opts, method, params...)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_IERC20 *IERC20Caller) Allowance(opts *bind.CallOpts, owner common.Address, spender common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IERC20.contract.Call(opts, &out, "allowance", owner, spender)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_IERC20 *IERC20Session) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _IERC20.Contract.Allowance(&_IERC20.CallOpts, owner, spender)
}

// Allowance is a free data retrieval call binding the contract method 0xdd62ed3e.
//
// Solidity: function allowance(address owner, address spender) view returns(uint256)
func (_IERC20 *IERC20CallerSession) Allowance(owner common.Address, spender common.Address) (*big.Int, error) {
	return _IERC20.Contract.Allowance(&_IERC20.CallOpts, owner, spender)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_IERC20 *IERC20Caller) BalanceOf(opts *bind.CallOpts, account common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IERC20.contract.Call(opts, &out, "balanceOf", account)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_IERC20 *IERC20Session) BalanceOf(account common.Address) (*big.Int, error) {
	return _IERC20.Contract.BalanceOf(&_IERC20.CallOpts, account)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address account) view returns(uint256)
func (_IERC20 *IERC20CallerSession) BalanceOf(account common.Address) (*big.Int, error) {
	return _IERC20.Contract.BalanceOf(&_IERC20.CallOpts, account)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IERC20 *IERC20Caller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IERC20.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IERC20 *IERC20Session) TotalSupply() (*big.Int, error) {
	return _IERC20.Contract.TotalSupply(&_IERC20.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_IERC20 *IERC20CallerSession) TotalSupply() (*big.Int, error) {
	return _IERC20.Contract.TotalSupply(&_IERC20.CallOpts)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_IERC20 *IERC20Transactor) Approve(opts *bind.TransactOpts, spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "approve", spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_IERC20 *IERC20Session) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Approve(&_IERC20.TransactOpts, spender, amount)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address spender, uint256 amount) returns(bool)
func (_IERC20 *IERC20TransactorSession) Approve(spender common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Approve(&_IERC20.TransactOpts, spender, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20Transactor) Transfer(opts *bind.TransactOpts, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "transfer", recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20Session) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Transfer(&_IERC20.TransactOpts, recipient, amount)
}

// Transfer is a paid mutator transaction binding the contract method 0xa9059cbb.
//
// Solidity: function transfer(address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20TransactorSession) Transfer(recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.Transfer(&_IERC20.TransactOpts, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20Transactor) TransferFrom(opts *bind.TransactOpts, sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.contract.Transact(opts, "transferFrom", sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20Session) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.TransferFrom(&_IERC20.TransactOpts, sender, recipient, amount)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address sender, address recipient, uint256 amount) returns(bool)
func (_IERC20 *IERC20TransactorSession) TransferFrom(sender common.Address, recipient common.Address, amount *big.Int) (*types.Transaction, error) {
	return _IERC20.Contract.TransferFrom(&_IERC20.TransactOpts, sender, recipient, amount)
}

// IERC20ApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the IERC20 contract.
type IERC20ApprovalIterator struct {
	Event *IERC20Approval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IERC20ApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IERC20Approval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IERC20Approval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IERC20ApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IERC20ApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IERC20Approval represents a Approval event raised by the IERC20 contract.
type IERC20Approval struct {
	Owner   common.Address
	Spender common.Address
	Value   *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_IERC20 *IERC20Filterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, spender []common.Address) (*IERC20ApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _IERC20.contract.FilterLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return &IERC20ApprovalIterator{contract: _IERC20.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_IERC20 *IERC20Filterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *IERC20Approval, owner []common.Address, spender []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var spenderRule []interface{}
	for _, spenderItem := range spender {
		spenderRule = append(spenderRule, spenderItem)
	}

	logs, sub, err := _IERC20.contract.WatchLogs(opts, "Approval", ownerRule, spenderRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IERC20Approval)
				if err := _IERC20.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed spender, uint256 value)
func (_IERC20 *IERC20Filterer) ParseApproval(log types.Log) (*IERC20Approval, error) {
	event := new(IERC20Approval)
	if err := _IERC20.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IERC20TransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the IERC20 contract.
type IERC20TransferIterator struct {
	Event *IERC20Transfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *IERC20TransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(IERC20Transfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(IERC20Transfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *IERC20TransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *IERC20TransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// IERC20Transfer represents a Transfer event raised by the IERC20 contract.
type IERC20Transfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_IERC20 *IERC20Filterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*IERC20TransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IERC20.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &IERC20TransferIterator{contract: _IERC20.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_IERC20 *IERC20Filterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *IERC20Transfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _IERC20.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(IERC20Transfer)
				if err := _IERC20.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_IERC20 *IERC20Filterer) ParseTransfer(log types.Log) (*IERC20Transfer, error) {
	event := new(IERC20Transfer)
	if err := _IERC20.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// IFeeDistroMetaData contains all meta data concerning the IFeeDistro contract.
var IFeeDistroMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"name\":\"claim\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"4e71d92d": "claim()",
		"fc0c546a": "token()",
	},
}

// IFeeDistroABI is the input ABI used to generate the binding from.
// Deprecated: Use IFeeDistroMetaData.ABI instead.
var IFeeDistroABI = IFeeDistroMetaData.ABI

// Deprecated: Use IFeeDistroMetaData.Sigs instead.
// IFeeDistroFuncSigs maps the 4-byte function signature to its string representation.
var IFeeDistroFuncSigs = IFeeDistroMetaData.Sigs

// IFeeDistro is an auto generated Go binding around an Ethereum contract.
type IFeeDistro struct {
	IFeeDistroCaller     // Read-only binding to the contract
	IFeeDistroTransactor // Write-only binding to the contract
	IFeeDistroFilterer   // Log filterer for contract events
}

// IFeeDistroCaller is an auto generated read-only Go binding around an Ethereum contract.
type IFeeDistroCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IFeeDistroTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IFeeDistroTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IFeeDistroFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IFeeDistroFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IFeeDistroSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IFeeDistroSession struct {
	Contract     *IFeeDistro       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IFeeDistroCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IFeeDistroCallerSession struct {
	Contract *IFeeDistroCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// IFeeDistroTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IFeeDistroTransactorSession struct {
	Contract     *IFeeDistroTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// IFeeDistroRaw is an auto generated low-level Go binding around an Ethereum contract.
type IFeeDistroRaw struct {
	Contract *IFeeDistro // Generic contract binding to access the raw methods on
}

// IFeeDistroCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IFeeDistroCallerRaw struct {
	Contract *IFeeDistroCaller // Generic read-only contract binding to access the raw methods on
}

// IFeeDistroTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IFeeDistroTransactorRaw struct {
	Contract *IFeeDistroTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIFeeDistro creates a new instance of IFeeDistro, bound to a specific deployed contract.
func NewIFeeDistro(address common.Address, backend bind.ContractBackend) (*IFeeDistro, error) {
	contract, err := bindIFeeDistro(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IFeeDistro{IFeeDistroCaller: IFeeDistroCaller{contract: contract}, IFeeDistroTransactor: IFeeDistroTransactor{contract: contract}, IFeeDistroFilterer: IFeeDistroFilterer{contract: contract}}, nil
}

// NewIFeeDistroCaller creates a new read-only instance of IFeeDistro, bound to a specific deployed contract.
func NewIFeeDistroCaller(address common.Address, caller bind.ContractCaller) (*IFeeDistroCaller, error) {
	contract, err := bindIFeeDistro(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IFeeDistroCaller{contract: contract}, nil
}

// NewIFeeDistroTransactor creates a new write-only instance of IFeeDistro, bound to a specific deployed contract.
func NewIFeeDistroTransactor(address common.Address, transactor bind.ContractTransactor) (*IFeeDistroTransactor, error) {
	contract, err := bindIFeeDistro(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IFeeDistroTransactor{contract: contract}, nil
}

// NewIFeeDistroFilterer creates a new log filterer instance of IFeeDistro, bound to a specific deployed contract.
func NewIFeeDistroFilterer(address common.Address, filterer bind.ContractFilterer) (*IFeeDistroFilterer, error) {
	contract, err := bindIFeeDistro(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IFeeDistroFilterer{contract: contract}, nil
}

// bindIFeeDistro binds a generic wrapper to an already deployed contract.
func bindIFeeDistro(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IFeeDistroABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IFeeDistro *IFeeDistroRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IFeeDistro.Contract.IFeeDistroCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IFeeDistro *IFeeDistroRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IFeeDistro.Contract.IFeeDistroTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IFeeDistro *IFeeDistroRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IFeeDistro.Contract.IFeeDistroTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IFeeDistro *IFeeDistroCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IFeeDistro.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IFeeDistro *IFeeDistroTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IFeeDistro.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IFeeDistro *IFeeDistroTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IFeeDistro.Contract.contract.Transact(opts, method, params...)
}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_IFeeDistro *IFeeDistroCaller) Token(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IFeeDistro.contract.Call(opts, &out, "token")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_IFeeDistro *IFeeDistroSession) Token() (common.Address, error) {
	return _IFeeDistro.Contract.Token(&_IFeeDistro.CallOpts)
}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_IFeeDistro *IFeeDistroCallerSession) Token() (common.Address, error) {
	return _IFeeDistro.Contract.Token(&_IFeeDistro.CallOpts)
}

// Claim is a paid mutator transaction binding the contract method 0x4e71d92d.
//
// Solidity: function claim() returns()
func (_IFeeDistro *IFeeDistroTransactor) Claim(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IFeeDistro.contract.Transact(opts, "claim")
}

// Claim is a paid mutator transaction binding the contract method 0x4e71d92d.
//
// Solidity: function claim() returns()
func (_IFeeDistro *IFeeDistroSession) Claim() (*types.Transaction, error) {
	return _IFeeDistro.Contract.Claim(&_IFeeDistro.TransactOpts)
}

// Claim is a paid mutator transaction binding the contract method 0x4e71d92d.
//
// Solidity: function claim() returns()
func (_IFeeDistro *IFeeDistroTransactorSession) Claim() (*types.Transaction, error) {
	return _IFeeDistro.Contract.Claim(&_IFeeDistro.TransactOpts)
}

// IMinterMetaData contains all meta data concerning the IMinter contract.
var IMinterMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"6a627842": "mint(address)",
	},
}

// IMinterABI is the input ABI used to generate the binding from.
// Deprecated: Use IMinterMetaData.ABI instead.
var IMinterABI = IMinterMetaData.ABI

// Deprecated: Use IMinterMetaData.Sigs instead.
// IMinterFuncSigs maps the 4-byte function signature to its string representation.
var IMinterFuncSigs = IMinterMetaData.Sigs

// IMinter is an auto generated Go binding around an Ethereum contract.
type IMinter struct {
	IMinterCaller     // Read-only binding to the contract
	IMinterTransactor // Write-only binding to the contract
	IMinterFilterer   // Log filterer for contract events
}

// IMinterCaller is an auto generated read-only Go binding around an Ethereum contract.
type IMinterCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IMinterTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IMinterTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IMinterFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IMinterFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IMinterSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IMinterSession struct {
	Contract     *IMinter          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IMinterCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IMinterCallerSession struct {
	Contract *IMinterCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// IMinterTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IMinterTransactorSession struct {
	Contract     *IMinterTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// IMinterRaw is an auto generated low-level Go binding around an Ethereum contract.
type IMinterRaw struct {
	Contract *IMinter // Generic contract binding to access the raw methods on
}

// IMinterCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IMinterCallerRaw struct {
	Contract *IMinterCaller // Generic read-only contract binding to access the raw methods on
}

// IMinterTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IMinterTransactorRaw struct {
	Contract *IMinterTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIMinter creates a new instance of IMinter, bound to a specific deployed contract.
func NewIMinter(address common.Address, backend bind.ContractBackend) (*IMinter, error) {
	contract, err := bindIMinter(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IMinter{IMinterCaller: IMinterCaller{contract: contract}, IMinterTransactor: IMinterTransactor{contract: contract}, IMinterFilterer: IMinterFilterer{contract: contract}}, nil
}

// NewIMinterCaller creates a new read-only instance of IMinter, bound to a specific deployed contract.
func NewIMinterCaller(address common.Address, caller bind.ContractCaller) (*IMinterCaller, error) {
	contract, err := bindIMinter(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IMinterCaller{contract: contract}, nil
}

// NewIMinterTransactor creates a new write-only instance of IMinter, bound to a specific deployed contract.
func NewIMinterTransactor(address common.Address, transactor bind.ContractTransactor) (*IMinterTransactor, error) {
	contract, err := bindIMinter(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IMinterTransactor{contract: contract}, nil
}

// NewIMinterFilterer creates a new log filterer instance of IMinter, bound to a specific deployed contract.
func NewIMinterFilterer(address common.Address, filterer bind.ContractFilterer) (*IMinterFilterer, error) {
	contract, err := bindIMinter(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IMinterFilterer{contract: contract}, nil
}

// bindIMinter binds a generic wrapper to an already deployed contract.
func bindIMinter(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IMinterABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IMinter *IMinterRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMinter.Contract.IMinterCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IMinter *IMinterRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMinter.Contract.IMinterTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IMinter *IMinterRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMinter.Contract.IMinterTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IMinter *IMinterCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IMinter.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IMinter *IMinterTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IMinter.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IMinter *IMinterTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IMinter.Contract.contract.Transact(opts, method, params...)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address ) returns()
func (_IMinter *IMinterTransactor) Mint(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IMinter.contract.Transact(opts, "mint", arg0)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address ) returns()
func (_IMinter *IMinterSession) Mint(arg0 common.Address) (*types.Transaction, error) {
	return _IMinter.Contract.Mint(&_IMinter.TransactOpts, arg0)
}

// Mint is a paid mutator transaction binding the contract method 0x6a627842.
//
// Solidity: function mint(address ) returns()
func (_IMinter *IMinterTransactorSession) Mint(arg0 common.Address) (*types.Transaction, error) {
	return _IMinter.Contract.Mint(&_IMinter.TransactOpts, arg0)
}

// IPoolsMetaData contains all meta data concerning the IPools contract.
var IPoolsMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_lptoken\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_gauge\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_stashVersion\",\"type\":\"uint256\"}],\"name\":\"addPool\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"gaugeMap\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_poolM\",\"type\":\"address\"}],\"name\":\"setPoolManager\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"shutdownPool\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"7e29d6c2": "addPool(address,address,uint256)",
		"cb0d5b52": "gaugeMap(address)",
		"1526fe27": "poolInfo(uint256)",
		"081e3eda": "poolLength()",
		"7aef6715": "setPoolManager(address)",
		"60cafe84": "shutdownPool(uint256)",
	},
}

// IPoolsABI is the input ABI used to generate the binding from.
// Deprecated: Use IPoolsMetaData.ABI instead.
var IPoolsABI = IPoolsMetaData.ABI

// Deprecated: Use IPoolsMetaData.Sigs instead.
// IPoolsFuncSigs maps the 4-byte function signature to its string representation.
var IPoolsFuncSigs = IPoolsMetaData.Sigs

// IPools is an auto generated Go binding around an Ethereum contract.
type IPools struct {
	IPoolsCaller     // Read-only binding to the contract
	IPoolsTransactor // Write-only binding to the contract
	IPoolsFilterer   // Log filterer for contract events
}

// IPoolsCaller is an auto generated read-only Go binding around an Ethereum contract.
type IPoolsCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IPoolsTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IPoolsTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IPoolsFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IPoolsFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IPoolsSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IPoolsSession struct {
	Contract     *IPools           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IPoolsCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IPoolsCallerSession struct {
	Contract *IPoolsCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IPoolsTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IPoolsTransactorSession struct {
	Contract     *IPoolsTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IPoolsRaw is an auto generated low-level Go binding around an Ethereum contract.
type IPoolsRaw struct {
	Contract *IPools // Generic contract binding to access the raw methods on
}

// IPoolsCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IPoolsCallerRaw struct {
	Contract *IPoolsCaller // Generic read-only contract binding to access the raw methods on
}

// IPoolsTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IPoolsTransactorRaw struct {
	Contract *IPoolsTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIPools creates a new instance of IPools, bound to a specific deployed contract.
func NewIPools(address common.Address, backend bind.ContractBackend) (*IPools, error) {
	contract, err := bindIPools(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IPools{IPoolsCaller: IPoolsCaller{contract: contract}, IPoolsTransactor: IPoolsTransactor{contract: contract}, IPoolsFilterer: IPoolsFilterer{contract: contract}}, nil
}

// NewIPoolsCaller creates a new read-only instance of IPools, bound to a specific deployed contract.
func NewIPoolsCaller(address common.Address, caller bind.ContractCaller) (*IPoolsCaller, error) {
	contract, err := bindIPools(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IPoolsCaller{contract: contract}, nil
}

// NewIPoolsTransactor creates a new write-only instance of IPools, bound to a specific deployed contract.
func NewIPoolsTransactor(address common.Address, transactor bind.ContractTransactor) (*IPoolsTransactor, error) {
	contract, err := bindIPools(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IPoolsTransactor{contract: contract}, nil
}

// NewIPoolsFilterer creates a new log filterer instance of IPools, bound to a specific deployed contract.
func NewIPoolsFilterer(address common.Address, filterer bind.ContractFilterer) (*IPoolsFilterer, error) {
	contract, err := bindIPools(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IPoolsFilterer{contract: contract}, nil
}

// bindIPools binds a generic wrapper to an already deployed contract.
func bindIPools(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IPoolsABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IPools *IPoolsRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IPools.Contract.IPoolsCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IPools *IPoolsRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IPools.Contract.IPoolsTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IPools *IPoolsRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IPools.Contract.IPoolsTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IPools *IPoolsCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IPools.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IPools *IPoolsTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IPools.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IPools *IPoolsTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IPools.Contract.contract.Transact(opts, method, params...)
}

// GaugeMap is a free data retrieval call binding the contract method 0xcb0d5b52.
//
// Solidity: function gaugeMap(address ) view returns(bool)
func (_IPools *IPoolsCaller) GaugeMap(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _IPools.contract.Call(opts, &out, "gaugeMap", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// GaugeMap is a free data retrieval call binding the contract method 0xcb0d5b52.
//
// Solidity: function gaugeMap(address ) view returns(bool)
func (_IPools *IPoolsSession) GaugeMap(arg0 common.Address) (bool, error) {
	return _IPools.Contract.GaugeMap(&_IPools.CallOpts, arg0)
}

// GaugeMap is a free data retrieval call binding the contract method 0xcb0d5b52.
//
// Solidity: function gaugeMap(address ) view returns(bool)
func (_IPools *IPoolsCallerSession) GaugeMap(arg0 common.Address) (bool, error) {
	return _IPools.Contract.GaugeMap(&_IPools.CallOpts, arg0)
}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IPools *IPoolsCaller) PoolInfo(opts *bind.CallOpts, arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	var out []interface{}
	err := _IPools.contract.Call(opts, &out, "poolInfo", arg0)

	if err != nil {
		return *new(common.Address), *new(common.Address), *new(common.Address), *new(common.Address), *new(common.Address), *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)
	out1 := *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	out2 := *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	out3 := *abi.ConvertType(out[3], new(common.Address)).(*common.Address)
	out4 := *abi.ConvertType(out[4], new(common.Address)).(*common.Address)
	out5 := *abi.ConvertType(out[5], new(bool)).(*bool)

	return out0, out1, out2, out3, out4, out5, err

}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IPools *IPoolsSession) PoolInfo(arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	return _IPools.Contract.PoolInfo(&_IPools.CallOpts, arg0)
}

// PoolInfo is a free data retrieval call binding the contract method 0x1526fe27.
//
// Solidity: function poolInfo(uint256 ) view returns(address, address, address, address, address, bool)
func (_IPools *IPoolsCallerSession) PoolInfo(arg0 *big.Int) (common.Address, common.Address, common.Address, common.Address, common.Address, bool, error) {
	return _IPools.Contract.PoolInfo(&_IPools.CallOpts, arg0)
}

// PoolLength is a free data retrieval call binding the contract method 0x081e3eda.
//
// Solidity: function poolLength() view returns(uint256)
func (_IPools *IPoolsCaller) PoolLength(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _IPools.contract.Call(opts, &out, "poolLength")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PoolLength is a free data retrieval call binding the contract method 0x081e3eda.
//
// Solidity: function poolLength() view returns(uint256)
func (_IPools *IPoolsSession) PoolLength() (*big.Int, error) {
	return _IPools.Contract.PoolLength(&_IPools.CallOpts)
}

// PoolLength is a free data retrieval call binding the contract method 0x081e3eda.
//
// Solidity: function poolLength() view returns(uint256)
func (_IPools *IPoolsCallerSession) PoolLength() (*big.Int, error) {
	return _IPools.Contract.PoolLength(&_IPools.CallOpts)
}

// AddPool is a paid mutator transaction binding the contract method 0x7e29d6c2.
//
// Solidity: function addPool(address _lptoken, address _gauge, uint256 _stashVersion) returns(bool)
func (_IPools *IPoolsTransactor) AddPool(opts *bind.TransactOpts, _lptoken common.Address, _gauge common.Address, _stashVersion *big.Int) (*types.Transaction, error) {
	return _IPools.contract.Transact(opts, "addPool", _lptoken, _gauge, _stashVersion)
}

// AddPool is a paid mutator transaction binding the contract method 0x7e29d6c2.
//
// Solidity: function addPool(address _lptoken, address _gauge, uint256 _stashVersion) returns(bool)
func (_IPools *IPoolsSession) AddPool(_lptoken common.Address, _gauge common.Address, _stashVersion *big.Int) (*types.Transaction, error) {
	return _IPools.Contract.AddPool(&_IPools.TransactOpts, _lptoken, _gauge, _stashVersion)
}

// AddPool is a paid mutator transaction binding the contract method 0x7e29d6c2.
//
// Solidity: function addPool(address _lptoken, address _gauge, uint256 _stashVersion) returns(bool)
func (_IPools *IPoolsTransactorSession) AddPool(_lptoken common.Address, _gauge common.Address, _stashVersion *big.Int) (*types.Transaction, error) {
	return _IPools.Contract.AddPool(&_IPools.TransactOpts, _lptoken, _gauge, _stashVersion)
}

// SetPoolManager is a paid mutator transaction binding the contract method 0x7aef6715.
//
// Solidity: function setPoolManager(address _poolM) returns()
func (_IPools *IPoolsTransactor) SetPoolManager(opts *bind.TransactOpts, _poolM common.Address) (*types.Transaction, error) {
	return _IPools.contract.Transact(opts, "setPoolManager", _poolM)
}

// SetPoolManager is a paid mutator transaction binding the contract method 0x7aef6715.
//
// Solidity: function setPoolManager(address _poolM) returns()
func (_IPools *IPoolsSession) SetPoolManager(_poolM common.Address) (*types.Transaction, error) {
	return _IPools.Contract.SetPoolManager(&_IPools.TransactOpts, _poolM)
}

// SetPoolManager is a paid mutator transaction binding the contract method 0x7aef6715.
//
// Solidity: function setPoolManager(address _poolM) returns()
func (_IPools *IPoolsTransactorSession) SetPoolManager(_poolM common.Address) (*types.Transaction, error) {
	return _IPools.Contract.SetPoolManager(&_IPools.TransactOpts, _poolM)
}

// ShutdownPool is a paid mutator transaction binding the contract method 0x60cafe84.
//
// Solidity: function shutdownPool(uint256 _pid) returns(bool)
func (_IPools *IPoolsTransactor) ShutdownPool(opts *bind.TransactOpts, _pid *big.Int) (*types.Transaction, error) {
	return _IPools.contract.Transact(opts, "shutdownPool", _pid)
}

// ShutdownPool is a paid mutator transaction binding the contract method 0x60cafe84.
//
// Solidity: function shutdownPool(uint256 _pid) returns(bool)
func (_IPools *IPoolsSession) ShutdownPool(_pid *big.Int) (*types.Transaction, error) {
	return _IPools.Contract.ShutdownPool(&_IPools.TransactOpts, _pid)
}

// ShutdownPool is a paid mutator transaction binding the contract method 0x60cafe84.
//
// Solidity: function shutdownPool(uint256 _pid) returns(bool)
func (_IPools *IPoolsTransactorSession) ShutdownPool(_pid *big.Int) (*types.Transaction, error) {
	return _IPools.Contract.ShutdownPool(&_IPools.TransactOpts, _pid)
}

// IRegistryMetaData contains all meta data concerning the IRegistry contract.
var IRegistryMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"name\":\"gauge_controller\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_id\",\"type\":\"uint256\"}],\"name\":\"get_address\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"get_gauges\",\"outputs\":[{\"internalType\":\"address[10]\",\"name\":\"\",\"type\":\"address[10]\"},{\"internalType\":\"uint128[10]\",\"name\":\"\",\"type\":\"uint128[10]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"get_lp_token\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"get_registry\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"d8b9a018": "gauge_controller()",
		"493f4f74": "get_address(uint256)",
		"56059ffb": "get_gauges(address)",
		"37951049": "get_lp_token(address)",
		"a262904b": "get_registry()",
	},
}

// IRegistryABI is the input ABI used to generate the binding from.
// Deprecated: Use IRegistryMetaData.ABI instead.
var IRegistryABI = IRegistryMetaData.ABI

// Deprecated: Use IRegistryMetaData.Sigs instead.
// IRegistryFuncSigs maps the 4-byte function signature to its string representation.
var IRegistryFuncSigs = IRegistryMetaData.Sigs

// IRegistry is an auto generated Go binding around an Ethereum contract.
type IRegistry struct {
	IRegistryCaller     // Read-only binding to the contract
	IRegistryTransactor // Write-only binding to the contract
	IRegistryFilterer   // Log filterer for contract events
}

// IRegistryCaller is an auto generated read-only Go binding around an Ethereum contract.
type IRegistryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRegistryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IRegistryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRegistryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IRegistryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRegistrySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IRegistrySession struct {
	Contract     *IRegistry        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IRegistryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IRegistryCallerSession struct {
	Contract *IRegistryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// IRegistryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IRegistryTransactorSession struct {
	Contract     *IRegistryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// IRegistryRaw is an auto generated low-level Go binding around an Ethereum contract.
type IRegistryRaw struct {
	Contract *IRegistry // Generic contract binding to access the raw methods on
}

// IRegistryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IRegistryCallerRaw struct {
	Contract *IRegistryCaller // Generic read-only contract binding to access the raw methods on
}

// IRegistryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IRegistryTransactorRaw struct {
	Contract *IRegistryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIRegistry creates a new instance of IRegistry, bound to a specific deployed contract.
func NewIRegistry(address common.Address, backend bind.ContractBackend) (*IRegistry, error) {
	contract, err := bindIRegistry(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IRegistry{IRegistryCaller: IRegistryCaller{contract: contract}, IRegistryTransactor: IRegistryTransactor{contract: contract}, IRegistryFilterer: IRegistryFilterer{contract: contract}}, nil
}

// NewIRegistryCaller creates a new read-only instance of IRegistry, bound to a specific deployed contract.
func NewIRegistryCaller(address common.Address, caller bind.ContractCaller) (*IRegistryCaller, error) {
	contract, err := bindIRegistry(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IRegistryCaller{contract: contract}, nil
}

// NewIRegistryTransactor creates a new write-only instance of IRegistry, bound to a specific deployed contract.
func NewIRegistryTransactor(address common.Address, transactor bind.ContractTransactor) (*IRegistryTransactor, error) {
	contract, err := bindIRegistry(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IRegistryTransactor{contract: contract}, nil
}

// NewIRegistryFilterer creates a new log filterer instance of IRegistry, bound to a specific deployed contract.
func NewIRegistryFilterer(address common.Address, filterer bind.ContractFilterer) (*IRegistryFilterer, error) {
	contract, err := bindIRegistry(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IRegistryFilterer{contract: contract}, nil
}

// bindIRegistry binds a generic wrapper to an already deployed contract.
func bindIRegistry(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IRegistryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRegistry *IRegistryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRegistry.Contract.IRegistryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRegistry *IRegistryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRegistry.Contract.IRegistryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRegistry *IRegistryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRegistry.Contract.IRegistryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRegistry *IRegistryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRegistry.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRegistry *IRegistryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRegistry.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRegistry *IRegistryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRegistry.Contract.contract.Transact(opts, method, params...)
}

// GaugeController is a free data retrieval call binding the contract method 0xd8b9a018.
//
// Solidity: function gauge_controller() view returns(address)
func (_IRegistry *IRegistryCaller) GaugeController(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IRegistry.contract.Call(opts, &out, "gauge_controller")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GaugeController is a free data retrieval call binding the contract method 0xd8b9a018.
//
// Solidity: function gauge_controller() view returns(address)
func (_IRegistry *IRegistrySession) GaugeController() (common.Address, error) {
	return _IRegistry.Contract.GaugeController(&_IRegistry.CallOpts)
}

// GaugeController is a free data retrieval call binding the contract method 0xd8b9a018.
//
// Solidity: function gauge_controller() view returns(address)
func (_IRegistry *IRegistryCallerSession) GaugeController() (common.Address, error) {
	return _IRegistry.Contract.GaugeController(&_IRegistry.CallOpts)
}

// GetAddress is a free data retrieval call binding the contract method 0x493f4f74.
//
// Solidity: function get_address(uint256 _id) view returns(address)
func (_IRegistry *IRegistryCaller) GetAddress(opts *bind.CallOpts, _id *big.Int) (common.Address, error) {
	var out []interface{}
	err := _IRegistry.contract.Call(opts, &out, "get_address", _id)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetAddress is a free data retrieval call binding the contract method 0x493f4f74.
//
// Solidity: function get_address(uint256 _id) view returns(address)
func (_IRegistry *IRegistrySession) GetAddress(_id *big.Int) (common.Address, error) {
	return _IRegistry.Contract.GetAddress(&_IRegistry.CallOpts, _id)
}

// GetAddress is a free data retrieval call binding the contract method 0x493f4f74.
//
// Solidity: function get_address(uint256 _id) view returns(address)
func (_IRegistry *IRegistryCallerSession) GetAddress(_id *big.Int) (common.Address, error) {
	return _IRegistry.Contract.GetAddress(&_IRegistry.CallOpts, _id)
}

// GetGauges is a free data retrieval call binding the contract method 0x56059ffb.
//
// Solidity: function get_gauges(address ) view returns(address[10], uint128[10])
func (_IRegistry *IRegistryCaller) GetGauges(opts *bind.CallOpts, arg0 common.Address) ([10]common.Address, [10]*big.Int, error) {
	var out []interface{}
	err := _IRegistry.contract.Call(opts, &out, "get_gauges", arg0)

	if err != nil {
		return *new([10]common.Address), *new([10]*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new([10]common.Address)).(*[10]common.Address)
	out1 := *abi.ConvertType(out[1], new([10]*big.Int)).(*[10]*big.Int)

	return out0, out1, err

}

// GetGauges is a free data retrieval call binding the contract method 0x56059ffb.
//
// Solidity: function get_gauges(address ) view returns(address[10], uint128[10])
func (_IRegistry *IRegistrySession) GetGauges(arg0 common.Address) ([10]common.Address, [10]*big.Int, error) {
	return _IRegistry.Contract.GetGauges(&_IRegistry.CallOpts, arg0)
}

// GetGauges is a free data retrieval call binding the contract method 0x56059ffb.
//
// Solidity: function get_gauges(address ) view returns(address[10], uint128[10])
func (_IRegistry *IRegistryCallerSession) GetGauges(arg0 common.Address) ([10]common.Address, [10]*big.Int, error) {
	return _IRegistry.Contract.GetGauges(&_IRegistry.CallOpts, arg0)
}

// GetLpToken is a free data retrieval call binding the contract method 0x37951049.
//
// Solidity: function get_lp_token(address ) view returns(address)
func (_IRegistry *IRegistryCaller) GetLpToken(opts *bind.CallOpts, arg0 common.Address) (common.Address, error) {
	var out []interface{}
	err := _IRegistry.contract.Call(opts, &out, "get_lp_token", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetLpToken is a free data retrieval call binding the contract method 0x37951049.
//
// Solidity: function get_lp_token(address ) view returns(address)
func (_IRegistry *IRegistrySession) GetLpToken(arg0 common.Address) (common.Address, error) {
	return _IRegistry.Contract.GetLpToken(&_IRegistry.CallOpts, arg0)
}

// GetLpToken is a free data retrieval call binding the contract method 0x37951049.
//
// Solidity: function get_lp_token(address ) view returns(address)
func (_IRegistry *IRegistryCallerSession) GetLpToken(arg0 common.Address) (common.Address, error) {
	return _IRegistry.Contract.GetLpToken(&_IRegistry.CallOpts, arg0)
}

// GetRegistry is a free data retrieval call binding the contract method 0xa262904b.
//
// Solidity: function get_registry() view returns(address)
func (_IRegistry *IRegistryCaller) GetRegistry(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IRegistry.contract.Call(opts, &out, "get_registry")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetRegistry is a free data retrieval call binding the contract method 0xa262904b.
//
// Solidity: function get_registry() view returns(address)
func (_IRegistry *IRegistrySession) GetRegistry() (common.Address, error) {
	return _IRegistry.Contract.GetRegistry(&_IRegistry.CallOpts)
}

// GetRegistry is a free data retrieval call binding the contract method 0xa262904b.
//
// Solidity: function get_registry() view returns(address)
func (_IRegistry *IRegistryCallerSession) GetRegistry() (common.Address, error) {
	return _IRegistry.Contract.GetRegistry(&_IRegistry.CallOpts)
}

// IRewardFactoryMetaData contains all meta data concerning the IRewardFactory contract.
var IRewardFactoryMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"CreateCrvRewards\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"CreateTokenRewards\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"activeRewardCount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"addActiveReward\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"removeActiveReward\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"name\":\"setAccess\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"58cbfd45": "CreateCrvRewards(uint256,address)",
		"f8d6122e": "CreateTokenRewards(address,address,address)",
		"0d5843f7": "activeRewardCount(address)",
		"b7f927b1": "addActiveReward(address,uint256)",
		"ef9126ad": "removeActiveReward(address,uint256)",
		"b84614a5": "setAccess(address,bool)",
	},
}

// IRewardFactoryABI is the input ABI used to generate the binding from.
// Deprecated: Use IRewardFactoryMetaData.ABI instead.
var IRewardFactoryABI = IRewardFactoryMetaData.ABI

// Deprecated: Use IRewardFactoryMetaData.Sigs instead.
// IRewardFactoryFuncSigs maps the 4-byte function signature to its string representation.
var IRewardFactoryFuncSigs = IRewardFactoryMetaData.Sigs

// IRewardFactory is an auto generated Go binding around an Ethereum contract.
type IRewardFactory struct {
	IRewardFactoryCaller     // Read-only binding to the contract
	IRewardFactoryTransactor // Write-only binding to the contract
	IRewardFactoryFilterer   // Log filterer for contract events
}

// IRewardFactoryCaller is an auto generated read-only Go binding around an Ethereum contract.
type IRewardFactoryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardFactoryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IRewardFactoryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardFactoryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IRewardFactoryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardFactorySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IRewardFactorySession struct {
	Contract     *IRewardFactory   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IRewardFactoryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IRewardFactoryCallerSession struct {
	Contract *IRewardFactoryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// IRewardFactoryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IRewardFactoryTransactorSession struct {
	Contract     *IRewardFactoryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// IRewardFactoryRaw is an auto generated low-level Go binding around an Ethereum contract.
type IRewardFactoryRaw struct {
	Contract *IRewardFactory // Generic contract binding to access the raw methods on
}

// IRewardFactoryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IRewardFactoryCallerRaw struct {
	Contract *IRewardFactoryCaller // Generic read-only contract binding to access the raw methods on
}

// IRewardFactoryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IRewardFactoryTransactorRaw struct {
	Contract *IRewardFactoryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIRewardFactory creates a new instance of IRewardFactory, bound to a specific deployed contract.
func NewIRewardFactory(address common.Address, backend bind.ContractBackend) (*IRewardFactory, error) {
	contract, err := bindIRewardFactory(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IRewardFactory{IRewardFactoryCaller: IRewardFactoryCaller{contract: contract}, IRewardFactoryTransactor: IRewardFactoryTransactor{contract: contract}, IRewardFactoryFilterer: IRewardFactoryFilterer{contract: contract}}, nil
}

// NewIRewardFactoryCaller creates a new read-only instance of IRewardFactory, bound to a specific deployed contract.
func NewIRewardFactoryCaller(address common.Address, caller bind.ContractCaller) (*IRewardFactoryCaller, error) {
	contract, err := bindIRewardFactory(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IRewardFactoryCaller{contract: contract}, nil
}

// NewIRewardFactoryTransactor creates a new write-only instance of IRewardFactory, bound to a specific deployed contract.
func NewIRewardFactoryTransactor(address common.Address, transactor bind.ContractTransactor) (*IRewardFactoryTransactor, error) {
	contract, err := bindIRewardFactory(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IRewardFactoryTransactor{contract: contract}, nil
}

// NewIRewardFactoryFilterer creates a new log filterer instance of IRewardFactory, bound to a specific deployed contract.
func NewIRewardFactoryFilterer(address common.Address, filterer bind.ContractFilterer) (*IRewardFactoryFilterer, error) {
	contract, err := bindIRewardFactory(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IRewardFactoryFilterer{contract: contract}, nil
}

// bindIRewardFactory binds a generic wrapper to an already deployed contract.
func bindIRewardFactory(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IRewardFactoryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRewardFactory *IRewardFactoryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRewardFactory.Contract.IRewardFactoryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRewardFactory *IRewardFactoryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRewardFactory.Contract.IRewardFactoryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRewardFactory *IRewardFactoryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRewardFactory.Contract.IRewardFactoryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRewardFactory *IRewardFactoryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRewardFactory.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRewardFactory *IRewardFactoryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRewardFactory.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRewardFactory *IRewardFactoryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRewardFactory.Contract.contract.Transact(opts, method, params...)
}

// ActiveRewardCount is a free data retrieval call binding the contract method 0x0d5843f7.
//
// Solidity: function activeRewardCount(address ) view returns(uint256)
func (_IRewardFactory *IRewardFactoryCaller) ActiveRewardCount(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IRewardFactory.contract.Call(opts, &out, "activeRewardCount", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// ActiveRewardCount is a free data retrieval call binding the contract method 0x0d5843f7.
//
// Solidity: function activeRewardCount(address ) view returns(uint256)
func (_IRewardFactory *IRewardFactorySession) ActiveRewardCount(arg0 common.Address) (*big.Int, error) {
	return _IRewardFactory.Contract.ActiveRewardCount(&_IRewardFactory.CallOpts, arg0)
}

// ActiveRewardCount is a free data retrieval call binding the contract method 0x0d5843f7.
//
// Solidity: function activeRewardCount(address ) view returns(uint256)
func (_IRewardFactory *IRewardFactoryCallerSession) ActiveRewardCount(arg0 common.Address) (*big.Int, error) {
	return _IRewardFactory.Contract.ActiveRewardCount(&_IRewardFactory.CallOpts, arg0)
}

// CreateCrvRewards is a paid mutator transaction binding the contract method 0x58cbfd45.
//
// Solidity: function CreateCrvRewards(uint256 , address ) returns(address)
func (_IRewardFactory *IRewardFactoryTransactor) CreateCrvRewards(opts *bind.TransactOpts, arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.contract.Transact(opts, "CreateCrvRewards", arg0, arg1)
}

// CreateCrvRewards is a paid mutator transaction binding the contract method 0x58cbfd45.
//
// Solidity: function CreateCrvRewards(uint256 , address ) returns(address)
func (_IRewardFactory *IRewardFactorySession) CreateCrvRewards(arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.Contract.CreateCrvRewards(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// CreateCrvRewards is a paid mutator transaction binding the contract method 0x58cbfd45.
//
// Solidity: function CreateCrvRewards(uint256 , address ) returns(address)
func (_IRewardFactory *IRewardFactoryTransactorSession) CreateCrvRewards(arg0 *big.Int, arg1 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.Contract.CreateCrvRewards(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// CreateTokenRewards is a paid mutator transaction binding the contract method 0xf8d6122e.
//
// Solidity: function CreateTokenRewards(address , address , address ) returns(address)
func (_IRewardFactory *IRewardFactoryTransactor) CreateTokenRewards(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.contract.Transact(opts, "CreateTokenRewards", arg0, arg1, arg2)
}

// CreateTokenRewards is a paid mutator transaction binding the contract method 0xf8d6122e.
//
// Solidity: function CreateTokenRewards(address , address , address ) returns(address)
func (_IRewardFactory *IRewardFactorySession) CreateTokenRewards(arg0 common.Address, arg1 common.Address, arg2 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.Contract.CreateTokenRewards(&_IRewardFactory.TransactOpts, arg0, arg1, arg2)
}

// CreateTokenRewards is a paid mutator transaction binding the contract method 0xf8d6122e.
//
// Solidity: function CreateTokenRewards(address , address , address ) returns(address)
func (_IRewardFactory *IRewardFactoryTransactorSession) CreateTokenRewards(arg0 common.Address, arg1 common.Address, arg2 common.Address) (*types.Transaction, error) {
	return _IRewardFactory.Contract.CreateTokenRewards(&_IRewardFactory.TransactOpts, arg0, arg1, arg2)
}

// AddActiveReward is a paid mutator transaction binding the contract method 0xb7f927b1.
//
// Solidity: function addActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactoryTransactor) AddActiveReward(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.contract.Transact(opts, "addActiveReward", arg0, arg1)
}

// AddActiveReward is a paid mutator transaction binding the contract method 0xb7f927b1.
//
// Solidity: function addActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactorySession) AddActiveReward(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.Contract.AddActiveReward(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// AddActiveReward is a paid mutator transaction binding the contract method 0xb7f927b1.
//
// Solidity: function addActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactoryTransactorSession) AddActiveReward(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.Contract.AddActiveReward(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// RemoveActiveReward is a paid mutator transaction binding the contract method 0xef9126ad.
//
// Solidity: function removeActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactoryTransactor) RemoveActiveReward(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.contract.Transact(opts, "removeActiveReward", arg0, arg1)
}

// RemoveActiveReward is a paid mutator transaction binding the contract method 0xef9126ad.
//
// Solidity: function removeActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactorySession) RemoveActiveReward(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.Contract.RemoveActiveReward(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// RemoveActiveReward is a paid mutator transaction binding the contract method 0xef9126ad.
//
// Solidity: function removeActiveReward(address , uint256 ) returns(bool)
func (_IRewardFactory *IRewardFactoryTransactorSession) RemoveActiveReward(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewardFactory.Contract.RemoveActiveReward(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// SetAccess is a paid mutator transaction binding the contract method 0xb84614a5.
//
// Solidity: function setAccess(address , bool ) returns()
func (_IRewardFactory *IRewardFactoryTransactor) SetAccess(opts *bind.TransactOpts, arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IRewardFactory.contract.Transact(opts, "setAccess", arg0, arg1)
}

// SetAccess is a paid mutator transaction binding the contract method 0xb84614a5.
//
// Solidity: function setAccess(address , bool ) returns()
func (_IRewardFactory *IRewardFactorySession) SetAccess(arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IRewardFactory.Contract.SetAccess(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// SetAccess is a paid mutator transaction binding the contract method 0xb84614a5.
//
// Solidity: function setAccess(address , bool ) returns()
func (_IRewardFactory *IRewardFactoryTransactorSession) SetAccess(arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IRewardFactory.Contract.SetAccess(&_IRewardFactory.TransactOpts, arg0, arg1)
}

// IRewardsMetaData contains all meta data concerning the IRewards contract.
var IRewardsMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"addExtraReward\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"exit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"getReward\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"notifyRewardAmount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"queueNewRewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"stake\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"stakeFor\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"stakingToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"5e43c47b": "addExtraReward(address)",
		"b42652e9": "exit(address)",
		"c00007b0": "getReward(address)",
		"3c6b16ab": "notifyRewardAmount(uint256)",
		"590a41f5": "queueNewRewards(uint256)",
		"adc9772e": "stake(address,uint256)",
		"2ee40908": "stakeFor(address,uint256)",
		"72f702f3": "stakingToken()",
		"f3fef3a3": "withdraw(address,uint256)",
	},
}

// IRewardsABI is the input ABI used to generate the binding from.
// Deprecated: Use IRewardsMetaData.ABI instead.
var IRewardsABI = IRewardsMetaData.ABI

// Deprecated: Use IRewardsMetaData.Sigs instead.
// IRewardsFuncSigs maps the 4-byte function signature to its string representation.
var IRewardsFuncSigs = IRewardsMetaData.Sigs

// IRewards is an auto generated Go binding around an Ethereum contract.
type IRewards struct {
	IRewardsCaller     // Read-only binding to the contract
	IRewardsTransactor // Write-only binding to the contract
	IRewardsFilterer   // Log filterer for contract events
}

// IRewardsCaller is an auto generated read-only Go binding around an Ethereum contract.
type IRewardsCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardsTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IRewardsTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardsFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IRewardsFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IRewardsSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IRewardsSession struct {
	Contract     *IRewards         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IRewardsCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IRewardsCallerSession struct {
	Contract *IRewardsCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// IRewardsTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IRewardsTransactorSession struct {
	Contract     *IRewardsTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// IRewardsRaw is an auto generated low-level Go binding around an Ethereum contract.
type IRewardsRaw struct {
	Contract *IRewards // Generic contract binding to access the raw methods on
}

// IRewardsCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IRewardsCallerRaw struct {
	Contract *IRewardsCaller // Generic read-only contract binding to access the raw methods on
}

// IRewardsTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IRewardsTransactorRaw struct {
	Contract *IRewardsTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIRewards creates a new instance of IRewards, bound to a specific deployed contract.
func NewIRewards(address common.Address, backend bind.ContractBackend) (*IRewards, error) {
	contract, err := bindIRewards(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IRewards{IRewardsCaller: IRewardsCaller{contract: contract}, IRewardsTransactor: IRewardsTransactor{contract: contract}, IRewardsFilterer: IRewardsFilterer{contract: contract}}, nil
}

// NewIRewardsCaller creates a new read-only instance of IRewards, bound to a specific deployed contract.
func NewIRewardsCaller(address common.Address, caller bind.ContractCaller) (*IRewardsCaller, error) {
	contract, err := bindIRewards(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IRewardsCaller{contract: contract}, nil
}

// NewIRewardsTransactor creates a new write-only instance of IRewards, bound to a specific deployed contract.
func NewIRewardsTransactor(address common.Address, transactor bind.ContractTransactor) (*IRewardsTransactor, error) {
	contract, err := bindIRewards(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IRewardsTransactor{contract: contract}, nil
}

// NewIRewardsFilterer creates a new log filterer instance of IRewards, bound to a specific deployed contract.
func NewIRewardsFilterer(address common.Address, filterer bind.ContractFilterer) (*IRewardsFilterer, error) {
	contract, err := bindIRewards(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IRewardsFilterer{contract: contract}, nil
}

// bindIRewards binds a generic wrapper to an already deployed contract.
func bindIRewards(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IRewardsABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRewards *IRewardsRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRewards.Contract.IRewardsCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRewards *IRewardsRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRewards.Contract.IRewardsTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRewards *IRewardsRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRewards.Contract.IRewardsTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IRewards *IRewardsCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IRewards.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IRewards *IRewardsTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRewards.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IRewards *IRewardsTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IRewards.Contract.contract.Transact(opts, method, params...)
}

// AddExtraReward is a paid mutator transaction binding the contract method 0x5e43c47b.
//
// Solidity: function addExtraReward(address ) returns()
func (_IRewards *IRewardsTransactor) AddExtraReward(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "addExtraReward", arg0)
}

// AddExtraReward is a paid mutator transaction binding the contract method 0x5e43c47b.
//
// Solidity: function addExtraReward(address ) returns()
func (_IRewards *IRewardsSession) AddExtraReward(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.AddExtraReward(&_IRewards.TransactOpts, arg0)
}

// AddExtraReward is a paid mutator transaction binding the contract method 0x5e43c47b.
//
// Solidity: function addExtraReward(address ) returns()
func (_IRewards *IRewardsTransactorSession) AddExtraReward(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.AddExtraReward(&_IRewards.TransactOpts, arg0)
}

// Exit is a paid mutator transaction binding the contract method 0xb42652e9.
//
// Solidity: function exit(address ) returns()
func (_IRewards *IRewardsTransactor) Exit(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "exit", arg0)
}

// Exit is a paid mutator transaction binding the contract method 0xb42652e9.
//
// Solidity: function exit(address ) returns()
func (_IRewards *IRewardsSession) Exit(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.Exit(&_IRewards.TransactOpts, arg0)
}

// Exit is a paid mutator transaction binding the contract method 0xb42652e9.
//
// Solidity: function exit(address ) returns()
func (_IRewards *IRewardsTransactorSession) Exit(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.Exit(&_IRewards.TransactOpts, arg0)
}

// GetReward is a paid mutator transaction binding the contract method 0xc00007b0.
//
// Solidity: function getReward(address ) returns()
func (_IRewards *IRewardsTransactor) GetReward(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "getReward", arg0)
}

// GetReward is a paid mutator transaction binding the contract method 0xc00007b0.
//
// Solidity: function getReward(address ) returns()
func (_IRewards *IRewardsSession) GetReward(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.GetReward(&_IRewards.TransactOpts, arg0)
}

// GetReward is a paid mutator transaction binding the contract method 0xc00007b0.
//
// Solidity: function getReward(address ) returns()
func (_IRewards *IRewardsTransactorSession) GetReward(arg0 common.Address) (*types.Transaction, error) {
	return _IRewards.Contract.GetReward(&_IRewards.TransactOpts, arg0)
}

// NotifyRewardAmount is a paid mutator transaction binding the contract method 0x3c6b16ab.
//
// Solidity: function notifyRewardAmount(uint256 ) returns()
func (_IRewards *IRewardsTransactor) NotifyRewardAmount(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "notifyRewardAmount", arg0)
}

// NotifyRewardAmount is a paid mutator transaction binding the contract method 0x3c6b16ab.
//
// Solidity: function notifyRewardAmount(uint256 ) returns()
func (_IRewards *IRewardsSession) NotifyRewardAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.NotifyRewardAmount(&_IRewards.TransactOpts, arg0)
}

// NotifyRewardAmount is a paid mutator transaction binding the contract method 0x3c6b16ab.
//
// Solidity: function notifyRewardAmount(uint256 ) returns()
func (_IRewards *IRewardsTransactorSession) NotifyRewardAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.NotifyRewardAmount(&_IRewards.TransactOpts, arg0)
}

// QueueNewRewards is a paid mutator transaction binding the contract method 0x590a41f5.
//
// Solidity: function queueNewRewards(uint256 ) returns()
func (_IRewards *IRewardsTransactor) QueueNewRewards(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "queueNewRewards", arg0)
}

// QueueNewRewards is a paid mutator transaction binding the contract method 0x590a41f5.
//
// Solidity: function queueNewRewards(uint256 ) returns()
func (_IRewards *IRewardsSession) QueueNewRewards(arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.QueueNewRewards(&_IRewards.TransactOpts, arg0)
}

// QueueNewRewards is a paid mutator transaction binding the contract method 0x590a41f5.
//
// Solidity: function queueNewRewards(uint256 ) returns()
func (_IRewards *IRewardsTransactorSession) QueueNewRewards(arg0 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.QueueNewRewards(&_IRewards.TransactOpts, arg0)
}

// Stake is a paid mutator transaction binding the contract method 0xadc9772e.
//
// Solidity: function stake(address , uint256 ) returns()
func (_IRewards *IRewardsTransactor) Stake(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "stake", arg0, arg1)
}

// Stake is a paid mutator transaction binding the contract method 0xadc9772e.
//
// Solidity: function stake(address , uint256 ) returns()
func (_IRewards *IRewardsSession) Stake(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.Stake(&_IRewards.TransactOpts, arg0, arg1)
}

// Stake is a paid mutator transaction binding the contract method 0xadc9772e.
//
// Solidity: function stake(address , uint256 ) returns()
func (_IRewards *IRewardsTransactorSession) Stake(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.Stake(&_IRewards.TransactOpts, arg0, arg1)
}

// StakeFor is a paid mutator transaction binding the contract method 0x2ee40908.
//
// Solidity: function stakeFor(address , uint256 ) returns()
func (_IRewards *IRewardsTransactor) StakeFor(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "stakeFor", arg0, arg1)
}

// StakeFor is a paid mutator transaction binding the contract method 0x2ee40908.
//
// Solidity: function stakeFor(address , uint256 ) returns()
func (_IRewards *IRewardsSession) StakeFor(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.StakeFor(&_IRewards.TransactOpts, arg0, arg1)
}

// StakeFor is a paid mutator transaction binding the contract method 0x2ee40908.
//
// Solidity: function stakeFor(address , uint256 ) returns()
func (_IRewards *IRewardsTransactorSession) StakeFor(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.StakeFor(&_IRewards.TransactOpts, arg0, arg1)
}

// StakingToken is a paid mutator transaction binding the contract method 0x72f702f3.
//
// Solidity: function stakingToken() returns(address)
func (_IRewards *IRewardsTransactor) StakingToken(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "stakingToken")
}

// StakingToken is a paid mutator transaction binding the contract method 0x72f702f3.
//
// Solidity: function stakingToken() returns(address)
func (_IRewards *IRewardsSession) StakingToken() (*types.Transaction, error) {
	return _IRewards.Contract.StakingToken(&_IRewards.TransactOpts)
}

// StakingToken is a paid mutator transaction binding the contract method 0x72f702f3.
//
// Solidity: function stakingToken() returns(address)
func (_IRewards *IRewardsTransactorSession) StakingToken() (*types.Transaction, error) {
	return _IRewards.Contract.StakingToken(&_IRewards.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0xf3fef3a3.
//
// Solidity: function withdraw(address , uint256 ) returns()
func (_IRewards *IRewardsTransactor) Withdraw(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.contract.Transact(opts, "withdraw", arg0, arg1)
}

// Withdraw is a paid mutator transaction binding the contract method 0xf3fef3a3.
//
// Solidity: function withdraw(address , uint256 ) returns()
func (_IRewards *IRewardsSession) Withdraw(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.Withdraw(&_IRewards.TransactOpts, arg0, arg1)
}

// Withdraw is a paid mutator transaction binding the contract method 0xf3fef3a3.
//
// Solidity: function withdraw(address , uint256 ) returns()
func (_IRewards *IRewardsTransactorSession) Withdraw(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IRewards.Contract.Withdraw(&_IRewards.TransactOpts, arg0, arg1)
}

// IStakerMetaData contains all meta data concerning the IStaker contract.
var IStakerMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOfPool\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"claimCrv\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"claimFees\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"claimRewards\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"createLock\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"execute\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"increaseAmount\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"increaseTime\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"operator\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"release\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"name\":\"setStashAccess\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"name\":\"vote\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"voteGaugeWeight\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"withdrawAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"b0f63794": "balanceOfPool(address)",
		"3fe9bc06": "claimCrv(address)",
		"2dbfa735": "claimFees(address,address)",
		"ef5cfb8c": "claimRewards(address)",
		"b52c05fe": "createLock(uint256,uint256)",
		"f9609f08": "deposit(address,address)",
		"b61d27f6": "execute(address,uint256,bytes)",
		"15456eba": "increaseAmount(uint256)",
		"3c9a2a1a": "increaseTime(uint256)",
		"570ca735": "operator()",
		"86d1a69f": "release()",
		"fa3964b2": "setStashAccess(address,bool)",
		"e2cdd42a": "vote(uint256,address,bool)",
		"5d7e9bcb": "voteGaugeWeight(address,uint256)",
		"51cff8d9": "withdraw(address)",
		"d9caed12": "withdraw(address,address,uint256)",
		"09cae2c8": "withdrawAll(address,address)",
	},
}

// IStakerABI is the input ABI used to generate the binding from.
// Deprecated: Use IStakerMetaData.ABI instead.
var IStakerABI = IStakerMetaData.ABI

// Deprecated: Use IStakerMetaData.Sigs instead.
// IStakerFuncSigs maps the 4-byte function signature to its string representation.
var IStakerFuncSigs = IStakerMetaData.Sigs

// IStaker is an auto generated Go binding around an Ethereum contract.
type IStaker struct {
	IStakerCaller     // Read-only binding to the contract
	IStakerTransactor // Write-only binding to the contract
	IStakerFilterer   // Log filterer for contract events
}

// IStakerCaller is an auto generated read-only Go binding around an Ethereum contract.
type IStakerCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStakerTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IStakerTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStakerFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IStakerFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStakerSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IStakerSession struct {
	Contract     *IStaker          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IStakerCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IStakerCallerSession struct {
	Contract *IStakerCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// IStakerTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IStakerTransactorSession struct {
	Contract     *IStakerTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// IStakerRaw is an auto generated low-level Go binding around an Ethereum contract.
type IStakerRaw struct {
	Contract *IStaker // Generic contract binding to access the raw methods on
}

// IStakerCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IStakerCallerRaw struct {
	Contract *IStakerCaller // Generic read-only contract binding to access the raw methods on
}

// IStakerTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IStakerTransactorRaw struct {
	Contract *IStakerTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIStaker creates a new instance of IStaker, bound to a specific deployed contract.
func NewIStaker(address common.Address, backend bind.ContractBackend) (*IStaker, error) {
	contract, err := bindIStaker(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IStaker{IStakerCaller: IStakerCaller{contract: contract}, IStakerTransactor: IStakerTransactor{contract: contract}, IStakerFilterer: IStakerFilterer{contract: contract}}, nil
}

// NewIStakerCaller creates a new read-only instance of IStaker, bound to a specific deployed contract.
func NewIStakerCaller(address common.Address, caller bind.ContractCaller) (*IStakerCaller, error) {
	contract, err := bindIStaker(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IStakerCaller{contract: contract}, nil
}

// NewIStakerTransactor creates a new write-only instance of IStaker, bound to a specific deployed contract.
func NewIStakerTransactor(address common.Address, transactor bind.ContractTransactor) (*IStakerTransactor, error) {
	contract, err := bindIStaker(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IStakerTransactor{contract: contract}, nil
}

// NewIStakerFilterer creates a new log filterer instance of IStaker, bound to a specific deployed contract.
func NewIStakerFilterer(address common.Address, filterer bind.ContractFilterer) (*IStakerFilterer, error) {
	contract, err := bindIStaker(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IStakerFilterer{contract: contract}, nil
}

// bindIStaker binds a generic wrapper to an already deployed contract.
func bindIStaker(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IStakerABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStaker *IStakerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStaker.Contract.IStakerCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStaker *IStakerRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStaker.Contract.IStakerTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStaker *IStakerRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStaker.Contract.IStakerTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStaker *IStakerCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStaker.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStaker *IStakerTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStaker.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStaker *IStakerTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStaker.Contract.contract.Transact(opts, method, params...)
}

// BalanceOfPool is a free data retrieval call binding the contract method 0xb0f63794.
//
// Solidity: function balanceOfPool(address ) view returns(uint256)
func (_IStaker *IStakerCaller) BalanceOfPool(opts *bind.CallOpts, arg0 common.Address) (*big.Int, error) {
	var out []interface{}
	err := _IStaker.contract.Call(opts, &out, "balanceOfPool", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOfPool is a free data retrieval call binding the contract method 0xb0f63794.
//
// Solidity: function balanceOfPool(address ) view returns(uint256)
func (_IStaker *IStakerSession) BalanceOfPool(arg0 common.Address) (*big.Int, error) {
	return _IStaker.Contract.BalanceOfPool(&_IStaker.CallOpts, arg0)
}

// BalanceOfPool is a free data retrieval call binding the contract method 0xb0f63794.
//
// Solidity: function balanceOfPool(address ) view returns(uint256)
func (_IStaker *IStakerCallerSession) BalanceOfPool(arg0 common.Address) (*big.Int, error) {
	return _IStaker.Contract.BalanceOfPool(&_IStaker.CallOpts, arg0)
}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_IStaker *IStakerCaller) Operator(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _IStaker.contract.Call(opts, &out, "operator")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_IStaker *IStakerSession) Operator() (common.Address, error) {
	return _IStaker.Contract.Operator(&_IStaker.CallOpts)
}

// Operator is a free data retrieval call binding the contract method 0x570ca735.
//
// Solidity: function operator() view returns(address)
func (_IStaker *IStakerCallerSession) Operator() (common.Address, error) {
	return _IStaker.Contract.Operator(&_IStaker.CallOpts)
}

// ClaimCrv is a paid mutator transaction binding the contract method 0x3fe9bc06.
//
// Solidity: function claimCrv(address ) returns(uint256)
func (_IStaker *IStakerTransactor) ClaimCrv(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "claimCrv", arg0)
}

// ClaimCrv is a paid mutator transaction binding the contract method 0x3fe9bc06.
//
// Solidity: function claimCrv(address ) returns(uint256)
func (_IStaker *IStakerSession) ClaimCrv(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimCrv(&_IStaker.TransactOpts, arg0)
}

// ClaimCrv is a paid mutator transaction binding the contract method 0x3fe9bc06.
//
// Solidity: function claimCrv(address ) returns(uint256)
func (_IStaker *IStakerTransactorSession) ClaimCrv(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimCrv(&_IStaker.TransactOpts, arg0)
}

// ClaimFees is a paid mutator transaction binding the contract method 0x2dbfa735.
//
// Solidity: function claimFees(address , address ) returns()
func (_IStaker *IStakerTransactor) ClaimFees(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "claimFees", arg0, arg1)
}

// ClaimFees is a paid mutator transaction binding the contract method 0x2dbfa735.
//
// Solidity: function claimFees(address , address ) returns()
func (_IStaker *IStakerSession) ClaimFees(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimFees(&_IStaker.TransactOpts, arg0, arg1)
}

// ClaimFees is a paid mutator transaction binding the contract method 0x2dbfa735.
//
// Solidity: function claimFees(address , address ) returns()
func (_IStaker *IStakerTransactorSession) ClaimFees(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimFees(&_IStaker.TransactOpts, arg0, arg1)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xef5cfb8c.
//
// Solidity: function claimRewards(address ) returns()
func (_IStaker *IStakerTransactor) ClaimRewards(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "claimRewards", arg0)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xef5cfb8c.
//
// Solidity: function claimRewards(address ) returns()
func (_IStaker *IStakerSession) ClaimRewards(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimRewards(&_IStaker.TransactOpts, arg0)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0xef5cfb8c.
//
// Solidity: function claimRewards(address ) returns()
func (_IStaker *IStakerTransactorSession) ClaimRewards(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.ClaimRewards(&_IStaker.TransactOpts, arg0)
}

// CreateLock is a paid mutator transaction binding the contract method 0xb52c05fe.
//
// Solidity: function createLock(uint256 , uint256 ) returns()
func (_IStaker *IStakerTransactor) CreateLock(opts *bind.TransactOpts, arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "createLock", arg0, arg1)
}

// CreateLock is a paid mutator transaction binding the contract method 0xb52c05fe.
//
// Solidity: function createLock(uint256 , uint256 ) returns()
func (_IStaker *IStakerSession) CreateLock(arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.CreateLock(&_IStaker.TransactOpts, arg0, arg1)
}

// CreateLock is a paid mutator transaction binding the contract method 0xb52c05fe.
//
// Solidity: function createLock(uint256 , uint256 ) returns()
func (_IStaker *IStakerTransactorSession) CreateLock(arg0 *big.Int, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.CreateLock(&_IStaker.TransactOpts, arg0, arg1)
}

// Deposit is a paid mutator transaction binding the contract method 0xf9609f08.
//
// Solidity: function deposit(address , address ) returns()
func (_IStaker *IStakerTransactor) Deposit(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "deposit", arg0, arg1)
}

// Deposit is a paid mutator transaction binding the contract method 0xf9609f08.
//
// Solidity: function deposit(address , address ) returns()
func (_IStaker *IStakerSession) Deposit(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.Deposit(&_IStaker.TransactOpts, arg0, arg1)
}

// Deposit is a paid mutator transaction binding the contract method 0xf9609f08.
//
// Solidity: function deposit(address , address ) returns()
func (_IStaker *IStakerTransactorSession) Deposit(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.Deposit(&_IStaker.TransactOpts, arg0, arg1)
}

// Execute is a paid mutator transaction binding the contract method 0xb61d27f6.
//
// Solidity: function execute(address _to, uint256 _value, bytes _data) returns(bool, bytes)
func (_IStaker *IStakerTransactor) Execute(opts *bind.TransactOpts, _to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "execute", _to, _value, _data)
}

// Execute is a paid mutator transaction binding the contract method 0xb61d27f6.
//
// Solidity: function execute(address _to, uint256 _value, bytes _data) returns(bool, bytes)
func (_IStaker *IStakerSession) Execute(_to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _IStaker.Contract.Execute(&_IStaker.TransactOpts, _to, _value, _data)
}

// Execute is a paid mutator transaction binding the contract method 0xb61d27f6.
//
// Solidity: function execute(address _to, uint256 _value, bytes _data) returns(bool, bytes)
func (_IStaker *IStakerTransactorSession) Execute(_to common.Address, _value *big.Int, _data []byte) (*types.Transaction, error) {
	return _IStaker.Contract.Execute(&_IStaker.TransactOpts, _to, _value, _data)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x15456eba.
//
// Solidity: function increaseAmount(uint256 ) returns()
func (_IStaker *IStakerTransactor) IncreaseAmount(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "increaseAmount", arg0)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x15456eba.
//
// Solidity: function increaseAmount(uint256 ) returns()
func (_IStaker *IStakerSession) IncreaseAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.IncreaseAmount(&_IStaker.TransactOpts, arg0)
}

// IncreaseAmount is a paid mutator transaction binding the contract method 0x15456eba.
//
// Solidity: function increaseAmount(uint256 ) returns()
func (_IStaker *IStakerTransactorSession) IncreaseAmount(arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.IncreaseAmount(&_IStaker.TransactOpts, arg0)
}

// IncreaseTime is a paid mutator transaction binding the contract method 0x3c9a2a1a.
//
// Solidity: function increaseTime(uint256 ) returns()
func (_IStaker *IStakerTransactor) IncreaseTime(opts *bind.TransactOpts, arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "increaseTime", arg0)
}

// IncreaseTime is a paid mutator transaction binding the contract method 0x3c9a2a1a.
//
// Solidity: function increaseTime(uint256 ) returns()
func (_IStaker *IStakerSession) IncreaseTime(arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.IncreaseTime(&_IStaker.TransactOpts, arg0)
}

// IncreaseTime is a paid mutator transaction binding the contract method 0x3c9a2a1a.
//
// Solidity: function increaseTime(uint256 ) returns()
func (_IStaker *IStakerTransactorSession) IncreaseTime(arg0 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.IncreaseTime(&_IStaker.TransactOpts, arg0)
}

// Release is a paid mutator transaction binding the contract method 0x86d1a69f.
//
// Solidity: function release() returns()
func (_IStaker *IStakerTransactor) Release(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "release")
}

// Release is a paid mutator transaction binding the contract method 0x86d1a69f.
//
// Solidity: function release() returns()
func (_IStaker *IStakerSession) Release() (*types.Transaction, error) {
	return _IStaker.Contract.Release(&_IStaker.TransactOpts)
}

// Release is a paid mutator transaction binding the contract method 0x86d1a69f.
//
// Solidity: function release() returns()
func (_IStaker *IStakerTransactorSession) Release() (*types.Transaction, error) {
	return _IStaker.Contract.Release(&_IStaker.TransactOpts)
}

// SetStashAccess is a paid mutator transaction binding the contract method 0xfa3964b2.
//
// Solidity: function setStashAccess(address , bool ) returns()
func (_IStaker *IStakerTransactor) SetStashAccess(opts *bind.TransactOpts, arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "setStashAccess", arg0, arg1)
}

// SetStashAccess is a paid mutator transaction binding the contract method 0xfa3964b2.
//
// Solidity: function setStashAccess(address , bool ) returns()
func (_IStaker *IStakerSession) SetStashAccess(arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IStaker.Contract.SetStashAccess(&_IStaker.TransactOpts, arg0, arg1)
}

// SetStashAccess is a paid mutator transaction binding the contract method 0xfa3964b2.
//
// Solidity: function setStashAccess(address , bool ) returns()
func (_IStaker *IStakerTransactorSession) SetStashAccess(arg0 common.Address, arg1 bool) (*types.Transaction, error) {
	return _IStaker.Contract.SetStashAccess(&_IStaker.TransactOpts, arg0, arg1)
}

// Vote is a paid mutator transaction binding the contract method 0xe2cdd42a.
//
// Solidity: function vote(uint256 , address , bool ) returns()
func (_IStaker *IStakerTransactor) Vote(opts *bind.TransactOpts, arg0 *big.Int, arg1 common.Address, arg2 bool) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "vote", arg0, arg1, arg2)
}

// Vote is a paid mutator transaction binding the contract method 0xe2cdd42a.
//
// Solidity: function vote(uint256 , address , bool ) returns()
func (_IStaker *IStakerSession) Vote(arg0 *big.Int, arg1 common.Address, arg2 bool) (*types.Transaction, error) {
	return _IStaker.Contract.Vote(&_IStaker.TransactOpts, arg0, arg1, arg2)
}

// Vote is a paid mutator transaction binding the contract method 0xe2cdd42a.
//
// Solidity: function vote(uint256 , address , bool ) returns()
func (_IStaker *IStakerTransactorSession) Vote(arg0 *big.Int, arg1 common.Address, arg2 bool) (*types.Transaction, error) {
	return _IStaker.Contract.Vote(&_IStaker.TransactOpts, arg0, arg1, arg2)
}

// VoteGaugeWeight is a paid mutator transaction binding the contract method 0x5d7e9bcb.
//
// Solidity: function voteGaugeWeight(address , uint256 ) returns()
func (_IStaker *IStakerTransactor) VoteGaugeWeight(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "voteGaugeWeight", arg0, arg1)
}

// VoteGaugeWeight is a paid mutator transaction binding the contract method 0x5d7e9bcb.
//
// Solidity: function voteGaugeWeight(address , uint256 ) returns()
func (_IStaker *IStakerSession) VoteGaugeWeight(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.VoteGaugeWeight(&_IStaker.TransactOpts, arg0, arg1)
}

// VoteGaugeWeight is a paid mutator transaction binding the contract method 0x5d7e9bcb.
//
// Solidity: function voteGaugeWeight(address , uint256 ) returns()
func (_IStaker *IStakerTransactorSession) VoteGaugeWeight(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.VoteGaugeWeight(&_IStaker.TransactOpts, arg0, arg1)
}

// Withdraw is a paid mutator transaction binding the contract method 0x51cff8d9.
//
// Solidity: function withdraw(address ) returns()
func (_IStaker *IStakerTransactor) Withdraw(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "withdraw", arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x51cff8d9.
//
// Solidity: function withdraw(address ) returns()
func (_IStaker *IStakerSession) Withdraw(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.Withdraw(&_IStaker.TransactOpts, arg0)
}

// Withdraw is a paid mutator transaction binding the contract method 0x51cff8d9.
//
// Solidity: function withdraw(address ) returns()
func (_IStaker *IStakerTransactorSession) Withdraw(arg0 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.Withdraw(&_IStaker.TransactOpts, arg0)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0xd9caed12.
//
// Solidity: function withdraw(address , address , uint256 ) returns()
func (_IStaker *IStakerTransactor) Withdraw0(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "withdraw0", arg0, arg1, arg2)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0xd9caed12.
//
// Solidity: function withdraw(address , address , uint256 ) returns()
func (_IStaker *IStakerSession) Withdraw0(arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.Withdraw0(&_IStaker.TransactOpts, arg0, arg1, arg2)
}

// Withdraw0 is a paid mutator transaction binding the contract method 0xd9caed12.
//
// Solidity: function withdraw(address , address , uint256 ) returns()
func (_IStaker *IStakerTransactorSession) Withdraw0(arg0 common.Address, arg1 common.Address, arg2 *big.Int) (*types.Transaction, error) {
	return _IStaker.Contract.Withdraw0(&_IStaker.TransactOpts, arg0, arg1, arg2)
}

// WithdrawAll is a paid mutator transaction binding the contract method 0x09cae2c8.
//
// Solidity: function withdrawAll(address , address ) returns()
func (_IStaker *IStakerTransactor) WithdrawAll(opts *bind.TransactOpts, arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.contract.Transact(opts, "withdrawAll", arg0, arg1)
}

// WithdrawAll is a paid mutator transaction binding the contract method 0x09cae2c8.
//
// Solidity: function withdrawAll(address , address ) returns()
func (_IStaker *IStakerSession) WithdrawAll(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.WithdrawAll(&_IStaker.TransactOpts, arg0, arg1)
}

// WithdrawAll is a paid mutator transaction binding the contract method 0x09cae2c8.
//
// Solidity: function withdrawAll(address , address ) returns()
func (_IStaker *IStakerTransactorSession) WithdrawAll(arg0 common.Address, arg1 common.Address) (*types.Transaction, error) {
	return _IStaker.Contract.WithdrawAll(&_IStaker.TransactOpts, arg0, arg1)
}

// IStashMetaData contains all meta data concerning the IStash contract.
var IStashMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"name\":\"claimRewards\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"processStash\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"stashRewards\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"372500ab": "claimRewards()",
		"ca8b0176": "processStash()",
		"b87bd481": "stashRewards()",
	},
}

// IStashABI is the input ABI used to generate the binding from.
// Deprecated: Use IStashMetaData.ABI instead.
var IStashABI = IStashMetaData.ABI

// Deprecated: Use IStashMetaData.Sigs instead.
// IStashFuncSigs maps the 4-byte function signature to its string representation.
var IStashFuncSigs = IStashMetaData.Sigs

// IStash is an auto generated Go binding around an Ethereum contract.
type IStash struct {
	IStashCaller     // Read-only binding to the contract
	IStashTransactor // Write-only binding to the contract
	IStashFilterer   // Log filterer for contract events
}

// IStashCaller is an auto generated read-only Go binding around an Ethereum contract.
type IStashCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IStashTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IStashFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IStashSession struct {
	Contract     *IStash           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IStashCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IStashCallerSession struct {
	Contract *IStashCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// IStashTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IStashTransactorSession struct {
	Contract     *IStashTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IStashRaw is an auto generated low-level Go binding around an Ethereum contract.
type IStashRaw struct {
	Contract *IStash // Generic contract binding to access the raw methods on
}

// IStashCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IStashCallerRaw struct {
	Contract *IStashCaller // Generic read-only contract binding to access the raw methods on
}

// IStashTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IStashTransactorRaw struct {
	Contract *IStashTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIStash creates a new instance of IStash, bound to a specific deployed contract.
func NewIStash(address common.Address, backend bind.ContractBackend) (*IStash, error) {
	contract, err := bindIStash(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IStash{IStashCaller: IStashCaller{contract: contract}, IStashTransactor: IStashTransactor{contract: contract}, IStashFilterer: IStashFilterer{contract: contract}}, nil
}

// NewIStashCaller creates a new read-only instance of IStash, bound to a specific deployed contract.
func NewIStashCaller(address common.Address, caller bind.ContractCaller) (*IStashCaller, error) {
	contract, err := bindIStash(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IStashCaller{contract: contract}, nil
}

// NewIStashTransactor creates a new write-only instance of IStash, bound to a specific deployed contract.
func NewIStashTransactor(address common.Address, transactor bind.ContractTransactor) (*IStashTransactor, error) {
	contract, err := bindIStash(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IStashTransactor{contract: contract}, nil
}

// NewIStashFilterer creates a new log filterer instance of IStash, bound to a specific deployed contract.
func NewIStashFilterer(address common.Address, filterer bind.ContractFilterer) (*IStashFilterer, error) {
	contract, err := bindIStash(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IStashFilterer{contract: contract}, nil
}

// bindIStash binds a generic wrapper to an already deployed contract.
func bindIStash(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IStashABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStash *IStashRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStash.Contract.IStashCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStash *IStashRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStash.Contract.IStashTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStash *IStashRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStash.Contract.IStashTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStash *IStashCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStash.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStash *IStashTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStash.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStash *IStashTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStash.Contract.contract.Transact(opts, method, params...)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns(bool)
func (_IStash *IStashTransactor) ClaimRewards(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStash.contract.Transact(opts, "claimRewards")
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns(bool)
func (_IStash *IStashSession) ClaimRewards() (*types.Transaction, error) {
	return _IStash.Contract.ClaimRewards(&_IStash.TransactOpts)
}

// ClaimRewards is a paid mutator transaction binding the contract method 0x372500ab.
//
// Solidity: function claimRewards() returns(bool)
func (_IStash *IStashTransactorSession) ClaimRewards() (*types.Transaction, error) {
	return _IStash.Contract.ClaimRewards(&_IStash.TransactOpts)
}

// ProcessStash is a paid mutator transaction binding the contract method 0xca8b0176.
//
// Solidity: function processStash() returns(bool)
func (_IStash *IStashTransactor) ProcessStash(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStash.contract.Transact(opts, "processStash")
}

// ProcessStash is a paid mutator transaction binding the contract method 0xca8b0176.
//
// Solidity: function processStash() returns(bool)
func (_IStash *IStashSession) ProcessStash() (*types.Transaction, error) {
	return _IStash.Contract.ProcessStash(&_IStash.TransactOpts)
}

// ProcessStash is a paid mutator transaction binding the contract method 0xca8b0176.
//
// Solidity: function processStash() returns(bool)
func (_IStash *IStashTransactorSession) ProcessStash() (*types.Transaction, error) {
	return _IStash.Contract.ProcessStash(&_IStash.TransactOpts)
}

// StashRewards is a paid mutator transaction binding the contract method 0xb87bd481.
//
// Solidity: function stashRewards() returns(bool)
func (_IStash *IStashTransactor) StashRewards(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStash.contract.Transact(opts, "stashRewards")
}

// StashRewards is a paid mutator transaction binding the contract method 0xb87bd481.
//
// Solidity: function stashRewards() returns(bool)
func (_IStash *IStashSession) StashRewards() (*types.Transaction, error) {
	return _IStash.Contract.StashRewards(&_IStash.TransactOpts)
}

// StashRewards is a paid mutator transaction binding the contract method 0xb87bd481.
//
// Solidity: function stashRewards() returns(bool)
func (_IStash *IStashTransactorSession) StashRewards() (*types.Transaction, error) {
	return _IStash.Contract.StashRewards(&_IStash.TransactOpts)
}

// IStashFactoryMetaData contains all meta data concerning the IStashFactory contract.
var IStashFactoryMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"CreateStash\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"99cb12de": "CreateStash(uint256,address,address,uint256)",
	},
}

// IStashFactoryABI is the input ABI used to generate the binding from.
// Deprecated: Use IStashFactoryMetaData.ABI instead.
var IStashFactoryABI = IStashFactoryMetaData.ABI

// Deprecated: Use IStashFactoryMetaData.Sigs instead.
// IStashFactoryFuncSigs maps the 4-byte function signature to its string representation.
var IStashFactoryFuncSigs = IStashFactoryMetaData.Sigs

// IStashFactory is an auto generated Go binding around an Ethereum contract.
type IStashFactory struct {
	IStashFactoryCaller     // Read-only binding to the contract
	IStashFactoryTransactor // Write-only binding to the contract
	IStashFactoryFilterer   // Log filterer for contract events
}

// IStashFactoryCaller is an auto generated read-only Go binding around an Ethereum contract.
type IStashFactoryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashFactoryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IStashFactoryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashFactoryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IStashFactoryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IStashFactorySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IStashFactorySession struct {
	Contract     *IStashFactory    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IStashFactoryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IStashFactoryCallerSession struct {
	Contract *IStashFactoryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// IStashFactoryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IStashFactoryTransactorSession struct {
	Contract     *IStashFactoryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// IStashFactoryRaw is an auto generated low-level Go binding around an Ethereum contract.
type IStashFactoryRaw struct {
	Contract *IStashFactory // Generic contract binding to access the raw methods on
}

// IStashFactoryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IStashFactoryCallerRaw struct {
	Contract *IStashFactoryCaller // Generic read-only contract binding to access the raw methods on
}

// IStashFactoryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IStashFactoryTransactorRaw struct {
	Contract *IStashFactoryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIStashFactory creates a new instance of IStashFactory, bound to a specific deployed contract.
func NewIStashFactory(address common.Address, backend bind.ContractBackend) (*IStashFactory, error) {
	contract, err := bindIStashFactory(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IStashFactory{IStashFactoryCaller: IStashFactoryCaller{contract: contract}, IStashFactoryTransactor: IStashFactoryTransactor{contract: contract}, IStashFactoryFilterer: IStashFactoryFilterer{contract: contract}}, nil
}

// NewIStashFactoryCaller creates a new read-only instance of IStashFactory, bound to a specific deployed contract.
func NewIStashFactoryCaller(address common.Address, caller bind.ContractCaller) (*IStashFactoryCaller, error) {
	contract, err := bindIStashFactory(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IStashFactoryCaller{contract: contract}, nil
}

// NewIStashFactoryTransactor creates a new write-only instance of IStashFactory, bound to a specific deployed contract.
func NewIStashFactoryTransactor(address common.Address, transactor bind.ContractTransactor) (*IStashFactoryTransactor, error) {
	contract, err := bindIStashFactory(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IStashFactoryTransactor{contract: contract}, nil
}

// NewIStashFactoryFilterer creates a new log filterer instance of IStashFactory, bound to a specific deployed contract.
func NewIStashFactoryFilterer(address common.Address, filterer bind.ContractFilterer) (*IStashFactoryFilterer, error) {
	contract, err := bindIStashFactory(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IStashFactoryFilterer{contract: contract}, nil
}

// bindIStashFactory binds a generic wrapper to an already deployed contract.
func bindIStashFactory(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IStashFactoryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStashFactory *IStashFactoryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStashFactory.Contract.IStashFactoryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStashFactory *IStashFactoryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStashFactory.Contract.IStashFactoryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStashFactory *IStashFactoryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStashFactory.Contract.IStashFactoryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IStashFactory *IStashFactoryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IStashFactory.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IStashFactory *IStashFactoryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IStashFactory.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IStashFactory *IStashFactoryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IStashFactory.Contract.contract.Transact(opts, method, params...)
}

// CreateStash is a paid mutator transaction binding the contract method 0x99cb12de.
//
// Solidity: function CreateStash(uint256 , address , address , uint256 ) returns(address)
func (_IStashFactory *IStashFactoryTransactor) CreateStash(opts *bind.TransactOpts, arg0 *big.Int, arg1 common.Address, arg2 common.Address, arg3 *big.Int) (*types.Transaction, error) {
	return _IStashFactory.contract.Transact(opts, "CreateStash", arg0, arg1, arg2, arg3)
}

// CreateStash is a paid mutator transaction binding the contract method 0x99cb12de.
//
// Solidity: function CreateStash(uint256 , address , address , uint256 ) returns(address)
func (_IStashFactory *IStashFactorySession) CreateStash(arg0 *big.Int, arg1 common.Address, arg2 common.Address, arg3 *big.Int) (*types.Transaction, error) {
	return _IStashFactory.Contract.CreateStash(&_IStashFactory.TransactOpts, arg0, arg1, arg2, arg3)
}

// CreateStash is a paid mutator transaction binding the contract method 0x99cb12de.
//
// Solidity: function CreateStash(uint256 , address , address , uint256 ) returns(address)
func (_IStashFactory *IStashFactoryTransactorSession) CreateStash(arg0 *big.Int, arg1 common.Address, arg2 common.Address, arg3 *big.Int) (*types.Transaction, error) {
	return _IStashFactory.Contract.CreateStash(&_IStashFactory.TransactOpts, arg0, arg1, arg2, arg3)
}

// ITokenFactoryMetaData contains all meta data concerning the ITokenFactory contract.
var ITokenFactoryMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"CreateDepositToken\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"114a899c": "CreateDepositToken(address)",
	},
}

// ITokenFactoryABI is the input ABI used to generate the binding from.
// Deprecated: Use ITokenFactoryMetaData.ABI instead.
var ITokenFactoryABI = ITokenFactoryMetaData.ABI

// Deprecated: Use ITokenFactoryMetaData.Sigs instead.
// ITokenFactoryFuncSigs maps the 4-byte function signature to its string representation.
var ITokenFactoryFuncSigs = ITokenFactoryMetaData.Sigs

// ITokenFactory is an auto generated Go binding around an Ethereum contract.
type ITokenFactory struct {
	ITokenFactoryCaller     // Read-only binding to the contract
	ITokenFactoryTransactor // Write-only binding to the contract
	ITokenFactoryFilterer   // Log filterer for contract events
}

// ITokenFactoryCaller is an auto generated read-only Go binding around an Ethereum contract.
type ITokenFactoryCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenFactoryTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ITokenFactoryTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenFactoryFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ITokenFactoryFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenFactorySession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ITokenFactorySession struct {
	Contract     *ITokenFactory    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ITokenFactoryCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ITokenFactoryCallerSession struct {
	Contract *ITokenFactoryCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// ITokenFactoryTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ITokenFactoryTransactorSession struct {
	Contract     *ITokenFactoryTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// ITokenFactoryRaw is an auto generated low-level Go binding around an Ethereum contract.
type ITokenFactoryRaw struct {
	Contract *ITokenFactory // Generic contract binding to access the raw methods on
}

// ITokenFactoryCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ITokenFactoryCallerRaw struct {
	Contract *ITokenFactoryCaller // Generic read-only contract binding to access the raw methods on
}

// ITokenFactoryTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ITokenFactoryTransactorRaw struct {
	Contract *ITokenFactoryTransactor // Generic write-only contract binding to access the raw methods on
}

// NewITokenFactory creates a new instance of ITokenFactory, bound to a specific deployed contract.
func NewITokenFactory(address common.Address, backend bind.ContractBackend) (*ITokenFactory, error) {
	contract, err := bindITokenFactory(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ITokenFactory{ITokenFactoryCaller: ITokenFactoryCaller{contract: contract}, ITokenFactoryTransactor: ITokenFactoryTransactor{contract: contract}, ITokenFactoryFilterer: ITokenFactoryFilterer{contract: contract}}, nil
}

// NewITokenFactoryCaller creates a new read-only instance of ITokenFactory, bound to a specific deployed contract.
func NewITokenFactoryCaller(address common.Address, caller bind.ContractCaller) (*ITokenFactoryCaller, error) {
	contract, err := bindITokenFactory(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ITokenFactoryCaller{contract: contract}, nil
}

// NewITokenFactoryTransactor creates a new write-only instance of ITokenFactory, bound to a specific deployed contract.
func NewITokenFactoryTransactor(address common.Address, transactor bind.ContractTransactor) (*ITokenFactoryTransactor, error) {
	contract, err := bindITokenFactory(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ITokenFactoryTransactor{contract: contract}, nil
}

// NewITokenFactoryFilterer creates a new log filterer instance of ITokenFactory, bound to a specific deployed contract.
func NewITokenFactoryFilterer(address common.Address, filterer bind.ContractFilterer) (*ITokenFactoryFilterer, error) {
	contract, err := bindITokenFactory(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ITokenFactoryFilterer{contract: contract}, nil
}

// bindITokenFactory binds a generic wrapper to an already deployed contract.
func bindITokenFactory(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ITokenFactoryABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ITokenFactory *ITokenFactoryRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ITokenFactory.Contract.ITokenFactoryCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ITokenFactory *ITokenFactoryRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ITokenFactory.Contract.ITokenFactoryTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ITokenFactory *ITokenFactoryRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ITokenFactory.Contract.ITokenFactoryTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ITokenFactory *ITokenFactoryCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ITokenFactory.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ITokenFactory *ITokenFactoryTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ITokenFactory.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ITokenFactory *ITokenFactoryTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ITokenFactory.Contract.contract.Transact(opts, method, params...)
}

// CreateDepositToken is a paid mutator transaction binding the contract method 0x114a899c.
//
// Solidity: function CreateDepositToken(address ) returns(address)
func (_ITokenFactory *ITokenFactoryTransactor) CreateDepositToken(opts *bind.TransactOpts, arg0 common.Address) (*types.Transaction, error) {
	return _ITokenFactory.contract.Transact(opts, "CreateDepositToken", arg0)
}

// CreateDepositToken is a paid mutator transaction binding the contract method 0x114a899c.
//
// Solidity: function CreateDepositToken(address ) returns(address)
func (_ITokenFactory *ITokenFactorySession) CreateDepositToken(arg0 common.Address) (*types.Transaction, error) {
	return _ITokenFactory.Contract.CreateDepositToken(&_ITokenFactory.TransactOpts, arg0)
}

// CreateDepositToken is a paid mutator transaction binding the contract method 0x114a899c.
//
// Solidity: function CreateDepositToken(address ) returns(address)
func (_ITokenFactory *ITokenFactoryTransactorSession) CreateDepositToken(arg0 common.Address) (*types.Transaction, error) {
	return _ITokenFactory.Contract.CreateDepositToken(&_ITokenFactory.TransactOpts, arg0)
}

// ITokenMinterMetaData contains all meta data concerning the ITokenMinter contract.
var ITokenMinterMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"burn\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"mint\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"9dc29fac": "burn(address,uint256)",
		"40c10f19": "mint(address,uint256)",
	},
}

// ITokenMinterABI is the input ABI used to generate the binding from.
// Deprecated: Use ITokenMinterMetaData.ABI instead.
var ITokenMinterABI = ITokenMinterMetaData.ABI

// Deprecated: Use ITokenMinterMetaData.Sigs instead.
// ITokenMinterFuncSigs maps the 4-byte function signature to its string representation.
var ITokenMinterFuncSigs = ITokenMinterMetaData.Sigs

// ITokenMinter is an auto generated Go binding around an Ethereum contract.
type ITokenMinter struct {
	ITokenMinterCaller     // Read-only binding to the contract
	ITokenMinterTransactor // Write-only binding to the contract
	ITokenMinterFilterer   // Log filterer for contract events
}

// ITokenMinterCaller is an auto generated read-only Go binding around an Ethereum contract.
type ITokenMinterCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenMinterTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ITokenMinterTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenMinterFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ITokenMinterFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ITokenMinterSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ITokenMinterSession struct {
	Contract     *ITokenMinter     // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ITokenMinterCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ITokenMinterCallerSession struct {
	Contract *ITokenMinterCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts       // Call options to use throughout this session
}

// ITokenMinterTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ITokenMinterTransactorSession struct {
	Contract     *ITokenMinterTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts       // Transaction auth options to use throughout this session
}

// ITokenMinterRaw is an auto generated low-level Go binding around an Ethereum contract.
type ITokenMinterRaw struct {
	Contract *ITokenMinter // Generic contract binding to access the raw methods on
}

// ITokenMinterCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ITokenMinterCallerRaw struct {
	Contract *ITokenMinterCaller // Generic read-only contract binding to access the raw methods on
}

// ITokenMinterTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ITokenMinterTransactorRaw struct {
	Contract *ITokenMinterTransactor // Generic write-only contract binding to access the raw methods on
}

// NewITokenMinter creates a new instance of ITokenMinter, bound to a specific deployed contract.
func NewITokenMinter(address common.Address, backend bind.ContractBackend) (*ITokenMinter, error) {
	contract, err := bindITokenMinter(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ITokenMinter{ITokenMinterCaller: ITokenMinterCaller{contract: contract}, ITokenMinterTransactor: ITokenMinterTransactor{contract: contract}, ITokenMinterFilterer: ITokenMinterFilterer{contract: contract}}, nil
}

// NewITokenMinterCaller creates a new read-only instance of ITokenMinter, bound to a specific deployed contract.
func NewITokenMinterCaller(address common.Address, caller bind.ContractCaller) (*ITokenMinterCaller, error) {
	contract, err := bindITokenMinter(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ITokenMinterCaller{contract: contract}, nil
}

// NewITokenMinterTransactor creates a new write-only instance of ITokenMinter, bound to a specific deployed contract.
func NewITokenMinterTransactor(address common.Address, transactor bind.ContractTransactor) (*ITokenMinterTransactor, error) {
	contract, err := bindITokenMinter(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ITokenMinterTransactor{contract: contract}, nil
}

// NewITokenMinterFilterer creates a new log filterer instance of ITokenMinter, bound to a specific deployed contract.
func NewITokenMinterFilterer(address common.Address, filterer bind.ContractFilterer) (*ITokenMinterFilterer, error) {
	contract, err := bindITokenMinter(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ITokenMinterFilterer{contract: contract}, nil
}

// bindITokenMinter binds a generic wrapper to an already deployed contract.
func bindITokenMinter(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ITokenMinterABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ITokenMinter *ITokenMinterRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ITokenMinter.Contract.ITokenMinterCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ITokenMinter *ITokenMinterRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ITokenMinter.Contract.ITokenMinterTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ITokenMinter *ITokenMinterRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ITokenMinter.Contract.ITokenMinterTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ITokenMinter *ITokenMinterCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ITokenMinter.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ITokenMinter *ITokenMinterTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ITokenMinter.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ITokenMinter *ITokenMinterTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ITokenMinter.Contract.contract.Transact(opts, method, params...)
}

// Burn is a paid mutator transaction binding the contract method 0x9dc29fac.
//
// Solidity: function burn(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterTransactor) Burn(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.contract.Transact(opts, "burn", arg0, arg1)
}

// Burn is a paid mutator transaction binding the contract method 0x9dc29fac.
//
// Solidity: function burn(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterSession) Burn(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.Contract.Burn(&_ITokenMinter.TransactOpts, arg0, arg1)
}

// Burn is a paid mutator transaction binding the contract method 0x9dc29fac.
//
// Solidity: function burn(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterTransactorSession) Burn(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.Contract.Burn(&_ITokenMinter.TransactOpts, arg0, arg1)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterTransactor) Mint(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.contract.Transact(opts, "mint", arg0, arg1)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterSession) Mint(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.Contract.Mint(&_ITokenMinter.TransactOpts, arg0, arg1)
}

// Mint is a paid mutator transaction binding the contract method 0x40c10f19.
//
// Solidity: function mint(address , uint256 ) returns()
func (_ITokenMinter *ITokenMinterTransactorSession) Mint(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _ITokenMinter.Contract.Mint(&_ITokenMinter.TransactOpts, arg0, arg1)
}

// IVestedEscrowMetaData contains all meta data concerning the IVestedEscrow contract.
var IVestedEscrowMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"_recipient\",\"type\":\"address[]\"},{\"internalType\":\"uint256[]\",\"name\":\"_amount\",\"type\":\"uint256[]\"}],\"name\":\"fund\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"b1e56f6b": "fund(address[],uint256[])",
	},
}

// IVestedEscrowABI is the input ABI used to generate the binding from.
// Deprecated: Use IVestedEscrowMetaData.ABI instead.
var IVestedEscrowABI = IVestedEscrowMetaData.ABI

// Deprecated: Use IVestedEscrowMetaData.Sigs instead.
// IVestedEscrowFuncSigs maps the 4-byte function signature to its string representation.
var IVestedEscrowFuncSigs = IVestedEscrowMetaData.Sigs

// IVestedEscrow is an auto generated Go binding around an Ethereum contract.
type IVestedEscrow struct {
	IVestedEscrowCaller     // Read-only binding to the contract
	IVestedEscrowTransactor // Write-only binding to the contract
	IVestedEscrowFilterer   // Log filterer for contract events
}

// IVestedEscrowCaller is an auto generated read-only Go binding around an Ethereum contract.
type IVestedEscrowCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVestedEscrowTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IVestedEscrowTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVestedEscrowFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IVestedEscrowFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVestedEscrowSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IVestedEscrowSession struct {
	Contract     *IVestedEscrow    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IVestedEscrowCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IVestedEscrowCallerSession struct {
	Contract *IVestedEscrowCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// IVestedEscrowTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IVestedEscrowTransactorSession struct {
	Contract     *IVestedEscrowTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// IVestedEscrowRaw is an auto generated low-level Go binding around an Ethereum contract.
type IVestedEscrowRaw struct {
	Contract *IVestedEscrow // Generic contract binding to access the raw methods on
}

// IVestedEscrowCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IVestedEscrowCallerRaw struct {
	Contract *IVestedEscrowCaller // Generic read-only contract binding to access the raw methods on
}

// IVestedEscrowTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IVestedEscrowTransactorRaw struct {
	Contract *IVestedEscrowTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIVestedEscrow creates a new instance of IVestedEscrow, bound to a specific deployed contract.
func NewIVestedEscrow(address common.Address, backend bind.ContractBackend) (*IVestedEscrow, error) {
	contract, err := bindIVestedEscrow(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IVestedEscrow{IVestedEscrowCaller: IVestedEscrowCaller{contract: contract}, IVestedEscrowTransactor: IVestedEscrowTransactor{contract: contract}, IVestedEscrowFilterer: IVestedEscrowFilterer{contract: contract}}, nil
}

// NewIVestedEscrowCaller creates a new read-only instance of IVestedEscrow, bound to a specific deployed contract.
func NewIVestedEscrowCaller(address common.Address, caller bind.ContractCaller) (*IVestedEscrowCaller, error) {
	contract, err := bindIVestedEscrow(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IVestedEscrowCaller{contract: contract}, nil
}

// NewIVestedEscrowTransactor creates a new write-only instance of IVestedEscrow, bound to a specific deployed contract.
func NewIVestedEscrowTransactor(address common.Address, transactor bind.ContractTransactor) (*IVestedEscrowTransactor, error) {
	contract, err := bindIVestedEscrow(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IVestedEscrowTransactor{contract: contract}, nil
}

// NewIVestedEscrowFilterer creates a new log filterer instance of IVestedEscrow, bound to a specific deployed contract.
func NewIVestedEscrowFilterer(address common.Address, filterer bind.ContractFilterer) (*IVestedEscrowFilterer, error) {
	contract, err := bindIVestedEscrow(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IVestedEscrowFilterer{contract: contract}, nil
}

// bindIVestedEscrow binds a generic wrapper to an already deployed contract.
func bindIVestedEscrow(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IVestedEscrowABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IVestedEscrow *IVestedEscrowRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IVestedEscrow.Contract.IVestedEscrowCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IVestedEscrow *IVestedEscrowRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.IVestedEscrowTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IVestedEscrow *IVestedEscrowRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.IVestedEscrowTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IVestedEscrow *IVestedEscrowCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IVestedEscrow.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IVestedEscrow *IVestedEscrowTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IVestedEscrow *IVestedEscrowTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.contract.Transact(opts, method, params...)
}

// Fund is a paid mutator transaction binding the contract method 0xb1e56f6b.
//
// Solidity: function fund(address[] _recipient, uint256[] _amount) returns(bool)
func (_IVestedEscrow *IVestedEscrowTransactor) Fund(opts *bind.TransactOpts, _recipient []common.Address, _amount []*big.Int) (*types.Transaction, error) {
	return _IVestedEscrow.contract.Transact(opts, "fund", _recipient, _amount)
}

// Fund is a paid mutator transaction binding the contract method 0xb1e56f6b.
//
// Solidity: function fund(address[] _recipient, uint256[] _amount) returns(bool)
func (_IVestedEscrow *IVestedEscrowSession) Fund(_recipient []common.Address, _amount []*big.Int) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.Fund(&_IVestedEscrow.TransactOpts, _recipient, _amount)
}

// Fund is a paid mutator transaction binding the contract method 0xb1e56f6b.
//
// Solidity: function fund(address[] _recipient, uint256[] _amount) returns(bool)
func (_IVestedEscrow *IVestedEscrowTransactorSession) Fund(_recipient []common.Address, _amount []*big.Int) (*types.Transaction, error) {
	return _IVestedEscrow.Contract.Fund(&_IVestedEscrow.TransactOpts, _recipient, _amount)
}

// IVotingMetaData contains all meta data concerning the IVoting contract.
var IVotingMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"getVote\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"uint64\",\"name\":\"\",\"type\":\"uint64\"},{\"internalType\":\"uint64\",\"name\":\"\",\"type\":\"uint64\"},{\"internalType\":\"uint64\",\"name\":\"\",\"type\":\"uint64\"},{\"internalType\":\"uint64\",\"name\":\"\",\"type\":\"uint64\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"\",\"type\":\"bytes\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"name\":\"vote\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"vote_for_gauge_weights\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"5a55c1f0": "getVote(uint256)",
		"df133bca": "vote(uint256,bool,bool)",
		"d7136328": "vote_for_gauge_weights(address,uint256)",
	},
}

// IVotingABI is the input ABI used to generate the binding from.
// Deprecated: Use IVotingMetaData.ABI instead.
var IVotingABI = IVotingMetaData.ABI

// Deprecated: Use IVotingMetaData.Sigs instead.
// IVotingFuncSigs maps the 4-byte function signature to its string representation.
var IVotingFuncSigs = IVotingMetaData.Sigs

// IVoting is an auto generated Go binding around an Ethereum contract.
type IVoting struct {
	IVotingCaller     // Read-only binding to the contract
	IVotingTransactor // Write-only binding to the contract
	IVotingFilterer   // Log filterer for contract events
}

// IVotingCaller is an auto generated read-only Go binding around an Ethereum contract.
type IVotingCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVotingTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IVotingTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVotingFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IVotingFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IVotingSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IVotingSession struct {
	Contract     *IVoting          // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IVotingCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IVotingCallerSession struct {
	Contract *IVotingCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts  // Call options to use throughout this session
}

// IVotingTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IVotingTransactorSession struct {
	Contract     *IVotingTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts  // Transaction auth options to use throughout this session
}

// IVotingRaw is an auto generated low-level Go binding around an Ethereum contract.
type IVotingRaw struct {
	Contract *IVoting // Generic contract binding to access the raw methods on
}

// IVotingCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IVotingCallerRaw struct {
	Contract *IVotingCaller // Generic read-only contract binding to access the raw methods on
}

// IVotingTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IVotingTransactorRaw struct {
	Contract *IVotingTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIVoting creates a new instance of IVoting, bound to a specific deployed contract.
func NewIVoting(address common.Address, backend bind.ContractBackend) (*IVoting, error) {
	contract, err := bindIVoting(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IVoting{IVotingCaller: IVotingCaller{contract: contract}, IVotingTransactor: IVotingTransactor{contract: contract}, IVotingFilterer: IVotingFilterer{contract: contract}}, nil
}

// NewIVotingCaller creates a new read-only instance of IVoting, bound to a specific deployed contract.
func NewIVotingCaller(address common.Address, caller bind.ContractCaller) (*IVotingCaller, error) {
	contract, err := bindIVoting(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IVotingCaller{contract: contract}, nil
}

// NewIVotingTransactor creates a new write-only instance of IVoting, bound to a specific deployed contract.
func NewIVotingTransactor(address common.Address, transactor bind.ContractTransactor) (*IVotingTransactor, error) {
	contract, err := bindIVoting(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IVotingTransactor{contract: contract}, nil
}

// NewIVotingFilterer creates a new log filterer instance of IVoting, bound to a specific deployed contract.
func NewIVotingFilterer(address common.Address, filterer bind.ContractFilterer) (*IVotingFilterer, error) {
	contract, err := bindIVoting(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IVotingFilterer{contract: contract}, nil
}

// bindIVoting binds a generic wrapper to an already deployed contract.
func bindIVoting(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IVotingABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IVoting *IVotingRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IVoting.Contract.IVotingCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IVoting *IVotingRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IVoting.Contract.IVotingTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IVoting *IVotingRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IVoting.Contract.IVotingTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IVoting *IVotingCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IVoting.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IVoting *IVotingTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IVoting.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IVoting *IVotingTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IVoting.Contract.contract.Transact(opts, method, params...)
}

// GetVote is a free data retrieval call binding the contract method 0x5a55c1f0.
//
// Solidity: function getVote(uint256 ) view returns(bool, bool, uint64, uint64, uint64, uint64, uint256, uint256, uint256, bytes)
func (_IVoting *IVotingCaller) GetVote(opts *bind.CallOpts, arg0 *big.Int) (bool, bool, uint64, uint64, uint64, uint64, *big.Int, *big.Int, *big.Int, []byte, error) {
	var out []interface{}
	err := _IVoting.contract.Call(opts, &out, "getVote", arg0)

	if err != nil {
		return *new(bool), *new(bool), *new(uint64), *new(uint64), *new(uint64), *new(uint64), *new(*big.Int), *new(*big.Int), *new(*big.Int), *new([]byte), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)
	out1 := *abi.ConvertType(out[1], new(bool)).(*bool)
	out2 := *abi.ConvertType(out[2], new(uint64)).(*uint64)
	out3 := *abi.ConvertType(out[3], new(uint64)).(*uint64)
	out4 := *abi.ConvertType(out[4], new(uint64)).(*uint64)
	out5 := *abi.ConvertType(out[5], new(uint64)).(*uint64)
	out6 := *abi.ConvertType(out[6], new(*big.Int)).(**big.Int)
	out7 := *abi.ConvertType(out[7], new(*big.Int)).(**big.Int)
	out8 := *abi.ConvertType(out[8], new(*big.Int)).(**big.Int)
	out9 := *abi.ConvertType(out[9], new([]byte)).(*[]byte)

	return out0, out1, out2, out3, out4, out5, out6, out7, out8, out9, err

}

// GetVote is a free data retrieval call binding the contract method 0x5a55c1f0.
//
// Solidity: function getVote(uint256 ) view returns(bool, bool, uint64, uint64, uint64, uint64, uint256, uint256, uint256, bytes)
func (_IVoting *IVotingSession) GetVote(arg0 *big.Int) (bool, bool, uint64, uint64, uint64, uint64, *big.Int, *big.Int, *big.Int, []byte, error) {
	return _IVoting.Contract.GetVote(&_IVoting.CallOpts, arg0)
}

// GetVote is a free data retrieval call binding the contract method 0x5a55c1f0.
//
// Solidity: function getVote(uint256 ) view returns(bool, bool, uint64, uint64, uint64, uint64, uint256, uint256, uint256, bytes)
func (_IVoting *IVotingCallerSession) GetVote(arg0 *big.Int) (bool, bool, uint64, uint64, uint64, uint64, *big.Int, *big.Int, *big.Int, []byte, error) {
	return _IVoting.Contract.GetVote(&_IVoting.CallOpts, arg0)
}

// Vote is a paid mutator transaction binding the contract method 0xdf133bca.
//
// Solidity: function vote(uint256 , bool , bool ) returns()
func (_IVoting *IVotingTransactor) Vote(opts *bind.TransactOpts, arg0 *big.Int, arg1 bool, arg2 bool) (*types.Transaction, error) {
	return _IVoting.contract.Transact(opts, "vote", arg0, arg1, arg2)
}

// Vote is a paid mutator transaction binding the contract method 0xdf133bca.
//
// Solidity: function vote(uint256 , bool , bool ) returns()
func (_IVoting *IVotingSession) Vote(arg0 *big.Int, arg1 bool, arg2 bool) (*types.Transaction, error) {
	return _IVoting.Contract.Vote(&_IVoting.TransactOpts, arg0, arg1, arg2)
}

// Vote is a paid mutator transaction binding the contract method 0xdf133bca.
//
// Solidity: function vote(uint256 , bool , bool ) returns()
func (_IVoting *IVotingTransactorSession) Vote(arg0 *big.Int, arg1 bool, arg2 bool) (*types.Transaction, error) {
	return _IVoting.Contract.Vote(&_IVoting.TransactOpts, arg0, arg1, arg2)
}

// VoteForGaugeWeights is a paid mutator transaction binding the contract method 0xd7136328.
//
// Solidity: function vote_for_gauge_weights(address , uint256 ) returns()
func (_IVoting *IVotingTransactor) VoteForGaugeWeights(opts *bind.TransactOpts, arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IVoting.contract.Transact(opts, "vote_for_gauge_weights", arg0, arg1)
}

// VoteForGaugeWeights is a paid mutator transaction binding the contract method 0xd7136328.
//
// Solidity: function vote_for_gauge_weights(address , uint256 ) returns()
func (_IVoting *IVotingSession) VoteForGaugeWeights(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IVoting.Contract.VoteForGaugeWeights(&_IVoting.TransactOpts, arg0, arg1)
}

// VoteForGaugeWeights is a paid mutator transaction binding the contract method 0xd7136328.
//
// Solidity: function vote_for_gauge_weights(address , uint256 ) returns()
func (_IVoting *IVotingTransactorSession) VoteForGaugeWeights(arg0 common.Address, arg1 *big.Int) (*types.Transaction, error) {
	return _IVoting.Contract.VoteForGaugeWeights(&_IVoting.TransactOpts, arg0, arg1)
}

// IWalletCheckerMetaData contains all meta data concerning the IWalletChecker contract.
var IWalletCheckerMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"check\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Sigs: map[string]string{
		"c23697a8": "check(address)",
	},
}

// IWalletCheckerABI is the input ABI used to generate the binding from.
// Deprecated: Use IWalletCheckerMetaData.ABI instead.
var IWalletCheckerABI = IWalletCheckerMetaData.ABI

// Deprecated: Use IWalletCheckerMetaData.Sigs instead.
// IWalletCheckerFuncSigs maps the 4-byte function signature to its string representation.
var IWalletCheckerFuncSigs = IWalletCheckerMetaData.Sigs

// IWalletChecker is an auto generated Go binding around an Ethereum contract.
type IWalletChecker struct {
	IWalletCheckerCaller     // Read-only binding to the contract
	IWalletCheckerTransactor // Write-only binding to the contract
	IWalletCheckerFilterer   // Log filterer for contract events
}

// IWalletCheckerCaller is an auto generated read-only Go binding around an Ethereum contract.
type IWalletCheckerCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IWalletCheckerTransactor is an auto generated write-only Go binding around an Ethereum contract.
type IWalletCheckerTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IWalletCheckerFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type IWalletCheckerFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// IWalletCheckerSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type IWalletCheckerSession struct {
	Contract     *IWalletChecker   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// IWalletCheckerCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type IWalletCheckerCallerSession struct {
	Contract *IWalletCheckerCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// IWalletCheckerTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type IWalletCheckerTransactorSession struct {
	Contract     *IWalletCheckerTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// IWalletCheckerRaw is an auto generated low-level Go binding around an Ethereum contract.
type IWalletCheckerRaw struct {
	Contract *IWalletChecker // Generic contract binding to access the raw methods on
}

// IWalletCheckerCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type IWalletCheckerCallerRaw struct {
	Contract *IWalletCheckerCaller // Generic read-only contract binding to access the raw methods on
}

// IWalletCheckerTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type IWalletCheckerTransactorRaw struct {
	Contract *IWalletCheckerTransactor // Generic write-only contract binding to access the raw methods on
}

// NewIWalletChecker creates a new instance of IWalletChecker, bound to a specific deployed contract.
func NewIWalletChecker(address common.Address, backend bind.ContractBackend) (*IWalletChecker, error) {
	contract, err := bindIWalletChecker(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &IWalletChecker{IWalletCheckerCaller: IWalletCheckerCaller{contract: contract}, IWalletCheckerTransactor: IWalletCheckerTransactor{contract: contract}, IWalletCheckerFilterer: IWalletCheckerFilterer{contract: contract}}, nil
}

// NewIWalletCheckerCaller creates a new read-only instance of IWalletChecker, bound to a specific deployed contract.
func NewIWalletCheckerCaller(address common.Address, caller bind.ContractCaller) (*IWalletCheckerCaller, error) {
	contract, err := bindIWalletChecker(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &IWalletCheckerCaller{contract: contract}, nil
}

// NewIWalletCheckerTransactor creates a new write-only instance of IWalletChecker, bound to a specific deployed contract.
func NewIWalletCheckerTransactor(address common.Address, transactor bind.ContractTransactor) (*IWalletCheckerTransactor, error) {
	contract, err := bindIWalletChecker(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &IWalletCheckerTransactor{contract: contract}, nil
}

// NewIWalletCheckerFilterer creates a new log filterer instance of IWalletChecker, bound to a specific deployed contract.
func NewIWalletCheckerFilterer(address common.Address, filterer bind.ContractFilterer) (*IWalletCheckerFilterer, error) {
	contract, err := bindIWalletChecker(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &IWalletCheckerFilterer{contract: contract}, nil
}

// bindIWalletChecker binds a generic wrapper to an already deployed contract.
func bindIWalletChecker(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(IWalletCheckerABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IWalletChecker *IWalletCheckerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IWalletChecker.Contract.IWalletCheckerCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IWalletChecker *IWalletCheckerRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IWalletChecker.Contract.IWalletCheckerTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IWalletChecker *IWalletCheckerRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IWalletChecker.Contract.IWalletCheckerTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_IWalletChecker *IWalletCheckerCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _IWalletChecker.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_IWalletChecker *IWalletCheckerTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _IWalletChecker.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_IWalletChecker *IWalletCheckerTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _IWalletChecker.Contract.contract.Transact(opts, method, params...)
}

// Check is a free data retrieval call binding the contract method 0xc23697a8.
//
// Solidity: function check(address ) view returns(bool)
func (_IWalletChecker *IWalletCheckerCaller) Check(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _IWalletChecker.contract.Call(opts, &out, "check", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// Check is a free data retrieval call binding the contract method 0xc23697a8.
//
// Solidity: function check(address ) view returns(bool)
func (_IWalletChecker *IWalletCheckerSession) Check(arg0 common.Address) (bool, error) {
	return _IWalletChecker.Contract.Check(&_IWalletChecker.CallOpts, arg0)
}

// Check is a free data retrieval call binding the contract method 0xc23697a8.
//
// Solidity: function check(address ) view returns(bool)
func (_IWalletChecker *IWalletCheckerCallerSession) Check(arg0 common.Address) (bool, error) {
	return _IWalletChecker.Contract.Check(&_IWalletChecker.CallOpts, arg0)
}

// MathUtilMetaData contains all meta data concerning the MathUtil contract.
var MathUtilMetaData = &bind.MetaData{
	ABI: "[]",
	Bin: "0x60566023600b82828239805160001a607314601657fe5b30600052607381538281f3fe73000000000000000000000000000000000000000030146080604052600080fdfea2646970667358221220ff16a1facc7a3152dbbd125a0b77cbea124c4c735777e9856fcb8efe5483ff7164736f6c634300060c0033",
}

// MathUtilABI is the input ABI used to generate the binding from.
// Deprecated: Use MathUtilMetaData.ABI instead.
var MathUtilABI = MathUtilMetaData.ABI

// MathUtilBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use MathUtilMetaData.Bin instead.
var MathUtilBin = MathUtilMetaData.Bin

// DeployMathUtil deploys a new Ethereum contract, binding an instance of MathUtil to it.
func DeployMathUtil(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *MathUtil, error) {
	parsed, err := MathUtilMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(MathUtilBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &MathUtil{MathUtilCaller: MathUtilCaller{contract: contract}, MathUtilTransactor: MathUtilTransactor{contract: contract}, MathUtilFilterer: MathUtilFilterer{contract: contract}}, nil
}

// MathUtil is an auto generated Go binding around an Ethereum contract.
type MathUtil struct {
	MathUtilCaller     // Read-only binding to the contract
	MathUtilTransactor // Write-only binding to the contract
	MathUtilFilterer   // Log filterer for contract events
}

// MathUtilCaller is an auto generated read-only Go binding around an Ethereum contract.
type MathUtilCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MathUtilTransactor is an auto generated write-only Go binding around an Ethereum contract.
type MathUtilTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MathUtilFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type MathUtilFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MathUtilSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type MathUtilSession struct {
	Contract     *MathUtil         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// MathUtilCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type MathUtilCallerSession struct {
	Contract *MathUtilCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// MathUtilTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type MathUtilTransactorSession struct {
	Contract     *MathUtilTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// MathUtilRaw is an auto generated low-level Go binding around an Ethereum contract.
type MathUtilRaw struct {
	Contract *MathUtil // Generic contract binding to access the raw methods on
}

// MathUtilCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type MathUtilCallerRaw struct {
	Contract *MathUtilCaller // Generic read-only contract binding to access the raw methods on
}

// MathUtilTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type MathUtilTransactorRaw struct {
	Contract *MathUtilTransactor // Generic write-only contract binding to access the raw methods on
}

// NewMathUtil creates a new instance of MathUtil, bound to a specific deployed contract.
func NewMathUtil(address common.Address, backend bind.ContractBackend) (*MathUtil, error) {
	contract, err := bindMathUtil(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &MathUtil{MathUtilCaller: MathUtilCaller{contract: contract}, MathUtilTransactor: MathUtilTransactor{contract: contract}, MathUtilFilterer: MathUtilFilterer{contract: contract}}, nil
}

// NewMathUtilCaller creates a new read-only instance of MathUtil, bound to a specific deployed contract.
func NewMathUtilCaller(address common.Address, caller bind.ContractCaller) (*MathUtilCaller, error) {
	contract, err := bindMathUtil(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &MathUtilCaller{contract: contract}, nil
}

// NewMathUtilTransactor creates a new write-only instance of MathUtil, bound to a specific deployed contract.
func NewMathUtilTransactor(address common.Address, transactor bind.ContractTransactor) (*MathUtilTransactor, error) {
	contract, err := bindMathUtil(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &MathUtilTransactor{contract: contract}, nil
}

// NewMathUtilFilterer creates a new log filterer instance of MathUtil, bound to a specific deployed contract.
func NewMathUtilFilterer(address common.Address, filterer bind.ContractFilterer) (*MathUtilFilterer, error) {
	contract, err := bindMathUtil(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &MathUtilFilterer{contract: contract}, nil
}

// bindMathUtil binds a generic wrapper to an already deployed contract.
func bindMathUtil(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(MathUtilABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_MathUtil *MathUtilRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _MathUtil.Contract.MathUtilCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_MathUtil *MathUtilRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _MathUtil.Contract.MathUtilTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_MathUtil *MathUtilRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _MathUtil.Contract.MathUtilTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_MathUtil *MathUtilCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _MathUtil.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_MathUtil *MathUtilTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _MathUtil.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_MathUtil *MathUtilTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _MathUtil.Contract.contract.Transact(opts, method, params...)
}

// ReentrancyGuardMetaData contains all meta data concerning the ReentrancyGuard contract.
var ReentrancyGuardMetaData = &bind.MetaData{
	ABI: "[]",
}

// ReentrancyGuardABI is the input ABI used to generate the binding from.
// Deprecated: Use ReentrancyGuardMetaData.ABI instead.
var ReentrancyGuardABI = ReentrancyGuardMetaData.ABI

// ReentrancyGuard is an auto generated Go binding around an Ethereum contract.
type ReentrancyGuard struct {
	ReentrancyGuardCaller     // Read-only binding to the contract
	ReentrancyGuardTransactor // Write-only binding to the contract
	ReentrancyGuardFilterer   // Log filterer for contract events
}

// ReentrancyGuardCaller is an auto generated read-only Go binding around an Ethereum contract.
type ReentrancyGuardCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ReentrancyGuardTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ReentrancyGuardTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ReentrancyGuardFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ReentrancyGuardFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ReentrancyGuardSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ReentrancyGuardSession struct {
	Contract     *ReentrancyGuard  // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ReentrancyGuardCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ReentrancyGuardCallerSession struct {
	Contract *ReentrancyGuardCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts          // Call options to use throughout this session
}

// ReentrancyGuardTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ReentrancyGuardTransactorSession struct {
	Contract     *ReentrancyGuardTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts          // Transaction auth options to use throughout this session
}

// ReentrancyGuardRaw is an auto generated low-level Go binding around an Ethereum contract.
type ReentrancyGuardRaw struct {
	Contract *ReentrancyGuard // Generic contract binding to access the raw methods on
}

// ReentrancyGuardCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ReentrancyGuardCallerRaw struct {
	Contract *ReentrancyGuardCaller // Generic read-only contract binding to access the raw methods on
}

// ReentrancyGuardTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ReentrancyGuardTransactorRaw struct {
	Contract *ReentrancyGuardTransactor // Generic write-only contract binding to access the raw methods on
}

// NewReentrancyGuard creates a new instance of ReentrancyGuard, bound to a specific deployed contract.
func NewReentrancyGuard(address common.Address, backend bind.ContractBackend) (*ReentrancyGuard, error) {
	contract, err := bindReentrancyGuard(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &ReentrancyGuard{ReentrancyGuardCaller: ReentrancyGuardCaller{contract: contract}, ReentrancyGuardTransactor: ReentrancyGuardTransactor{contract: contract}, ReentrancyGuardFilterer: ReentrancyGuardFilterer{contract: contract}}, nil
}

// NewReentrancyGuardCaller creates a new read-only instance of ReentrancyGuard, bound to a specific deployed contract.
func NewReentrancyGuardCaller(address common.Address, caller bind.ContractCaller) (*ReentrancyGuardCaller, error) {
	contract, err := bindReentrancyGuard(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ReentrancyGuardCaller{contract: contract}, nil
}

// NewReentrancyGuardTransactor creates a new write-only instance of ReentrancyGuard, bound to a specific deployed contract.
func NewReentrancyGuardTransactor(address common.Address, transactor bind.ContractTransactor) (*ReentrancyGuardTransactor, error) {
	contract, err := bindReentrancyGuard(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ReentrancyGuardTransactor{contract: contract}, nil
}

// NewReentrancyGuardFilterer creates a new log filterer instance of ReentrancyGuard, bound to a specific deployed contract.
func NewReentrancyGuardFilterer(address common.Address, filterer bind.ContractFilterer) (*ReentrancyGuardFilterer, error) {
	contract, err := bindReentrancyGuard(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ReentrancyGuardFilterer{contract: contract}, nil
}

// bindReentrancyGuard binds a generic wrapper to an already deployed contract.
func bindReentrancyGuard(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ReentrancyGuardABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ReentrancyGuard *ReentrancyGuardRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ReentrancyGuard.Contract.ReentrancyGuardCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ReentrancyGuard *ReentrancyGuardRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ReentrancyGuard.Contract.ReentrancyGuardTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ReentrancyGuard *ReentrancyGuardRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ReentrancyGuard.Contract.ReentrancyGuardTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_ReentrancyGuard *ReentrancyGuardCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _ReentrancyGuard.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_ReentrancyGuard *ReentrancyGuardTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _ReentrancyGuard.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_ReentrancyGuard *ReentrancyGuardTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _ReentrancyGuard.Contract.contract.Transact(opts, method, params...)
}

// SafeERC20MetaData contains all meta data concerning the SafeERC20 contract.
var SafeERC20MetaData = &bind.MetaData{
	ABI: "[]",
	Bin: "0x60566023600b82828239805160001a607314601657fe5b30600052607381538281f3fe73000000000000000000000000000000000000000030146080604052600080fdfea26469706673582212208e7b841aad933c039ebe47722ed03eb21112ec85bb9e739f53335014e8c401b064736f6c634300060c0033",
}

// SafeERC20ABI is the input ABI used to generate the binding from.
// Deprecated: Use SafeERC20MetaData.ABI instead.
var SafeERC20ABI = SafeERC20MetaData.ABI

// SafeERC20Bin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use SafeERC20MetaData.Bin instead.
var SafeERC20Bin = SafeERC20MetaData.Bin

// DeploySafeERC20 deploys a new Ethereum contract, binding an instance of SafeERC20 to it.
func DeploySafeERC20(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SafeERC20, error) {
	parsed, err := SafeERC20MetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(SafeERC20Bin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SafeERC20{SafeERC20Caller: SafeERC20Caller{contract: contract}, SafeERC20Transactor: SafeERC20Transactor{contract: contract}, SafeERC20Filterer: SafeERC20Filterer{contract: contract}}, nil
}

// SafeERC20 is an auto generated Go binding around an Ethereum contract.
type SafeERC20 struct {
	SafeERC20Caller     // Read-only binding to the contract
	SafeERC20Transactor // Write-only binding to the contract
	SafeERC20Filterer   // Log filterer for contract events
}

// SafeERC20Caller is an auto generated read-only Go binding around an Ethereum contract.
type SafeERC20Caller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeERC20Transactor is an auto generated write-only Go binding around an Ethereum contract.
type SafeERC20Transactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeERC20Filterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SafeERC20Filterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeERC20Session is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SafeERC20Session struct {
	Contract     *SafeERC20        // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SafeERC20CallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SafeERC20CallerSession struct {
	Contract *SafeERC20Caller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts    // Call options to use throughout this session
}

// SafeERC20TransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SafeERC20TransactorSession struct {
	Contract     *SafeERC20Transactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts    // Transaction auth options to use throughout this session
}

// SafeERC20Raw is an auto generated low-level Go binding around an Ethereum contract.
type SafeERC20Raw struct {
	Contract *SafeERC20 // Generic contract binding to access the raw methods on
}

// SafeERC20CallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SafeERC20CallerRaw struct {
	Contract *SafeERC20Caller // Generic read-only contract binding to access the raw methods on
}

// SafeERC20TransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SafeERC20TransactorRaw struct {
	Contract *SafeERC20Transactor // Generic write-only contract binding to access the raw methods on
}

// NewSafeERC20 creates a new instance of SafeERC20, bound to a specific deployed contract.
func NewSafeERC20(address common.Address, backend bind.ContractBackend) (*SafeERC20, error) {
	contract, err := bindSafeERC20(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SafeERC20{SafeERC20Caller: SafeERC20Caller{contract: contract}, SafeERC20Transactor: SafeERC20Transactor{contract: contract}, SafeERC20Filterer: SafeERC20Filterer{contract: contract}}, nil
}

// NewSafeERC20Caller creates a new read-only instance of SafeERC20, bound to a specific deployed contract.
func NewSafeERC20Caller(address common.Address, caller bind.ContractCaller) (*SafeERC20Caller, error) {
	contract, err := bindSafeERC20(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SafeERC20Caller{contract: contract}, nil
}

// NewSafeERC20Transactor creates a new write-only instance of SafeERC20, bound to a specific deployed contract.
func NewSafeERC20Transactor(address common.Address, transactor bind.ContractTransactor) (*SafeERC20Transactor, error) {
	contract, err := bindSafeERC20(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SafeERC20Transactor{contract: contract}, nil
}

// NewSafeERC20Filterer creates a new log filterer instance of SafeERC20, bound to a specific deployed contract.
func NewSafeERC20Filterer(address common.Address, filterer bind.ContractFilterer) (*SafeERC20Filterer, error) {
	contract, err := bindSafeERC20(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SafeERC20Filterer{contract: contract}, nil
}

// bindSafeERC20 binds a generic wrapper to an already deployed contract.
func bindSafeERC20(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SafeERC20ABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeERC20 *SafeERC20Raw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeERC20.Contract.SafeERC20Caller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeERC20 *SafeERC20Raw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeERC20.Contract.SafeERC20Transactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeERC20 *SafeERC20Raw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeERC20.Contract.SafeERC20Transactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeERC20 *SafeERC20CallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeERC20.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeERC20 *SafeERC20TransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeERC20.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeERC20 *SafeERC20TransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeERC20.Contract.contract.Transact(opts, method, params...)
}

// SafeMathMetaData contains all meta data concerning the SafeMath contract.
var SafeMathMetaData = &bind.MetaData{
	ABI: "[]",
	Bin: "0x60566023600b82828239805160001a607314601657fe5b30600052607381538281f3fe73000000000000000000000000000000000000000030146080604052600080fdfea264697066735822122093dcab9ac1ce468924df6d27a5f1fbd11b57843aa03527905ef5212713ef4ea964736f6c634300060c0033",
}

// SafeMathABI is the input ABI used to generate the binding from.
// Deprecated: Use SafeMathMetaData.ABI instead.
var SafeMathABI = SafeMathMetaData.ABI

// SafeMathBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use SafeMathMetaData.Bin instead.
var SafeMathBin = SafeMathMetaData.Bin

// DeploySafeMath deploys a new Ethereum contract, binding an instance of SafeMath to it.
func DeploySafeMath(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SafeMath, error) {
	parsed, err := SafeMathMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(SafeMathBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SafeMath{SafeMathCaller: SafeMathCaller{contract: contract}, SafeMathTransactor: SafeMathTransactor{contract: contract}, SafeMathFilterer: SafeMathFilterer{contract: contract}}, nil
}

// SafeMath is an auto generated Go binding around an Ethereum contract.
type SafeMath struct {
	SafeMathCaller     // Read-only binding to the contract
	SafeMathTransactor // Write-only binding to the contract
	SafeMathFilterer   // Log filterer for contract events
}

// SafeMathCaller is an auto generated read-only Go binding around an Ethereum contract.
type SafeMathCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathTransactor is an auto generated write-only Go binding around an Ethereum contract.
type SafeMathTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SafeMathFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SafeMathSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SafeMathSession struct {
	Contract     *SafeMath         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SafeMathCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SafeMathCallerSession struct {
	Contract *SafeMathCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// SafeMathTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SafeMathTransactorSession struct {
	Contract     *SafeMathTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// SafeMathRaw is an auto generated low-level Go binding around an Ethereum contract.
type SafeMathRaw struct {
	Contract *SafeMath // Generic contract binding to access the raw methods on
}

// SafeMathCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SafeMathCallerRaw struct {
	Contract *SafeMathCaller // Generic read-only contract binding to access the raw methods on
}

// SafeMathTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SafeMathTransactorRaw struct {
	Contract *SafeMathTransactor // Generic write-only contract binding to access the raw methods on
}

// NewSafeMath creates a new instance of SafeMath, bound to a specific deployed contract.
func NewSafeMath(address common.Address, backend bind.ContractBackend) (*SafeMath, error) {
	contract, err := bindSafeMath(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SafeMath{SafeMathCaller: SafeMathCaller{contract: contract}, SafeMathTransactor: SafeMathTransactor{contract: contract}, SafeMathFilterer: SafeMathFilterer{contract: contract}}, nil
}

// NewSafeMathCaller creates a new read-only instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathCaller(address common.Address, caller bind.ContractCaller) (*SafeMathCaller, error) {
	contract, err := bindSafeMath(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SafeMathCaller{contract: contract}, nil
}

// NewSafeMathTransactor creates a new write-only instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathTransactor(address common.Address, transactor bind.ContractTransactor) (*SafeMathTransactor, error) {
	contract, err := bindSafeMath(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SafeMathTransactor{contract: contract}, nil
}

// NewSafeMathFilterer creates a new log filterer instance of SafeMath, bound to a specific deployed contract.
func NewSafeMathFilterer(address common.Address, filterer bind.ContractFilterer) (*SafeMathFilterer, error) {
	contract, err := bindSafeMath(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SafeMathFilterer{contract: contract}, nil
}

// bindSafeMath binds a generic wrapper to an already deployed contract.
func bindSafeMath(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SafeMathABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeMath *SafeMathRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeMath.Contract.SafeMathCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeMath *SafeMathRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeMath.Contract.SafeMathTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeMath *SafeMathRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeMath.Contract.SafeMathTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SafeMath *SafeMathCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SafeMath.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SafeMath *SafeMathTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SafeMath.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SafeMath *SafeMathTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SafeMath.Contract.contract.Transact(opts, method, params...)
}
